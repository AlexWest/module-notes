%Matlab Program for explicit finite difference scheme to solve a
%parabolic/heat equation.
% du/dt=D d2u/dx2
%D is a constant
%----------------------------------------------------------------
clear all;

% Parameters to define the heat equation and the range in space and time
L = 1.; % Length (spatial domain)
T =1.; % Final time

% Parameters needed to solve the equation within the explicit method
dt=0.0001; %time step
M=T/dt;  % number of time steps
dx=0.02;  % space step (check r=dt/dx^2=0.25<0.5 for stability)
N=L/dx;
D=1;
r=dt/dx^2;

% Initial condition: 
for i = 1:N+1
x(i) =(i-1)*dx; % calculating the real space
u(i,1) =sin(pi*x(i));
time(1) = 0;  % calculating the real time

end

% Implementation of the explicit method
for j=1:M % Time Loop
% At the boundary
u(1,j) = 0.; % zero boundary condition 
u(N+1,j) = 0.; % zero boundary condition
time(j+1) = (j)*dt;  % calculating the real time

% At inside points
for i=2:N; % Space Loop
u(i,j+1) =u(i,j) +D*r*(u(i-1,j)+u(i+1,j)-2.*u(i,j));
end
end

% Graphical representation of the temperature at different selected times
figure(1)
plot(x,u(:,1),'-',x,u(:,100),'-',x,u(:,300),'-',x,u(:,600),'-')
title('Temperature within the explicit method')
xlabel('X')
ylabel('T')
legend('M=1','M=100', 'M=300','M=600')
figure(2)
mesh(x,time,u')
title('Temperature within the explicit method')
xlabel('X')
ylabel('Time')
zlabel('Temperature')