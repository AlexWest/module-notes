exact_x = @(t) (sqrt(3).*exp(3.*t))./(sqrt(2.+exp(6.*t))); % Analytical soloution
f = @(a) a*(3-a^2);

h = 0.05; % Step sise
t0 = 0;
tf = 2;
steps = (tf-t0)/h;
x = zeros(steps+1,1);
% Initial 2 values
x(1) = 1;
x(2) = 1.099490670;
t = 0:h:tf;
u_old = x(2);
epsilon = 1;
for i = 3:steps+1
    % Fpi loop
    while epsilon>10^-6 
        u_new = x(i-2) + h*( 4*x(i-1)*(3-x(i-1)^2) + x(i-2)*(3-x(i-2)^2) + h*u_old*(3-u_old^2) )/3;
        epsilon = abs(u_new-u_old);
        u_old = u_new;
    end
    x(i)=u_new;
    epsilon = 1;
end
fprintf("t_i       x_i            x'_i\n")
for i = 1:steps+1
    fprintf("%1.2f  %12.6f   %12.6f \n",t(i), x(i), f(x(i)))
end
plot(t,x,"*")
xlabel("t")
ylabel("x(t)")
hold on
plot(t,exact_x(t),"-")
legend("Aproximate", "Exact")
hold off