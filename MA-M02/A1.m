clc
close all

h = 0.05; % Step size
% 
t_i = sqrt(pi/2):h:3*sqrt(pi/2); 
N = length(t_i); % Number of values to be evaulate at
% Initialize arrays of right size to storevalues in
u_i = zeros(N,1);
v_i = zeros(N,1);
ud_i = zeros(N,1);
vd_i = zeros(N,1);


% Initial values were given
v_i(1) = 0; % x'(t_0)
u_i(1) = pi/2; 
% The inital values for ud and vd are calculated in the loop the  
% consequence of this is that the derivative at the final point is
% never calculated but this can be easily calculated afterwards if needed
% and its inclusion doesn't actuall effect the aproximation of x(t).

% Cheeky function 
vd = @(t,u,v) cos(t^2)*(t^2) + v/t - 4*u*t^2;


for step = 1:N-1
    % Calculating the derivative
    ud_i(step) = v_i(step); 
    vd_i(step) = vd(t_i(step),u_i(step),v_i(step));
    % Calculating u_i and v_i
    u_i(step+1) = u_i(step) + h*ud_i(step);
    v_i(step+1) = v_i(step) + h*vd_i(step);
end

% Print out table of values
disp('n         t_n         u_n         u`_n        v_n         v`_n')
for i=1:N
    fprintf('%2.0d %12.6f %12.6f %12.6f %12.6f %12.6f \n',i,t_i(i),u_i(i),ud_i(i),v_i(i),vd_i(i))
end

% Exact soloution
exact_X = @(t) (2.*cos(t.^2) + (7*pi + 2.*t.^2).*sin(t.^2))/16;

plot(t_i,u_i,'b*')
xlabel('t_n')
ylabel('x_n')
title('Graph of x(t) against t')
hold
plot(t_i,exact_X(t_i),'r.') % Add the exact values to the plot
legend('aproximation','exact')
grid on
hold off