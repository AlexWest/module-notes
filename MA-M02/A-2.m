% Fixed Point Iteration for Backward Euler's Method
clc; clear all close all
h=0.1; %step size
% we run the code twice, resetting the initial value(s) prior to the
 second
% run.  we print final values at end of each step along with a table of the
% approximations generated en route.
% step 1
% initial values l=0
t_0=1; %initial value of t
u_0=0; % initial value of x
%values at l=1
l=1; %index of the solutions
u(l)=u_0+h/2*(2+(u_0)^3+u_0^3);
difference(l)=u(l)-u_0;%termination criterion
epsilon=abs(difference(l));
% Loop to find iterative solutions
while epsilon>0.00001
    u(l+1)=u_0+h/2*(2+(u(l))^3+u_0^3); % next sol
    difference(l+1)=u(l+1)-u(l); %termination criterion
    epsilon=abs(difference(l+1));
    l=l+1; % increasing index by 1
end
delete('FPI_Q2.txt')
diary('FPI_Q2.txt') % saves the workbook to a file eu2.txt
%table with values
disp('---------------------------------------')
disp(' l     u[l]         u[l+1]-u[l]')
disp('---------------------------------------')
fprintf('%2d %12.6f \n',0,u_0)
for i=1:l
    fprintf('%2d %12.6f %12.6f\n',i,u(i),difference(i))
end
xf = u(l);
t = t_0 +h;
fprintf('\n\n The value for x(%2.2f) is %7.6f.\n\n',t,xf)
% step 2
% initial values l=0
t_1=t_0 + h; %initial value of t
u_0=u(l); % initial value of x
%values at l=1
l=1; %index of the solutions
u(l)=u_0+h/2*(2+(u_0)^3+u_0^3);
difference(l)=u(l)-u_0;%termination criterion
epsilon=abs(difference(l));
% Loop to find iterative solutions
while epsilon>0.00001
    u(l+1)=u_0+h/2*(2+(u(l))^3+u_0^3); % next sol
    difference(l+1)=u(l+1)-u(l); %termination criterion
    epsilon=abs(difference(l+1));
    l=l+1; % increasing index by 1
end
%table with values
disp('---------------------------------------')
disp(' l     u[l]         u[l+1]-u[l]')
disp('---------------------------------------')
fprintf('%2d %12.6f \n',0,u_0)
for i=1:l
    fprintf('%2d %12.6f %12.6f\n',i,u(i),difference(i))
end
xf = u(l);
t = t +h;
fprintf('\n\n The value for x(%2.2f) is %7.6f.\n\n',t,xf)
diary off