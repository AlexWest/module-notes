%lab02 problems
%--------------
%add date
%Alex - 1912648

% Question 1
f = @(x) exp(-x.^2).*sin(pi.*x.^3);
subplot(2,2,1)
plot(f(-2:0.01:2), -2:0.01:2)

% Question 2
X = -2*pi:0.01:2*pi;

subplot(2,2,2)
plot(X, 3.*cos(X))
subplot(2,2,3)
plot(X,-2.*cos(2.*X))
subplot(2,2,4)
plot(X,-0.8.*sin(X.^2))

% Question 3
figure 1
f = @(x) 1+5.*x-6.*x.^3-exp(2.*x);
fprintf("%f , %f\n",f(-2),f(2))
plot(-2:0.01:2, f(-2:0.01:2))

% Question 4

A = [1:10]'
[A factorial(A)./(A.^A)]
% Question 5
syms x;
f = @(x) x^2*asin(x)
simplify(diff(f(x))-x*diff(f(x),2)==(x^2*(2*x^2-3))/((1-x^2)^(3/2)))
