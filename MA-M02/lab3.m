% % Question 1
% clc;
% clear all
% close all
% t_f=1; %Total time
% h=0.05; %step size
% %initial values n=0
% t_0=0; %initial value of t
% x_0=0; % initial value of x
% x_d0=t_0-2*t_0*x_0; % computing x'(0)
% N=(t_f-t_0)/h; % total steps (n) invloved
% %computing first set of values n=1
% t(1)=t_0+h;
% x(1)=x_0+h*x_d0;
% x_d(1)=(1-2*t(1))*x(1);
% %Exact solution is given by
% x_exact(1)= (1-exp(-t(1)^2))/2;
% %Global Error is given by
% GE_0=0;
% GE(1)=x_exact(1)-x(1);
% % Loop to find out the set of values for n=2,3,4,...N
% for i=2:N
%     t(i) = t(i-1)+h;
%     x(i) = x(i-1)+h*x_d(i-1);
%     x_d(i) = t(i)-2*t(i)*x(i);
%     x_exact(i) = (1-exp(-t(i)^2))/2;
%     GE(i) = x_exact(i)-x(i);
% end
% delete eu1.txt % deletes previous version of eu1.txt
% % prevents appending new results on to old results.
% diary('eu1.txt') % saves the workbook to a file eu1.txt
% %table with values
% disp('-------------------------------------------------------------')
% disp('n t n x n x`n GE')
% disp('--------------------------------------------------------------')
% fprintf('%d\t %f \t %f \t %f\t %f \n',0,t_0,x_0,x_d0, GE_0)
% for i=1:N
%     fprintf('%d \t %f \t %f\t %f\t %f \n',i,t(i),x(i),x_d(i),GE(i))
% end
% %Plotting the points inlcluding the value at n=0
% h=figure; %%%%%%%
% plot([0, t],[x_0,x],'r*')
% hold on
% plot([0, t],[x_0,x_exact],'b')
% xlabel('t_n')
% ylabel('x_n')
% saveas(h,'eu1','jpg')
% diary off

% Question 2
clc;
clear all
close all
t_f=2; %Total time
h=0.1; %step size

%initiatl values n=0
t_0=1; %initial value of t
u_0=1; % initial value of u_0=x_0=1
v_0=-3; % initial value of v_0=x'_0=2
u_d0=v_0; % computing u_d0=v_0 = x'(0)
v_d0=-6*u_0/t_0-6*u_d0/(t_0^2);
N=(t_f-t_0)/h; % total steps (n) invloved


%computing first set of values n=1
t(1) = t_0+h;
u(1) = u_0+h*u_d0;
v(1) = v_0+h*v_d0;
u_d(1) = v(1); %Derivative
v_d(1) = -6*u(1)/t(1)-6*u_d(1)/(t(1)^2); %Derivative
% Loop to find out the set of values for n=2,3,4,...N
for i = 2:N
    t(i) = t(i-1)+h;
    u(i) = u(i-1)+h*u_d(i-1);
    v(i) = v(i-1)+h*v_d(i-1);
 
    u_d(i) = v(i);
    v_d(i) = -6*u(i)/t(i)-6*u_d(i)/(t(i)^2);
end
delete eu4.txt % deletes previous version of eu4.txt
% prevents appending new results on to old results.
diary('eu4.txt') % saves the workbook to a file eu4.txt
line_dashes = ['------------------------------------------------' ...
'-------------------'];
%table with values
disp(line_dashes)
disp('n     t_n     u_n     u`_n    v_n     v`_n')
disp(line_dashes)
fprintf('%2d %12.6f %12.6f %12.6f %12.6f %12.6f \n',0,t_0,u_0,u_d0,v_0,v_d0)
for i=1:N
fprintf('%2.0d %12.6f %12.6f %12.6f %12.6f %12.6f \n',i,t(i),u(i),u_d(i),v(i),v_d(i))
end
diary off

% Plotting
plot([t_0, t],[u_0,u],'r*')
hold on
xlabel('t_n')
ylabel('x_n')
legend('approximation')
title('Graph of x(t) against t')