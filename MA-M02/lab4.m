% Program to Solve IVP using Trapeziodal Rule and AB method
clc;
close all
clear all
t_f = 1; % Total time
h = 0.05; %step size
%initial values n = 0
t_0 = 0; %initial value of t
x_0 = 0; % initial value of x
x_d0 = t_0 - 2*t_0*x_0; % computing x'(0)
% total steps (n) invloved, we find the integer value
% is int8 a ceiling or floor function N
% if floor then add 1 for paranoia OR just use `ciel`
N = int8((t_f-t_0)/h);
% since we have the initial values standing alone we will only have values
% for t(i), x1(i), x2(i), x1 d(i), x2_d(i) available from arrays for i  =  1
% onwards, so the loop starts with index 3 since AB(2) needs two previous
% values to begin
%computing first set of values n = 1
%--------------------------------------
t(1) = t_0+h;

%%%%%%%%%%%%Trapeziodal Rule%%%%%%%%%%%
%let x1(t) be the approximation using Trapeziod method
%it is an implicit method and the linearity of f allows us to evaluate
% x (n + 1 in terms of x n exactly. For general f we'll need FPI
%substitute the given function and simplify to get
% x1(1) =  (1+(1/2)*h*( t_0 - 2*t_0*x_0)) / (1-(1/2)*h*( 1-2*t(1)) )*x_0;
x1_f = @(x,t,tn) (2*x+h*(t+tn-2*t*x))/(2+2*h*tn);
% x1(1) = ( h*(t(1)+t_0)/2 - (t_0 + 1)*x_0 )/(1 + t(1));
x1(1) = x1_f(x_0,t_0,t(1));
%--------------------------------------

%%%%%%%%%%%%AB(2)Method%%%%%%%%%%%%%%%
%let x2(t) be the approximation using AB method.
%we cannot calculate x2(1) as this method need values of
%previous k steps. We can only calculate from n = 2. So we
%use the approximatation obtained through Trapezoidal rule
x2(1) = x1(1);
x2_d(1) = t(1)- 2*t(1)*x2(1); %x2'(1)
%--------------------------------------
%computing first set of values for n = 2
t(2) = t(1)+h;

%%%%%%%%%%%%Trapeziodal Rule%%%%%%%%%%%
% x1(2) = (1+(1/2)*h*(1-2*t(1)))/(1-(1/2)*h*(1-2*t(2)))*x1(1); % linearity of f
x1(2) = x1_f(x1(1),t(1),t(2));
%--------------------------------------%

%%%%%%%%%%%%AB(2)Method%%%%%%%%%%%%%%%
%use general formulae
x2(2) = x2(1)+h/2*(3*x2_d(1)-x_d0); % note that it is x_d0, not x2_d(0)
x2_d(2) = t(2)-2*t(2)*x2(2);
%--------------------------------------%
% now that needed values are in arrays we can loop
% Loop to find out the set of values for n = 3,4,...N

for i = 3:N
    t(i) = t(i-1)+h;
    %%%%%%%%%%%%Trapeziodal Rule%%%%%%%%%%%
%     x1(i) = (1+1/2*h*(1-2*t(i-1)))/(1-(1/2)*h*(1-2*t(i)))*x1(i-1);
    x1(i) = x1_f(x1(i-1),t(i-1),t(i));
    %--------------------------------------%
    %%%%%%%%%%%%AB(2)Method%%%%%%%%%%%%%%%
    %use general formulae
    x2(i) = x2(i-1)+h/2*(3*x2_d(i-1)-x2_d(i-2));
    x2_d(i) = t(i)-2*t(i)*x2(i);
    %--------------------------------------%
end

%finding global error for two methods
%Global Error at n = 0 is given by
GE_0 = 0;
x_exact = zeros(1,N);
GE_trp = zeros(1,N);
GE_AB = zeros(1,N);
for i = 1:N
    %Exact solution is given by
    x_exact(i) =  (1-exp(-t(i)^2) )/2;
    GE_trp(i) = x_exact(i)-x1(i);
    GE_AB(i) = x_exact(i)-x2(i);
end

delete Trapezoid AB.txt % removes previous diary file
diary('Trapezoid AB.txt') % saves the workbook to a file
%table with values
line_dashes  =  ['--------------------------------------------------' ...
'-----------------'];
disp(line_dashes)
disp('n      t_n          x_n(trap)    x_n(AB)        GE_trap      GE_AB')
disp(line_dashes)
fprintf('%2d %12.6f %12.6f %12.6f %12.6f %12.6f \n',0,t_0,x_0,x_0, GE_0, GE_0)
for i = 1:N
    fprintf('%2.0d %12.6f %12.6f %12.6f %12.6f %12.6f \n', ...
    i,t(i),x1(i),x2(i),GE_trp(i),GE_AB(i))
end
%Plotting the points including the value at n = 0
h = figure; % to save the figure

plot([0, t],[x_0,x1],'r*')
hold on %To plot in the same figure
plot([0, t],[x_0,x2],'g*')
plot([0, t],[x_0,x_exact],'b')
xlabel('t n')
ylabel('x n')
legend('Trapezoid', 'AB', 'exact')
saveas(h,'Trap AB','jpg') % this save the figure
diary off