
clc;
clear all
close all
t_f=3*sqrt(pi/2); %Total time
h=0.05; %step size

%initiatl values n=0
t_0=sqrt(pi/2); %initial value of t
u_0=pi/2; % initial value of u_0=x_0=1
v_0=0; % initial value of v_0=x'_0=2
u_d0=v_0; % computing u_d0=v_0 = x'(0)
v_d0=-cos(t_0^2)*(t_0^2) + v_0/t_0 - 4*u_0*(t_0^2);
N=(t_f-t_0)/h; % total steps (n) invloved


%computing first set of values n=1
t(1) = t_0+h;
u(1) = u_0+h*u_d0;
v(1) = v_0+h*v_d0;
u_d(1) = v(1); %Derivative
v_d(1) = cos(t(1)^2)*(t(1)^2) + v(1)/t(1) - 4*u(1)*(t(1)^2); %Derivative
% Loop to find out the set of values for n=2,3,4,...N
for i = 2:N
    t(i) = t(i-1)+h;
    u(i) = u(i-1)+h*u_d(i-1);
    v(i) = v(i-1)+h*v_d(i-1);
 
    u_d(i) = v(i);
    v_d(i) = cos(t(i)^2)*(t(i)^2) + v(i)/t(i) - 4*u(i)*(t(i)^2);
end
delete eu4.txt % deletes previous version of eu4.txt
% prevents appending new results on to old results.
diary('eu4.txt') % saves the workbook to a file eu4.txt
line_dashes = ['------------------------------------------------' ...
'-------------------'];
%table with values
disp(line_dashes)
disp('n     t_n     u_n     u`_n    v_n     v`_n')
disp(line_dashes)
fprintf('%2d %12.6f %12.6f %12.6f %12.6f %12.6f \n',0,t_0,u_0,u_d0,v_0,v_d0)
for i=1:N
fprintf('%2.0d %12.6f %12.6f %12.6f %12.6f %12.6f \n',i,t(i),u(i),u_d(i),v(i),v_d(i))
end
diary off

% Plotting
plot([t_0, t],[u_0,u],'r*')
hold on
xlabel('t_n')
ylabel('x_n')
legend('approximation')
title('Graph of x(t) against t')