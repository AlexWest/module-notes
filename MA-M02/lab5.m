% Fixed Point Iteration for Backward Euler's Method
clc; clear all; close all;
h = 0.1; %step size
% initial values l = 0
t_0 = 1; % initial value of t
x_0 = 0; % initial value of x
u_0 = x_0;
%values at l = 1
l = 1; %index of the solutions
u(l) = x_0+2*h*u_0*(1-u_0);
difference(l) = u(l)-u_0; %termination criterion
epsilon = difference(l); % what is missing here!!!!!!!!!!!!!
% Loop to find iterative solutions
while epsilon > 0.00001
    u(l+1)=x_0+2*h*u(l)*(1-u(l)); % next sol
    difference(l+1)=u(l+1)-u(l); % termination criterion
    epsilon=difference(l+1); % and here too !!!!!!!!!!!!!!!!
    l=l+1; % increasing index by 1
end
delete FPI.txt
diary('FPI.txt') % saves the workbook to a file FPI.txt
% table with values
disp('-----------------------------')
disp('l u[l] u[l+1]-u[l]')
disp('-----------------------------')
fprintf('%d %12.6f \n',0,u_0)
for i=1:l
    fprintf('%d %12.6f %12.6f\n',i,u(i),difference(i))
end
diary off