function [z,mu_k,k,mus]=shifted_inverse_power_method(A, z0, lam ,tol,maxit)
    k = 0;
    mu_k = 1;
    mu_km1 = Inf;
    z = z0;
    mus = zeros(maxit,1);
    A = A - lam*eye(size(A));
    % [L, U]=gaussian_elimination(A, z0);
    while (k < maxit && abs(mu_km1-mu_k)>tol)
        % zhat = L\z; % forward substitution
        % y_new = U\zhat; %Backward substitution

        y = A\z; % step 1
        mu_km1 = mu_k; % previous mu
        [~, i]=max(abs(y)); %step 2
        mu_k = y(i); %step 2
        z = y/mu_k;%step 3
        k = k+1;
        mus(k)=abs(mu_k-mu_km1)/abs(mu_k);

    end
    mu_k = lam + 1/mu_k;
    %%%%%%%%%%%%%%%%%%
    % mfilename('fullpath')
    %%%%%%%%%%%%%%%%%%%%%
end