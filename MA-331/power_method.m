function [z ,mu_k ,k, mus]=power_method(A, z0, tol, maxit)
k = 0;
mu_k = 1; % initialise
mu_km1 = Inf; %initialise
z = z0;
mus = zeros(maxit,1); %error
while (k <maxit && abs(mu_km1-mu_k)>tol)
    y = A*z; %step 1
    mu_km1 = mu_k; % previous mu
    [~, i]=max(abs(y)); % inf norm
    mu_k = y(i); % step 2
    z = y/mu_k; %step 3
    k = k+1; % next step
    mus(k)=abs(mu_k-mu_km1)/abs(mu_k); %error
end
%%%%%%%%%%%%%%%%%%
% mfilename('fullpath')
%%%%%%%%%%%%%%%%%%%%%
end