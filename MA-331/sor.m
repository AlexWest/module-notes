function [x,nit,relres]=sor(A,b,x0,tol,maxit)
    k = 0;
    x = x0;
    relres = norm(b-A*x,2)/norm(b,2);
    n = size(A,1);
    while k<maxit && relres>tol
        for i = 1:n
            s = 0;
            for j = 1:n
                if j~=i
                    s = s + A(i,j)*x(j);
                end
            end
            x(i) = (b(i)-s)/A(i,i); %x is updated with new x
        end
        k = k+1;
        relres = norm(b-A*x,2)/norm(b,2);
    end
    nit = k; %number of iterations
    %%%%%%%%%%%%%%%%%%%%%
    mfilename('fullpath')
    %%%%%%%%%%%%%%%%%%%%%