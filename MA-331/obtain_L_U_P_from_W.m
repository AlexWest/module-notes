function [L,U,P]=obtain_L_U_P_from_W(W,p)
%number of equations (rows)
n = size(W,1);
% copying all elemets in order of p from W to LU, the to L and U
LU = W(p,1:n);
L = LU;
U = LU;
for i = 1:n
    L(i,i)=1;
end

for i = 1:n
    for j=1:n
        if j>i
            L(i,j)=0;
        else
            if j~=i
                U(i,j)=0;
            end
        end
    end
end

P = eye(n); P = P(p,:);
