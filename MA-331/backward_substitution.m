function [A,b,x] = backward_substitution(A,b)
n = size(A,1);
x = zeros(size(b));
for j = n:-1:1
    s = 0;
    for k = j+1:n
        s = s+A(j,k)*x(k);
    end
    x(j) = (b(j)-s)/A(j,j);
end