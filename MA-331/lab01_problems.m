% Question 1
f  = @(x) exp(-x.^2) .* sin(pi.*x.^3);
x = linspace(-10,10,10000);
plot(x, f(x));
hold off;


% Question 2
figure;
hold on;
syms x y1(x) y2(x) y3(x)
y1(x) =3*cos(x);
y2(x) = -2*sin(2*x);
y3(x) = -0.8*sin(x^2);

fplot(y1)
fplot(y2)
fplot(y3)
hold off;

% Quesiton 4
syms x f(x)
f(x) = x.^2 * asin(x);

diff(f(x)) - x*diff(diff(f(x)))