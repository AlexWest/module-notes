tol = 10^-8;
maxit = 100000;

disp("Question 1")
A = [4 1 0; 1 4 1; 0 1 4];
z0 = [0.053461 0.529700 0.671149]';

[z, mu_k, k] = symmetric_power_method(A, z0, tol, maxit);
fprintf("The eigenvalue after %i iterations is %4.3f and the associated eignenvectgor is: \n", k, mu_k)
disp(z)

disp("Question 2")
B = [2, 0, 3; 0, -4, 3; 0, 5, -3];
z0 = [.7, .9, .3]';
mu_0 = 4;

[z, mu_k, k] = rayleigh_quotient_iteration(B, z0, tol, maxit, mu_0);
fprintf("The eigenvalue after %i iterations is %2.1f and the associated eignenvectgor is: \n", k, mu_k)
disp(z)

disp("Question 3")
C = [1, 0, 1; 0, 2, -1; 0, -1, 3];
z0 = [.5 -.8 .5]';
mu_0 = 3;

[z, mu_k, k] = rayleigh_quotient_iteration(C, z0, tol, maxit, mu_0);
fprintf("The eigenvalue after %i iterations is %2.1f and the associated eignenvectgor is: \n", k, mu_k)
disp(z) % As an aside z converges much slower than mu_k.
