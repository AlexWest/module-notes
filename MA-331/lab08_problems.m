%Matlab Program for explicit finite difference scheme to solve a
%parabolic/heat equation.
% du/dt=D d2u/dx2
%D is a constant
% ----------------------------------------------------------------
clear all;
% Parameters to define the heat equation and the range in space and time
L = 3; % Length (spatial domain)
T =1.; % Final time
% Parameters needed to solve the equation within the explicit method

dt=0.0001; %time step
M=T/dt; % number of time steps
dx=0.02; % space step (check r=dt/dxˆ2=0.25<0.5 for stability)
N=L/dx;
D=1;
r=dt/dx^2;
h=pi/6;

% region Initial condition:
for i = 1:N+1
    x(i) =(i-1)*dx; % x(i) contains the real value of x at the ith point
    u(i,1) =cos(x(i)); 

    time(1) = 0; % calculating the real time
end
% endregion


% region Implementation of the explicit method
for j=1:M % j is the current time so time(j) represent real value
    % Boundary Condition
    % at x = 0
    u(1,j) = u(2,j)/(h+1); % zero boundary condition
    u(N+1,j) = u(N,j)/(1+h); % zero boundary condition
    
    time(j+1) = (j)*dt; % calculating the real time
    
    % At inside points
    for i=2:N % Space Loop
        u(i,j+1) = u(i,j) +D*r*(u(i-1,j)+u(i+1,j)-2.*u(i,j));
    end
end
% endregion


% region Graphical representation of the temperature at different selected times
figure(1)
plot(x,u(:,1),'-',x,u(:,100),'-',x,u(:,300),'-',x,u(:,600),'-')
title('Temperature within the explicit method')
xlabel('X')
ylabel('T')
legend('M=1','M=100', 'M=300','M=600')
figure(2)
mesh(x,time,u')
title('Temperature within the explicit method')
xlabel('X')
ylabel('Temperature')
% endregion