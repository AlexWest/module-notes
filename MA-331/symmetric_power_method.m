function [z ,mu_k ,k, mus]=symmetric_power_method(A, z0, tol, maxit)
k = 0;
mu_k = 1; % initialise
mu_km1 = Inf; %initialise
z = z0/norm(z0);
mus = zeros(maxit,1); %error

while (k <maxit && abs(mu_km1-mu_k)>tol)
    y = A*z; %step 1
    mu_km1 = mu_k; % previous mu


    mu_k = dot(z,y);

    z = y/norm(y); %step 2

    k = k+1; % next step

    mus(k)=abs(mu_k-mu_km1)/abs(mu_k); %error
end
%%%%%%%%%%%%%%%%%%
% mfilename('fullpath')
%%%%%%%%%%%%%%%%%%%%%
end