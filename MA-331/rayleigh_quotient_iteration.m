function [z ,mu_k ,k, mus]=rayleigh_quotient_iteration(A, z0, tol, maxit, mu_0)
    k = 0;
   
    mu_km1 = Inf; %initialise

    z = z0/norm(z0,2);

    mu_k = mu_0; % initialise

    mus = zeros(maxit,1); %error


    while (k <maxit && abs(mu_km1-mu_k)>tol)
        y = linsolve(A - mu_k*eye(size(A)), z); %step 1
        % disp(z)
        % disp(y)
        % pause()
        mu_km1 = mu_k; % previous mu
        z = y/norm(y, 2); % step 3
        mu_k = (z')*A*z;
        k = k+1; % next step

        mus(k)=abs(mu_k-mu_km1)/abs(mu_k); %error

    end
    %%%%%%%%%%%%%%%%%%
    % mfilename('fullpath')
    %%%%%%%%%%%%%%%%%%%%%
end