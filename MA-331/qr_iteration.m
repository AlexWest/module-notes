function [Qt,R,Qp,Ak, k, achieved_tol]=qr_iteration(A,maxit,tol)
k = 0;
Ak = A;
Qt = eye(size(A,1));
%tril(X) is the lower triangular part of X.
%tril(X,K) is the elements on and below the K-th diagonal of X
while(k<maxit && norm(tril(Ak,-1),2)>tol)
    [Q,R]=qr_decomposition(Ak);
    Ak = R*Q;
    Qt = Qt*Q;
    k = k+1;
    norm(tril(Ak,-1),2);
end
achieved_tol = norm(tril(Ak,-1),2);
Qp=eigenvectors_from_qr(Qt,R);
end