function I=trap_x_simp_y(f,x_0, x_1, y_0, y_1, NT, NS) %M=2N
% NT is number of repeats of trapezium rule
% NS is number of repeats of Simpsons rule
% that's why h_y = (y_1 - y_0)/(2 * NS)
% x mesh and y_mesh

x_m=linspace(x_0, x_1, NT+1)';
h_x=(x_1 - x_0)/NT;
y_m = linspace(y_0, y_1, 2 * NS + 1)';
h_y = (y_1 - y_0)/(2 * NS);

% Trapezium weights
t_w = 2 * ones(NT + 1,1);
t_w(1) = 1;
t_w(end) = 1;
% Simpson weights
s_w=2*ones(2 * NS + 1,1);
s_w(1)=1;

for j=2:2:2 * NS
    s_w(j)=4;
end
s_w(end)=1;

% Calculations
% summing in loops
I=0;
for i = 1:NT + 1
    for j = 1:2*NS +1
        I = I+t_w(i) * s_w(j)*f(x_m(i), y_m(j));
    end
end
I = (h_x/2) * (h_y/3) * I;
%w
%x