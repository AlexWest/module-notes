%parabolic/heat equation.
% du/dt=D (d2u/dx2+d2u/dy2)
%D is a constant%
%----------------------------------------------------------------
clear all;

% Parameters to define the heat equation and the range in space and time
L = 3.14; % Length in x and y directions (spatial domain)
T =1.; % Final time

% Parameters needed to solve the equation within the explicit method
dt=0.00005; %time step
M=T/dt; % number of time steps
dx=0.02; % space step (check r=dt/dxˆ2=0.125<0.25 for stability)
dy=dx; % equal space step
N=L/dx; % Number of points in x and y directions
D=1;
r_x=dt/dx^2;
r_y=dt/dy^2;

% Initial condition:
for i = 1:N+1
    for j=1:N+1
        x(i) =(i-1)*dx; % calculating the real space
        y(j)= (j-1)*dy;
        u(i,j,1) = sin(x(i))*sin(y(j)); %  Initial contdition at t=0
        time(1) = 0; % calculating the real time
    end
end



% Implementation of the explicit method
for n=1:M % Time Loop
    % At the boundary
    % derivative boundary condition du/dy=0, use forward difference to approximate
    u(1:N+1,1,n) =  0 ;%u(1:N+1,2,n);
    % derivative boundary condition du/dy=0, use backward difference to approximate
    u(1:N+1,N+1,n) = 0; %u(1:N+1,N,n);
    % derivative boundary condition du/dx=0, use forward difference to approximate
    u(1,1:N+1,n) = 0; %u(2,1:N+1,n);
    % derivative boundary condition du/dx=0, use backward difference to approximate
    u(N+1,1:N+1,n) = 0;%u(N,1:N+1,n);

    
    time(n+1) = (n)*dt; % calculating the real time
    % At inside points
    for i=2:N % Space Loop
        for j=2:N % Space Loop
            u(i,j,n+1) = u(i,j,n) + D*(r_x*(u(i-1,j,n)+u(i+1,j,n)-2.*u(i,j,n))+r_y*(u(i,j-1,n)+u(i,j+1,n)-2.*u(i,j,n)));
        end
    end
end

% Graphical representation of the temperature at different selected times
k=1;% first plot
for n=1:1000:M % plotting in every 1000 time steps
subplot(5,4,k) % to plot 20 figures in in figure
meshc(x,y,u(:,:,n)')
str=sprintf('Time = %3.3f Hrs' , time(n));
title(str)
xlabel('X')
ylabel('Y')
k=k+1; %next small plot
end