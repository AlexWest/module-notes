fprintf("Section 3.3 \n-----------\n")

disp("Question 1")
A = [1 4 -2 3; 0 1 4 5;0 0 1 -4; 0 0 0 1];
b = [5 14 -2 1];
[A, b, x] = backward_substitution(A,b)

disp("Question 2")
A = [1 2 -4 3; 0 1 -3 4; 0 0 1 2; 0 0 0 1];
b = [6 3 3 1];
[A, b ,x] = backward_substitution(A,b)

fprintf("Section 4.3 \n-----------\n")

disp("Question 2")
A = [2 -2 4 5; -4 2 -9 7; 6 -10 14 17; -8 12 -17 -25];
b = [12 -23 33 -51];
[A, b, x] = gaussian_elimination(A, b)

disp("Question 3")
A = [1.8 2.9 2.7 -1.3; -2.2 2.8 -1.7 1.1; -1.8 2.7 1 1.7; -0.1 1.3 0.3 1.6];
b = [17.3 1.7 2.6 2.8];
[A, b, x] = gaussian_elimination(A, b)
