disp("1.3 Question 1")
% Gausian Elimination on Ax=b.
A = [-0.4 2.8 2.3 0.8; 0.5 0.2 -1.3 2.9; 2.4 2.1 1.3 -0.1; -2.8 -0.4 0.2 -1];
b = [-0.2 0.6 4.1 -3]';
disp("The resultant matrix is displayed as W");
[W,x,b,p] = gaussian_elimination_with_pivoting(A,b,'naive')

disp("1.3 Question 2")
A = [2 -2 4 5; -4 2 -9 -7; 6 -10 13 17; -8 12 -17 -25];
b = [12 -23 33 -51]';
disp("The resultant matrix is displayed as W");
[W,x,b,p] = gaussian_elimination_with_pivoting(A,b,'scaled')

% Just to avoid overwriting so i can use them in the last bit.
p2 = p;
W2 = W;
disp("1.3 Question 3")
A = [1.8 2.9 2.7 -1.3; -2.2 2.8 -1.7 1.1; -1.8 2.7 -1.0 1.7; -0.1 1.3 0.3 1.6];
b = [-17.3 1.7 2.6 2.8]';
disp("The resultant matrix is displayed as W");
[W,x,b,p] = gaussian_elimination_with_pivoting(A,b,'partial')

disp("Section 2.2.1")
w = W2;
p = p2;
[L,U,P] = obtain_L_U_P_from_W(W, p)