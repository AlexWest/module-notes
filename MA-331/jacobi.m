function [x,nit,relres]=jacobi(A,b,x0,tol,maxit)
k = 0;
x = x0;
% giving the stopping criteria ||b-Ax ||2 /||b ||and maximum number of stepps maxit to avoid infinite loop
relres = norm(b-A*x,2)/norm(b,2);
n = size(A,1);
while k<maxit && relres>tol
    xnew = x; %x at k+1
    for i = 1:n
        s = 0;
        for j = 1:n
            if j~=i %i not equal to j
                s = s + A(i,j)*x(j); %sum
            end
        end
        xnew(i) = (b(i)-s)/A(i,i);
    end
    k = k+1;
    x = xnew;
    relres = norm(b-A*x,2)/norm(b,2);%finding norm
end
nit = k;%number of iternations
%%%%%%%%%%%%%%%%%%%%%
mfilename('fullpath')
%%%%%%%%%%%%%%%%%%%%%