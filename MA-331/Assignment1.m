disp("Question (4) i)")
% The initial matrix A and vector b for Ax=b.
A = [0.41681, -0.328195, -0.00475369, 0.0995082, 0.132078; 
    0.956786, -0.684191, 0.777055, 0.13244, 0.504651;
    -0.553239, -0.922224, -0.111614, 0.467012, 0.0836783;
    0.76388, 0.264723, 0.744699, 0.81498, -0.583127;
    0.753682, 0.158474, -0.0659439, 0.83054, -0.663128];

b = [-0.562509 -1.5526 -0.495475 0.0971416 0.261463]';

disp("The reduced matrix is W & the soloution is displayed as x.")
[W,x,b,p] = gaussian_elimination_with_pivoting(A, b, 'partial')

disp("ii)");
disp("The matrixs L, U & P s.t. PLU = A.");
% Using the matrix W and the pivot vector p from the previous part.
[L,U,P] = obtain_L_U_P_from_W(W, p)

P*(L*U)