function [W,x,b,p]=gaussian_elimination_with_pivoting(A, b, strategy)
% Gaussian Elimination with choice of pivoting strategy
n=size(A,1);
%n=2
if nargin <3 % if number of function input is <3
    strategy = 'no pivoting';
end
% compute scaling in case of scaled partial pivoting
d = max(abs(A),[],2);
% initialise identity pivot vector
p = (1:numel(b))';
% Set initial workspace to be the augmented system
W = [A b];
% Loop over stages...
for k = 1:n-1 % move from stage 1 to stage n-1 of the elimination process.
    % Now we need to choose the pivot from equations p k, p {k+1},...,p n
    j = choose_pivot(k,W,d,p,strategy); % it is a function given below
    % swap p(j) <->p(k)
    temp = p(j);
    p(j) = p(k);
    p(k) = temp;
    for i = k+1:n
        W(p(i),k) = W(p(i),k)/W(p(k),k);
        for j = k+1:n+1
            W(p(i),j) = W(p(i),j) - W(p(i),k)*W(p(k),j);
        end
    end
end
% Backward elimination with pivoting- function given below
[x]=backward_substitution_with_pivoting(W,p);
end

function j = choose_pivot(k,W,d,p,strategy)
n = size(W,1);
switch strategy
case 'no pivoting'
    j = k;
% no need to swap rows
case 'naive'
    % find the first non-zero in remaining pivots for variable x k
    for i = k:n
        if W(p(i),k)~=0
            j = i;
            return
        end
    end
    case 'partial'
        [~,j]=max(abs(W(p(k:n),k)));
        j = j+k-1;
    case 'scaled'
        [~,j]=max(abs(W(p(k:n),k))./d(k:n));
        j = j+k-1;
end
end

function [x] = backward_substitution_with_pivoting(W,p)
n = size(W,1);
x = zeros(numel(p),1);
for k = n:-1:1
    s = 0;
for j = k+1:n
    s = s+W(p(k),j)*x(j);
end
x(k) = (W(p(k),n+1)-s)/W(p(k),k);
end
end