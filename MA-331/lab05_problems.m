% Setting the tollerance and max iteration
tol = 0.0001;
maxit = 1000;

disp("Question 1")
A = [4 -1 2; -1 3 -2; 2 -2 1];
z0 = [.7 -.5 .45]';


fprintf(['The eigenvector is dsplayed as z and the associated eigenvalue'...
            'as mu_k.\n k is the numbe of steps taken.'])
[z, mu_k, k] = power_method(A, z0, tol, maxit)

disp("Question 2")
A = [4 -1 1; -1 3 -2; 1 -2 3];
z0 = [.7 -.5 .4]';

fprintf(['The eigenvector is dsplayed as z and the associated eigenvalue'...
            'as mu_k.\n k is the numbe of steps taken.'])
[z, mu_k, k] = inverse_power_method(A, z0, tol, maxit)

disp("Question 3")
A = [4 .4 .1; 1.2 3.04 .14; -.4 .24 3.3];
z0 = [-.1 0 1]';
lam = 3;

fprintf(['The eigenvector is dsplayed as z and the associated eigenvalue'...
            'as mu_k.\n k is the numbe of steps taken.'])
[z, mu_k, k] = shifted_inverse_power_method(A, z0, lam, tol, maxit)