maxit = 4;
tol = 10^-8;
B = [2 -1 2; 0 -2 5; 0 3 -4];
z0 = [-0.5 2.9 3.2]';
mu_0 = 1.2; %Using 1.2 as initial guess to ensure the eigen value is nearby

[z ,mu_k ,k] = rayleigh_quotient_iteration(B, z0, tol, maxit, mu_0);
fprintf("After %i iterations the aproximation of the eigenvalue (µ) is %2.4f (4 dp).\n",k,mu_k)
fprintf("The associated eigenvector z is: \n")
disp(z)