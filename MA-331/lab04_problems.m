%  Section 4 Question 2
B = [6 1 0 1 1; 0 -8 2 1 1; 0 -2 8 2 0; 2 1 0 11 -2; 0 -1 1 0 6]
b = [6.333,7.500,10.166,0.830,5.500]';
x0 = [1,1,1,1,1]';

tol= 0.0001
maxit = 100

[x,nit,relres] = gauss_seidel(B,b,x0,tol,maxit)

[x,nit,relres] = jacobi(B,b,x0,tol,maxit)