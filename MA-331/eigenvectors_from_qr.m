function V=eigenvectors_from_qr(Q,R)
V = zeros(size(Q));
n = size(Q,1);
for k = 1:n % try to determine the k-th eigenvector
    c = zeros(n,1);
    c(k) = 1;
    for j = k-1:-1:1 % determine the coefficients of this eigenvector
                        %in the basis given by Q, in reverse order
        s = 0;
        for i = j+1:k
            s = s+R(j,i)*c(i);
        end
        c(j) = s/(R(k,k)-R(j,j));
    end
    % k-th eigenvector
    V(:,k)=Q*c;
end
for k = 1:n
    V(:,k) = V(:,k)/norm(V(:,k),2);
end
end