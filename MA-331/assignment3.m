%Matlab Program for explicit finite difference scheme to solve a
%parabolic/heat equation.
% du/dt=D d2u/dx2
%D is a constant
% ----------------------------------------------------------------
clear all;
% Parameters to define the heat equation and the range in space and time
L = 3; % Length (spatial domain)
T =2.5; % Final time
% Parameters needed to solve the equation within the explicit method

dt=0.00005; %time step
M=T/dt; % number of time steps
dx=0.02; % space step (check r=dt/dxˆ2=0.25<0.5 for stability)
N=L/dx;
D=1;
r=dt/dx^2;
h=pi/30;

% region Initial condition:
for i = 1:N+1
    x(i) =(i-1)*dx; % x(i) contains the real value of x at the ith point
    u(i,1) =sin(x(i)); 

    time(1) = 0; % calculating the real time
end
% endregion


% region Implementation of the explicit method
for j=1:M % j is the current time so time(j) represent real value
    % At inside points
        
    for i=2:N % Space Loop
        u(i,j+1) = u(i,j) +D*r*(u(i-1,j)+u(i+1,j)-2.*u(i,j));
    end
    
    % Boundary Condition
    % at x = 0
    u(1,j+1) = u(2,j+1)-h; % zero boundary condition
    u(N+1,j+1) = u(N,j+1)-h; % zero boundary condition
    
    time(j+1) = (j)*dt; % calculating the real time
end
% endregion


% region Graphical representation of the temperature at different selected times
figure(1)
plot(x,u(:,1),'-',x,u(:,100),'-',x,u(:,300),'-',x,u(:,600),'-')
title('Temperature within the explicit method')
xlabel('X')
ylabel('T')
legend('M=1','M=100', 'M=300','M=600')
figure(2)
mesh(x,time,u')
title('Temperature within the explicit method')
xlabel('X')
ylabel('Temperature')
% endregion


disp("Part ii")
u_exact = @(x,t) -(x.^2)./pi + x -(2.*t)/pi;
j = 1;
for i=1:length(x)
    if abs((pi/2)-x(i))<abs((pi/2)-x(j)) % find find the mesh point where x is closest to pi/2
        j = i; 
    end
end

diffconst = u(j,50001) - u_exact(pi/2, 2.5);
fprintf("The difference between the two soloutions at x=pi/2 for the 50000 time step is %f.\n",diffconst)

disp("Part iii")
new_u_exact = @(x,t) u_exact(x,t) + diffconst;

max_diff=norm(u(j,:)-new_u_exact(pi/2,time),Inf); % Use the inf norm to find the absoloute max difference

fprintf("The maximum absolute difference between the function and vector is %f.\n", max_diff)
