function [A,b,x]=gaussian_elimination(A,b)
    % Naive implementation. As in algorithm presented in lectures
    n=size(A,1);
    l = zeros(size(A));
    for k = 1:n-1
    for i = 1:k
    for j = 1:n
    A(i,j) = A(i,j);
    end
    b(i) = b(i);
    end
    for i = k+1:n
    l(i,k) = A(i,k)/A(k,k);
    for j=1:k
    A(i,j) = 0;
    end
    for j = k+1:n
    A(i,j) = A(i,j) - l(i,k)*A(k,j);
    end
    b(i) = b(i) - l(i,k)*b(k);
    end
    end
    %Note that here we are using already defined backward substitution function
[A,b,x] = backward_substitution(A,b);