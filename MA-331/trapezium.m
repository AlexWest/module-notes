function I=trapezium(f,a,b,N)
x=linspace(a,b,N+1)';
h=(b-a)/N;
I=f(x(1));
for j=2:N
    I=I+2*f(x(j));
end
I=I+f(x(N+1));
I=I*h/2;