function [Q,R]=qr_decomposition(A)
n = size(A,1);
e1 = zeros(n,1);
e1(1) = 1;
I = eye(n);
Q = I;% starting matrix
for j = 1:n
    a = A(j:end,j);
    c = sqrt(a'*a);
    w = a+sign(a(1))*c*e1;
    gamma = 2/(w'*w);
    k = j-1;
    Q11=eye(n-k) - gamma*(w*w');
    %Orthogonal matrix embedded into identity
    Q1 = [eye(k) zeros(k,n-k); zeros(n-k,k) Q11];
    %New A k
    A = Q1*A;
    % Final Q=Q1'*Q2'*....
    Q = Q*Q1';
    e1 = e1(1:end-1);
end
R = A;