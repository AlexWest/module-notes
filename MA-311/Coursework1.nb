(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21922,        634]
NotebookOptionsPosition[     18926,        582]
NotebookOutlinePosition[     19320,        598]
CellTagsIndexPosition[     19277,        595]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["Question 1", "Text",
 CellChangeTimes->{{3.854274585263962*^9, 
  3.854274587452804*^9}},ExpressionUUID->"962c73b7-e7cb-4125-9565-\
545f90770866"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"pde", " ", "=", " ", 
  RowBox[{
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", "x", ",", "x"}], "]"}], " ", "+", 
    " ", 
    RowBox[{"2", 
     RowBox[{"D", "[", 
      RowBox[{
       RowBox[{"u", "[", 
        RowBox[{"x", ",", "y"}], "]"}], ",", "x", ",", "y"}], "]"}]}], " ", 
    "+", " ", 
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", "y", ",", "y"}], "]"}]}], "==", 
   "0"}]}], "\[IndentingNewLine]", 
 RowBox[{"soln", "=", 
  RowBox[{"DSolveValue", "[", 
   RowBox[{"pde", ",", 
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "y"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", "y"}], "}"}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.854274597448283*^9, 3.8542746515732927`*^9}, {
  3.854274728657048*^9, 3.854274729204851*^9}, {3.854274764254421*^9, 
  3.854274771397896*^9}, {3.8543663704635067`*^9, 3.854366371397162*^9}},
 CellLabel->"In[97]:=",ExpressionUUID->"bc1c6aa5-33d2-4077-ac24-b20e8e8fc727"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    SuperscriptBox["u", 
     TagBox[
      RowBox[{"(", 
       RowBox[{"0", ",", "2"}], ")"}],
      Derivative],
     MultilineFunction->None], "[", 
    RowBox[{"x", ",", "y"}], "]"}], "+", 
   RowBox[{"2", " ", 
    RowBox[{
     SuperscriptBox["u", 
      TagBox[
       RowBox[{"(", 
        RowBox[{"1", ",", "1"}], ")"}],
       Derivative],
      MultilineFunction->None], "[", 
     RowBox[{"x", ",", "y"}], "]"}]}], "+", 
   RowBox[{
    SuperscriptBox["u", 
     TagBox[
      RowBox[{"(", 
       RowBox[{"2", ",", "0"}], ")"}],
      Derivative],
     MultilineFunction->None], "[", 
    RowBox[{"x", ",", "y"}], "]"}]}], "\[Equal]", "0"}]], "Output",
 CellChangeTimes->{{3.854274621978793*^9, 3.85427463807791*^9}, 
   3.854274729521389*^9, {3.854274764963077*^9, 3.854274771890719*^9}, 
   3.854366372027343*^9},
 CellLabel->"Out[97]=",ExpressionUUID->"2f42f33e-761a-4c8a-9abf-55245ea63e3d"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   TemplateBox[{"1"},
    "C"], "[", 
   RowBox[{
    RowBox[{"-", "x"}], "+", "y"}], "]"}], "+", 
  RowBox[{"x", " ", 
   RowBox[{
    TemplateBox[{"2"},
     "C"], "[", 
    RowBox[{
     RowBox[{"-", "x"}], "+", "y"}], "]"}]}]}]], "Output",
 CellChangeTimes->{{3.854274621978793*^9, 3.85427463807791*^9}, 
   3.854274729521389*^9, {3.854274764963077*^9, 3.854274771890719*^9}, 
   3.854366372049817*^9},
 CellLabel->"Out[98]=",ExpressionUUID->"dd65046c-2c2c-486a-95a4-1629431b7b68"]
}, Open  ]],

Cell["Question 2", "Text",
 CellChangeTimes->{{3.8542745749141083`*^9, 
  3.854274581845027*^9}},ExpressionUUID->"8bc7baeb-ecff-4f10-94a8-\
ab19064e48dc"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"pde", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], "+", 
    RowBox[{
     RowBox[{"x", "^", "2"}], "*", 
     RowBox[{"D", "[", 
      RowBox[{
       RowBox[{"u", "[", 
        RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}]}]}], "==", 
   "0"}]}], "\[IndentingNewLine]", 
 RowBox[{"DSolve", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"pde", ",", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"x", ",", "0"}], "]"}], "==", 
      RowBox[{"x", "^", "6"}]}]}], "}"}], ",", 
   RowBox[{"u", "[", 
    RowBox[{"x", ",", "y"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "y"}], "}"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"ic", " ", "=", " ", 
  RowBox[{
   RowBox[{"u", "[", 
    RowBox[{"x", ",", "0"}], "]"}], "==", 
   RowBox[{"x", "^", "6"}]}]}]}], "Input",
 CellChangeTimes->{{3.8542682991362247`*^9, 3.854268330939878*^9}, {
  3.854274563904665*^9, 3.8542745650093718`*^9}, {3.854276253982925*^9, 
  3.854276265013425*^9}, {3.854366674804234*^9, 
  3.8543667099431973`*^9}},ExpressionUUID->"0ea3f137-092a-47df-9097-\
b4417e444eea"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    SuperscriptBox["u", 
     TagBox[
      RowBox[{"(", 
       RowBox[{"0", ",", "1"}], ")"}],
      Derivative],
     MultilineFunction->None], "[", 
    RowBox[{"x", ",", "y"}], "]"}], "+", 
   RowBox[{
    SuperscriptBox["x", "2"], " ", 
    RowBox[{
     SuperscriptBox["u", 
      TagBox[
       RowBox[{"(", 
        RowBox[{"1", ",", "0"}], ")"}],
       Derivative],
      MultilineFunction->None], "[", 
     RowBox[{"x", ",", "y"}], "]"}]}]}], "\[Equal]", "0"}]], "Output",
 CellChangeTimes->{{3.854268319970818*^9, 3.854268331706704*^9}, 
   3.854274565945849*^9, 3.854276276764641*^9, 3.8543666950223293`*^9},
 CellLabel->"Out[99]=",ExpressionUUID->"b7990ac9-75af-410c-9ade-52d89fd9c3af"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "y"}], "]"}], "\[Rule]", 
    FractionBox[
     SuperscriptBox["x", "6"], 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"1", "+", 
        RowBox[{"x", " ", "y"}]}], ")"}], "6"]]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.854268319970818*^9, 3.854268331706704*^9}, 
   3.854274565945849*^9, 3.854276276764641*^9, 3.854366695042395*^9},
 CellLabel->
  "Out[100]=",ExpressionUUID->"c4f5dcc1-c68a-4f3e-90ec-3a1eda77b4f0"],

Cell[BoxData[
 RowBox[{
  RowBox[{"u", "[", 
   RowBox[{"x", ",", "0"}], "]"}], "\[Equal]", 
  SuperscriptBox["x", "6"]}]], "Output",
 CellChangeTimes->{{3.854268319970818*^9, 3.854268331706704*^9}, 
   3.854274565945849*^9, 3.854276276764641*^9, 3.8543666950435953`*^9},
 CellLabel->
  "Out[101]=",ExpressionUUID->"3a08d66e-ad41-4a53-bc47-e2c7040a0399"]
}, Open  ]],

Cell["Question 3) a)", "Text",
 CellChangeTimes->{{3.8542765166243753`*^9, 
  3.854276531350984*^9}},ExpressionUUID->"b76c13aa-d204-43cb-b122-\
25c366b5c573"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"Z", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", 
       RowBox[{"-", "4"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "4"}], ",", "1"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Det", "[", "Z", "]"}], "\[IndentingNewLine]", 
 RowBox[{"PositiveDefiniteMatrixQ", "[", "Z", "]"}]}], "Input",
 CellChangeTimes->{{3.854276525677106*^9, 3.854276543199009*^9}, {
   3.854276574127187*^9, 3.8542765782199097`*^9}, {3.8542766354474163`*^9, 
   3.85427664146631*^9}, 3.8542769855358057`*^9},
 CellLabel->"In[25]:=",ExpressionUUID->"98a383f1-a684-4c3d-9bd1-79003a96e30d"],

Cell[BoxData[
 RowBox[{"-", "15"}]], "Output",
 CellChangeTimes->{3.85427654432594*^9, 3.854276578661787*^9, 
  3.8542766420391827`*^9, 3.854276986076942*^9},
 CellLabel->"Out[26]=",ExpressionUUID->"ff4c1597-e068-4f1e-899b-18518db8af90"],

Cell[BoxData["False"], "Output",
 CellChangeTimes->{3.85427654432594*^9, 3.854276578661787*^9, 
  3.8542766420391827`*^9, 3.854276986078614*^9},
 CellLabel->"Out[27]=",ExpressionUUID->"77e3a79c-7ac1-4459-9eff-b3164ff92fc0"]
}, Open  ]],

Cell["b)", "Text",
 CellChangeTimes->{{3.854276991883057*^9, 
  3.854276992486939*^9}},ExpressionUUID->"66f4faf8-3ef5-45b6-a7dd-\
c751d7fa367b"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"Z", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"9", ",", "6"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"6", ",", "1"}], "}"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Det", "[", "Z", "]"}], "\[IndentingNewLine]", 
 RowBox[{"PositiveDefiniteMatrixQ", "[", "Z", "]"}]}], "Input",
 CellChangeTimes->{{3.854276996393214*^9, 3.854277017709339*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"fa0a574c-e2fc-49ce-b177-db8f36294cb1"],

Cell[BoxData[
 RowBox[{"-", "27"}]], "Output",
 CellChangeTimes->{3.854277018211434*^9},
 CellLabel->"Out[29]=",ExpressionUUID->"e5381eb3-55af-4f5f-85b0-108dcf54517b"],

Cell[BoxData["False"], "Output",
 CellChangeTimes->{3.854277018213072*^9},
 CellLabel->"Out[30]=",ExpressionUUID->"8fcd7589-56fa-4d3c-9d51-f5c9b1b6f9ba"]
}, Open  ]],

Cell["Question 4 a)", "Text",
 CellChangeTimes->{{3.854354493634198*^9, 3.854354494684894*^9}, {
  3.8543659004079027`*^9, 
  3.854365901468454*^9}},ExpressionUUID->"43aed147-f321-4516-9b3e-\
03ee70d1d395"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"pde", " ", "=", " ", 
  RowBox[{
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "t"}], "]"}], ",", "t", ",", "t"}], "]"}], "==", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "t"}], "]"}], ",", "x", ",", "x"}], 
    "]"}]}]}], "\[IndentingNewLine]", 
 RowBox[{"ic", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "0"}], "]"}], "==", 
     RowBox[{"Exp", "[", "x", "]"}]}], ",", "  ", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"Derivative", "[", 
        RowBox[{"0", ",", "1"}], "]"}], "[", "u", "]"}], "[", 
      RowBox[{"x", ",", "0"}], "]"}], "==", 
     RowBox[{"2", 
      RowBox[{"Sin", "[", "x", "]"}], 
      RowBox[{"Cos", "[", "x", "]"}]}]}]}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"DSolve", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"pde", ",", "ic"}], "}"}], ",", 
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "t"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", "t"}], "}"}]}], "]"}], "//", 
  "FullSimplify"}]}], "Input",
 CellChangeTimes->{{3.8543544965433407`*^9, 3.854354649050888*^9}, {
  3.854355490972755*^9, 3.854355491472411*^9}, {3.854365356015645*^9, 
  3.8543653961573*^9}, {3.854365437705035*^9, 3.854365457747101*^9}, {
  3.854365488024519*^9, 3.8543655055203238`*^9}, {3.854365682239663*^9, 
  3.85436569724225*^9}, {3.854365727686376*^9, 3.85436582049507*^9}, {
  3.854366161385*^9, 3.854366161738422*^9}, {3.854435363810626*^9, 
  3.854435385989777*^9}},
 CellLabel->
  "In[111]:=",ExpressionUUID->"796aa0e3-fae3-4557-a454-0550f121a3e5"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["u", 
    TagBox[
     RowBox[{"(", 
      RowBox[{"0", ",", "2"}], ")"}],
     Derivative],
    MultilineFunction->None], "[", 
   RowBox[{"x", ",", "t"}], "]"}], "\[Equal]", 
  RowBox[{
   SuperscriptBox["u", 
    TagBox[
     RowBox[{"(", 
      RowBox[{"2", ",", "0"}], ")"}],
     Derivative],
    MultilineFunction->None], "[", 
   RowBox[{"x", ",", "t"}], "]"}]}]], "Output",
 CellChangeTimes->{{3.8543545201583433`*^9, 3.854354582990905*^9}, {
   3.8543546324629383`*^9, 3.854354650599139*^9}, 3.854365357988627*^9, 
   3.854365403805873*^9, {3.854365454656042*^9, 3.854365458105936*^9}, {
   3.854365488805587*^9, 3.85436550588551*^9}, {3.854365687531096*^9, 
   3.854365697904069*^9}, 3.85436572993082*^9, {3.8543658018785963`*^9, 
   3.854365820935891*^9}, 3.854366162000243*^9, {3.85443536416156*^9, 
   3.854435386305188*^9}},
 CellLabel->
  "Out[111]=",ExpressionUUID->"e5b4c02a-5edb-4833-a541-2d31088a4286"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "0"}], "]"}], "\[Equal]", 
    SuperscriptBox["\[ExponentialE]", "x"]}], ",", 
   RowBox[{
    RowBox[{
     SuperscriptBox["u", 
      TagBox[
       RowBox[{"(", 
        RowBox[{"0", ",", "1"}], ")"}],
       Derivative],
      MultilineFunction->None], "[", 
     RowBox[{"x", ",", "0"}], "]"}], "\[Equal]", 
    RowBox[{"2", " ", 
     RowBox[{"Cos", "[", "x", "]"}], " ", 
     RowBox[{"Sin", "[", "x", "]"}]}]}]}], "}"}]], "Output",
 CellChangeTimes->{{3.8543545201583433`*^9, 3.854354582990905*^9}, {
   3.8543546324629383`*^9, 3.854354650599139*^9}, 3.854365357988627*^9, 
   3.854365403805873*^9, {3.854365454656042*^9, 3.854365458105936*^9}, {
   3.854365488805587*^9, 3.85436550588551*^9}, {3.854365687531096*^9, 
   3.854365697904069*^9}, 3.85436572993082*^9, {3.8543658018785963`*^9, 
   3.854365820935891*^9}, 3.854366162000243*^9, {3.85443536416156*^9, 
   3.8544353863074713`*^9}},
 CellLabel->
  "Out[112]=",ExpressionUUID->"2645239b-bc75-4658-980c-1880a2f49000"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "t"}], "]"}], "\[Rule]", 
    TagBox[GridBox[{
       {"\[Piecewise]", GridBox[{
          {
           RowBox[{
            RowBox[{
             SuperscriptBox["\[ExponentialE]", "x"], " ", 
             RowBox[{"Cosh", "[", "t", "]"}]}], "+", 
            RowBox[{"2", " ", 
             RowBox[{"Cos", "[", "t", "]"}], " ", 
             RowBox[{"Cos", "[", "x", "]"}], " ", 
             RowBox[{"Sin", "[", "t", "]"}], " ", 
             RowBox[{"Sin", "[", "x", "]"}]}]}], 
           RowBox[{"t", "\[GreaterEqual]", "0"}]},
          {"Indeterminate", 
           TagBox["True",
            "PiecewiseDefault",
            AutoDelete->True]}
         },
         AllowedDimensions->{2, Automatic},
         Editable->True,
         GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
         GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.84]}, 
             Offset[0.27999999999999997`]}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}},
         Selectable->True]}
      },
      GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
      GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.35]}, 
          Offset[0.27999999999999997`]}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}}],
     "Piecewise",
     DeleteWithContents->True,
     Editable->False,
     SelectWithContents->True,
     Selectable->False,
     StripWrapperBoxes->True]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.8543545201583433`*^9, 3.854354582990905*^9}, {
   3.8543546324629383`*^9, 3.854354650599139*^9}, 3.854365357988627*^9, 
   3.854365403805873*^9, {3.854365454656042*^9, 3.854365458105936*^9}, {
   3.854365488805587*^9, 3.85436550588551*^9}, {3.854365687531096*^9, 
   3.854365697904069*^9}, 3.85436572993082*^9, {3.8543658018785963`*^9, 
   3.854365820935891*^9}, 3.854366162000243*^9, {3.85443536416156*^9, 
   3.8544353863255463`*^9}},
 CellLabel->
  "Out[113]=",ExpressionUUID->"b4f27efb-ad66-4888-9e70-abd19ac43ae3"]
}, Open  ]],

Cell["Question 4) b)", "Text",
 CellChangeTimes->{{3.854365903712098*^9, 
  3.854365910092024*^9}},ExpressionUUID->"5b9497ef-6021-44e6-9428-\
80d66f420362"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"pde", " ", "=", " ", 
  RowBox[{
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "t"}], "]"}], ",", "t", ",", "t"}], "]"}], "==", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "t"}], "]"}], ",", "x", ",", "x"}], 
    "]"}]}]}], "\[IndentingNewLine]", 
 RowBox[{"ic", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "0"}], "]"}], "==", "1"}], ",", "  ", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"Derivative", "[", 
        RowBox[{"0", ",", "1"}], "]"}], "[", "u", "]"}], "[", 
      RowBox[{"x", ",", "0"}], "]"}], "==", "x"}]}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"DSolveValue", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"pde", ",", "ic"}], "}"}], ",", 
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "t"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", "t"}], "}"}]}], "]"}], "//", 
  "FullSimplify"}]}], "Input",
 CellChangeTimes->{{3.854365924363563*^9, 3.854365927027454*^9}},
 CellLabel->"In[90]:=",ExpressionUUID->"e5b3b7e8-56ac-4811-ab98-d5436d0ade6b"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["u", 
    TagBox[
     RowBox[{"(", 
      RowBox[{"0", ",", "2"}], ")"}],
     Derivative],
    MultilineFunction->None], "[", 
   RowBox[{"x", ",", "t"}], "]"}], "\[Equal]", 
  RowBox[{
   SuperscriptBox["u", 
    TagBox[
     RowBox[{"(", 
      RowBox[{"2", ",", "0"}], ")"}],
     Derivative],
    MultilineFunction->None], "[", 
   RowBox[{"x", ",", "t"}], "]"}]}]], "Output",
 CellChangeTimes->{3.854365928623824*^9},
 CellLabel->"Out[90]=",ExpressionUUID->"185e6ae5-c9a6-4ce1-8699-1ad8e84b24a5"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "0"}], "]"}], "\[Equal]", "1"}], ",", 
   RowBox[{
    RowBox[{
     SuperscriptBox["u", 
      TagBox[
       RowBox[{"(", 
        RowBox[{"0", ",", "1"}], ")"}],
       Derivative],
      MultilineFunction->None], "[", 
     RowBox[{"x", ",", "0"}], "]"}], "\[Equal]", "x"}]}], "}"}]], "Output",
 CellChangeTimes->{3.8543659286260643`*^9},
 CellLabel->"Out[91]=",ExpressionUUID->"15cd7735-e5a6-4517-854e-384ee1b94683"],

Cell[BoxData[
 TagBox[GridBox[{
    {"\[Piecewise]", GridBox[{
       {
        RowBox[{"1", "+", 
         RowBox[{"t", " ", "x"}]}], 
        RowBox[{"t", "\[GreaterEqual]", "0"}]},
       {"Indeterminate", 
        TagBox["True",
         "PiecewiseDefault",
         AutoDelete->True]}
      },
      AllowedDimensions->{2, Automatic},
      Editable->True,
      GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
      GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.84]}, 
          Offset[0.27999999999999997`]}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}},
      Selectable->True]}
   },
   GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
   GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[0.35]}, 
       Offset[0.27999999999999997`]}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}}],
  "Piecewise",
  DeleteWithContents->True,
  Editable->False,
  SelectWithContents->True,
  Selectable->False,
  StripWrapperBoxes->True]], "Output",
 CellChangeTimes->{3.8543659286312113`*^9},
 CellLabel->"Out[92]=",ExpressionUUID->"1f374a80-3921-4b7d-bb40-95d452c5eb8e"]
}, Open  ]]
},
WindowSize->{808, 747},
WindowMargins->{{4, Automatic}, {Automatic, 4}},
FrontEndVersion->"13.0 for Mac OS X ARM (64-bit) (December 2, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"850ddbb8-f8df-4831-83f0-ef0bf07a3bdb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 152, 3, 35, "Text",ExpressionUUID->"962c73b7-e7cb-4125-9565-545f90770866"],
Cell[CellGroupData[{
Cell[735, 27, 1084, 30, 52, "Input",ExpressionUUID->"bc1c6aa5-33d2-4077-ac24-b20e8e8fc727"],
Cell[1822, 59, 955, 31, 34, "Output",ExpressionUUID->"2f42f33e-761a-4c8a-9abf-55245ea63e3d"],
Cell[2780, 92, 520, 16, 34, "Output",ExpressionUUID->"dd65046c-2c2c-486a-95a4-1629431b7b68"]
}, Open  ]],
Cell[3315, 111, 154, 3, 35, "Text",ExpressionUUID->"8bc7baeb-ecff-4f10-94a8-ab19064e48dc"],
Cell[CellGroupData[{
Cell[3494, 118, 1192, 36, 73, "Input",ExpressionUUID->"0ea3f137-092a-47df-9097-b4417e444eea"],
Cell[4689, 156, 749, 23, 34, "Output",ExpressionUUID->"b7990ac9-75af-410c-9ade-52d89fd9c3af"],
Cell[5441, 181, 539, 15, 57, "Output",ExpressionUUID->"c4f5dcc1-c68a-4f3e-90ec-3a1eda77b4f0"],
Cell[5983, 198, 354, 8, 34, "Output",ExpressionUUID->"3a08d66e-ad41-4a53-bc47-e2c7040a0399"]
}, Open  ]],
Cell[6352, 209, 158, 3, 35, "Text",ExpressionUUID->"b76c13aa-d204-43cb-b122-25c366b5c573"],
Cell[CellGroupData[{
Cell[6535, 216, 679, 17, 73, "Input",ExpressionUUID->"98a383f1-a684-4c3d-9bd1-79003a96e30d"],
Cell[7217, 235, 237, 4, 34, "Output",ExpressionUUID->"ff4c1597-e068-4f1e-899b-18518db8af90"],
Cell[7457, 241, 223, 3, 34, "Output",ExpressionUUID->"77e3a79c-7ac1-4459-9eff-b3164ff92fc0"]
}, Open  ]],
Cell[7695, 247, 144, 3, 35, "Text",ExpressionUUID->"66f4faf8-3ef5-45b6-a7dd-c751d7fa367b"],
Cell[CellGroupData[{
Cell[7864, 254, 503, 12, 73, "Input",ExpressionUUID->"fa0a574c-e2fc-49ce-b177-db8f36294cb1"],
Cell[8370, 268, 167, 3, 34, "Output",ExpressionUUID->"e5381eb3-55af-4f5f-85b0-108dcf54517b"],
Cell[8540, 273, 153, 2, 34, "Output",ExpressionUUID->"8fcd7589-56fa-4d3c-9d51-f5c9b1b6f9ba"]
}, Open  ]],
Cell[8708, 278, 206, 4, 35, "Text",ExpressionUUID->"43aed147-f321-4516-9b3e-03ee70d1d395"],
Cell[CellGroupData[{
Cell[8939, 286, 1673, 46, 73, "Input",ExpressionUUID->"796aa0e3-fae3-4557-a454-0550f121a3e5"],
Cell[10615, 334, 975, 26, 34, "Output",ExpressionUUID->"e5b4c02a-5edb-4833-a541-2d31088a4286"],
Cell[11593, 362, 1079, 27, 37, "Output",ExpressionUUID->"2645239b-bc75-4658-980c-1880a2f49000"],
Cell[12675, 391, 2386, 60, 53, "Output",ExpressionUUID->"b4f27efb-ad66-4888-9e70-abd19ac43ae3"]
}, Open  ]],
Cell[15076, 454, 156, 3, 35, "Text",ExpressionUUID->"5b9497ef-6021-44e6-9428-80d66f420362"],
Cell[CellGroupData[{
Cell[15257, 461, 1168, 36, 73, "Input",ExpressionUUID->"e5b3b7e8-56ac-4811-ab98-d5436d0ade6b"],
Cell[16428, 499, 556, 19, 34, "Output",ExpressionUUID->"185e6ae5-c9a6-4ce1-8699-1ad8e84b24a5"],
Cell[16987, 520, 519, 16, 37, "Output",ExpressionUUID->"15cd7735-e5a6-4517-854e-384ee1b94683"],
Cell[17509, 538, 1401, 41, 51, "Output",ExpressionUUID->"1f374a80-3921-4b7d-bb40-95d452c5eb8e"]
}, Open  ]]
}
]
*)

