(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     33918,        895]
NotebookOptionsPosition[     29782,        828]
NotebookOutlinePosition[     30180,        844]
CellTagsIndexPosition[     30137,        841]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["Question 1", "Text",
 CellChangeTimes->{{3.856263869048462*^9, 
  3.8562638737896748`*^9}},ExpressionUUID->"978eee93-6265-4c46-a062-\
aad0e0b9eedd"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"MatrixRank", "[", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"1", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", 
      RowBox[{"-", "2"}]}], "}"}]}], "}"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{"u", ",", 
    RowBox[{"u", "-", 
     RowBox[{"2", "v"}]}], ",", 
    RowBox[{
     RowBox[{"2", 
      RowBox[{"u", "^", "2"}]}], "-", 
     RowBox[{"v", "^", "2"}]}]}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fu", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", "u"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fv", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", "v"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"Dot", "[", 
  RowBox[{
   RowBox[{"fu", "[", 
    RowBox[{"u", ",", "v"}], "]"}], ",", 
   RowBox[{"fu", "[", 
    RowBox[{"u", ",", "v"}], "]"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Dot", "[", 
  RowBox[{
   RowBox[{"fu", "[", 
    RowBox[{"u", ",", "v"}], "]"}], ",", 
   RowBox[{"fv", "[", 
    RowBox[{"u", ",", "v"}], "]"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Dot", "[", 
   RowBox[{
    RowBox[{"fv", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", 
    RowBox[{"fv", "[", 
     RowBox[{"u", ",", "v"}], "]"}]}], "]"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{" ", "iii", ")"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"p", "=", 
  RowBox[{"f", "[", 
   RowBox[{
    RowBox[{"-", "1"}], ",", 
    RowBox[{"-", "1"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"{", 
  RowBox[{
   RowBox[{"fu", "[", 
    RowBox[{
     RowBox[{"-", "1"}], ",", 
     RowBox[{"-", "1"}]}], "]"}], ",", 
   RowBox[{"fv", "[", 
    RowBox[{
     RowBox[{"-", "1"}], ",", 
     RowBox[{"-", "1"}]}], "]"}]}], "}"}], "\[IndentingNewLine]", 
 RowBox[{"Cross", "[", 
  RowBox[{
   RowBox[{"fu", "[", 
    RowBox[{
     RowBox[{"-", "1"}], ",", 
     RowBox[{"-", "1"}]}], "]"}], ",", 
   RowBox[{"fv", "[", 
    RowBox[{
     RowBox[{"-", "1"}], ",", 
     RowBox[{"-", "1"}]}], "]"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Norm", "[", 
  RowBox[{"Cross", "[", 
   RowBox[{
    RowBox[{"fu", "[", 
     RowBox[{
      RowBox[{"-", "1"}], ",", 
      RowBox[{"-", "1"}]}], "]"}], ",", 
    RowBox[{"fv", "[", 
     RowBox[{
      RowBox[{"-", "1"}], ",", 
      RowBox[{"-", "1"}]}], "]"}]}], "]"}], "]"}]}], "Input",
 CellChangeTimes->{{3.8562625067722673`*^9, 3.856262519921455*^9}, {
  3.856263877200881*^9, 3.8562640605070267`*^9}, {3.856264116587554*^9, 
  3.8562641465188913`*^9}, {3.8562652195437098`*^9, 3.856265221783822*^9}, {
  3.856265254025194*^9, 3.856265283764111*^9}, {3.856265326099972*^9, 
  3.85626532919869*^9}, {3.856265385023831*^9, 3.856265424718145*^9}, {
  3.856266073749379*^9, 3.856266117501996*^9}},
 CellLabel->
  "In[521]:=",ExpressionUUID->"6ed8119d-b300-4862-b648-5b9d32f05853"],

Cell[BoxData["2"], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117855324*^9}},
 CellLabel->
  "Out[521]=",ExpressionUUID->"95755856-8509-47ee-bb30-689f616549bc"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", "1", ",", 
   RowBox[{"4", " ", "u"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117857139*^9}},
 CellLabel->
  "Out[523]=",ExpressionUUID->"fd72e2df-d748-4e1a-b6d8-690fb2d450fd"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", 
   RowBox[{"-", "2"}], ",", 
   RowBox[{
    RowBox[{"-", "2"}], " ", "v"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117858885*^9}},
 CellLabel->
  "Out[524]=",ExpressionUUID->"aa65ba51-1e2e-42f1-a463-b920de06f1c5"],

Cell[BoxData[
 RowBox[{"2", "+", 
  RowBox[{"16", " ", 
   SuperscriptBox["u", "2"]}]}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117860417*^9}},
 CellLabel->
  "Out[525]=",ExpressionUUID->"788198a9-9adc-4bd4-b45c-c8e5e9f44c6d"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "2"}], "-", 
  RowBox[{"8", " ", "u", " ", "v"}]}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.8562661178623753`*^9}},
 
 CellLabel->
  "Out[526]=",ExpressionUUID->"95093fef-176c-458e-a8e9-cae44987cc5e"],

Cell[BoxData[
 RowBox[{"4", "+", 
  RowBox[{"4", " ", 
   SuperscriptBox["v", "2"]}]}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117863822*^9}},
 CellLabel->
  "Out[527]=",ExpressionUUID->"c0473503-d303-47cf-8726-c98fd9283089"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "1"}], ",", "1", ",", "1"}], "}"}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117865059*^9}},
 CellLabel->
  "Out[528]=",ExpressionUUID->"cd2e70f4-d3ed-44c7-8e6c-511ba238dbe0"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "1", ",", 
     RowBox[{"-", "4"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{"-", "2"}], ",", "2"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117866341*^9}},
 CellLabel->
  "Out[529]=",ExpressionUUID->"e42c62e3-a1a2-4f37-ac7e-cd820f1f0464"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "6"}], ",", 
   RowBox[{"-", "2"}], ",", 
   RowBox[{"-", "2"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.8562661178678493`*^9}},
 
 CellLabel->
  "Out[530]=",ExpressionUUID->"1d774262-7672-407d-8f9a-0c10b50a281a"],

Cell[BoxData[
 RowBox[{"2", " ", 
  SqrtBox["11"]}]], "Output",
 CellChangeTimes->{
  3.856262523558015*^9, {3.85626391436858*^9, 3.856263919444313*^9}, {
   3.856263978607334*^9, 3.856264060944906*^9}, {3.856264137143963*^9, 
   3.856264147674863*^9}, 3.8562652649355497`*^9, {3.856265330212695*^9, 
   3.8562653324400663`*^9}, {3.8562654164301434`*^9, 
   3.8562654249837933`*^9}, {3.8562660807698097`*^9, 3.856266117869297*^9}},
 CellLabel->
  "Out[531]=",ExpressionUUID->"f5382ce7-45d7-4fea-9282-16c761c5fde6"]
}, Open  ]],

Cell["Question 3", "Text",
 CellChangeTimes->{{3.8563312279243193`*^9, 
  3.8563312294753733`*^9}},ExpressionUUID->"7af14335-6a5b-4961-88c2-\
86cdd18f47cf"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{"{", 
    RowBox[{"_", "\[Element]", "Reals"}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{
     RowBox[{"Cosh", "[", "u", "]"}], 
     RowBox[{"Cos", "[", "v", "]"}]}], ",", 
    RowBox[{
     RowBox[{"Cosh", "[", "u", "]"}], 
     RowBox[{"Sin", "[", "v", "]"}]}], ",", "u"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"fu", "[", 
    RowBox[{"u_", ",", "v_"}], "]"}], "=", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"f", "[", 
      RowBox[{"u", ",", "v"}], "]"}], ",", "u"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fv", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", "v"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fE", "=", 
   RowBox[{
    RowBox[{"Dot", "[", 
     RowBox[{
      RowBox[{"fu", "[", 
       RowBox[{"u", ",", "v"}], "]"}], ",", 
      RowBox[{"fu", "[", 
       RowBox[{"u", ",", "v"}], "]"}]}], "]"}], "//", "Simplify"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"F", "=", 
   RowBox[{"Dot", "[", 
    RowBox[{
     RowBox[{"fu", "[", 
      RowBox[{"u", ",", "v"}], "]"}], ",", 
     RowBox[{"fv", "[", 
      RowBox[{"u", ",", "v"}], "]"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"G", "=", 
    RowBox[{
     RowBox[{"Dot", "[", 
      RowBox[{
       RowBox[{"fv", "[", 
        RowBox[{"u", ",", "v"}], "]"}], ",", 
       RowBox[{"fv", "[", 
        RowBox[{"u", ",", "v"}], "]"}]}], "]"}], "//", "FullSimplify"}]}], 
   ";"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"Norm", "[", 
     RowBox[{
      RowBox[{"Cross", "[", 
       RowBox[{
        RowBox[{"fu", "[", 
         RowBox[{"u", ",", "v"}], "]"}], ",", 
        RowBox[{"fv", "[", 
         RowBox[{"u", ",", "v"}], "]"}]}], "]"}], ",", "2"}], "]"}], "//", 
    "FullSimplify"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a", " ", "=", 
   RowBox[{
    RowBox[{"Sqrt", "[", 
     RowBox[{
      RowBox[{"fE", "*", "G"}], "-", 
      RowBox[{"F", "^", "2"}]}], "]"}], "//", "FullSimplify"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"a", ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"temp", " ", "=", 
   RowBox[{"Integrate", "[", 
    RowBox[{"a", ",", 
     RowBox[{"{", 
      RowBox[{"u", ",", "0", ",", "1"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Integrate", "[", 
  RowBox[{"temp", ",", 
   RowBox[{"{", 
    RowBox[{"v", ",", "0", ",", "Pi"}], "}"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.856331231252945*^9, 3.856331269457802*^9}, {
   3.8563313158095016`*^9, 3.856331354456429*^9}, {3.8563314991179657`*^9, 
   3.856331515296102*^9}, {3.856331892412562*^9, 3.856331904793446*^9}, {
   3.856331939127502*^9, 3.856331942188229*^9}, {3.856332055374404*^9, 
   3.856332056385461*^9}, {3.856332296241643*^9, 3.856332355417903*^9}, {
   3.856332394773589*^9, 3.856332446247501*^9}, {3.856349167644857*^9, 
   3.8563492007990017`*^9}, {3.856349406627281*^9, 3.856349410724558*^9}, {
   3.8563495131898527`*^9, 3.856349604932722*^9}, 3.856349728937928*^9, {
   3.856349772375147*^9, 3.8563498166117983`*^9}, {3.856350024408148*^9, 
   3.8563500284454737`*^9}, {3.8563513045774193`*^9, 3.856351305638815*^9}, {
   3.856351689959977*^9, 3.856351802508317*^9}, {3.856351838114048*^9, 
   3.856351853224106*^9}, {3.85635201701295*^9, 3.8563520173816442`*^9}, {
   3.856352051067977*^9, 3.85635206703902*^9}, {3.856352156583726*^9, 
   3.8563521674509478`*^9}, {3.8563521985924*^9, 3.85635220007729*^9}, {
   3.856352646967537*^9, 3.8563526484035892`*^9}, {3.856373574985403*^9, 
   3.856373638354259*^9}, {3.856373864936021*^9, 3.856373868300042*^9}, {
   3.856373946875414*^9, 3.856373971755541*^9}, 3.856374170173773*^9, {
   3.856374596784876*^9, 3.856374597921537*^9}, {3.856374728348632*^9, 
   3.856374736563244*^9}, {3.856374834843416*^9, 3.856374835907465*^9}, {
   3.856374917726742*^9, 3.8563749312892017`*^9}, {3.856374996746279*^9, 
   3.856374998278202*^9}, 3.8563750487190657`*^9, {3.856375161329483*^9, 
   3.8563751754131413`*^9}, {3.856375216412359*^9, 3.856375218749277*^9}, {
   3.8563752657942*^9, 3.856375315220644*^9}, {3.857108987251411*^9, 
   3.857109020358872*^9}, {3.8571096147234488`*^9, 3.857109727959893*^9}, 
   3.857109868415924*^9, {3.8571099168413363`*^9, 3.8571099391686707`*^9}, {
   3.8571100456937532`*^9, 3.857110061093549*^9}, {3.857110150369357*^9, 
   3.857110219264291*^9}},
 CellLabel->
  "In[312]:=",ExpressionUUID->"157dc97b-fa30-48b0-9460-5a7f292d710a"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     RowBox[{"Cosh", "[", "u", "]"}]}], " ", 
    RowBox[{"Sin", "[", "v", "]"}]}], ",", 
   RowBox[{
    RowBox[{"Cos", "[", "v", "]"}], " ", 
    RowBox[{"Cosh", "[", "u", "]"}]}], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{{3.856349197351861*^9, 3.856349201126796*^9}, {
   3.8563494084338093`*^9, 3.856349410937643*^9}, {3.856349522780204*^9, 
   3.8563495378697987`*^9}, {3.856349578189625*^9, 3.8563496051943703`*^9}, 
   3.856349729261414*^9, {3.8563497762345057`*^9, 3.8563498173644*^9}, 
   3.8563500288602543`*^9, {3.8563517521730347`*^9, 3.856351802817531*^9}, {
   3.856351842684967*^9, 3.856351853707308*^9}, 3.8563520180135612`*^9, 
   3.8563521227059813`*^9, 3.856352168616426*^9, 3.856352437480351*^9, {
   3.856373591962582*^9, 3.856373666156045*^9}, {3.8563739236630993`*^9, 
   3.856373972498856*^9}, 3.856374171125902*^9, 3.8563745984424334`*^9, 
   3.856374737473948*^9, 3.856374836390967*^9, {3.856374920144092*^9, 
   3.8563749315021276`*^9}, 3.856374998728548*^9, 3.856375049109561*^9, {
   3.856375165227603*^9, 3.85637517607458*^9}, 3.856375218978422*^9, {
   3.8563752757464933`*^9, 3.856375282146254*^9}, 3.8563753161164227`*^9, 
   3.8571087822327423`*^9, {3.8571090120670156`*^9, 3.857109020601804*^9}, {
   3.857109619524691*^9, 3.857109728213459*^9}, 3.8571098698307867`*^9, {
   3.8571099171991796`*^9, 3.857109939499755*^9}, {3.8571100558306913`*^9, 
   3.85711006161371*^9}, {3.857110151841537*^9, 3.857110184229501*^9}, {
   3.8571102156226797`*^9, 3.8571102195446253`*^9}},
 CellLabel->
  "Out[315]=",ExpressionUUID->"4c00913b-82cf-4aa8-9a39-e149ccefa345"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", "\[Pi]", " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", 
    RowBox[{
     RowBox[{"Cosh", "[", "1", "]"}], " ", 
     RowBox[{"Sinh", "[", "1", "]"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.856349197351861*^9, 3.856349201126796*^9}, {
   3.8563494084338093`*^9, 3.856349410937643*^9}, {3.856349522780204*^9, 
   3.8563495378697987`*^9}, {3.856349578189625*^9, 3.8563496051943703`*^9}, 
   3.856349729261414*^9, {3.8563497762345057`*^9, 3.8563498173644*^9}, 
   3.8563500288602543`*^9, {3.8563517521730347`*^9, 3.856351802817531*^9}, {
   3.856351842684967*^9, 3.856351853707308*^9}, 3.8563520180135612`*^9, 
   3.8563521227059813`*^9, 3.856352168616426*^9, 3.856352437480351*^9, {
   3.856373591962582*^9, 3.856373666156045*^9}, {3.8563739236630993`*^9, 
   3.856373972498856*^9}, 3.856374171125902*^9, 3.8563745984424334`*^9, 
   3.856374737473948*^9, 3.856374836390967*^9, {3.856374920144092*^9, 
   3.8563749315021276`*^9}, 3.856374998728548*^9, 3.856375049109561*^9, {
   3.856375165227603*^9, 3.85637517607458*^9}, 3.856375218978422*^9, {
   3.8563752757464933`*^9, 3.856375282146254*^9}, 3.8563753161164227`*^9, 
   3.8571087822327423`*^9, {3.8571090120670156`*^9, 3.857109020601804*^9}, {
   3.857109619524691*^9, 3.857109728213459*^9}, 3.8571098698307867`*^9, {
   3.8571099171991796`*^9, 3.857109939499755*^9}, {3.8571100558306913`*^9, 
   3.85711006161371*^9}, {3.857110151841537*^9, 3.857110184229501*^9}, {
   3.8571102156226797`*^9, 3.857110219725932*^9}},
 CellLabel->
  "Out[322]=",ExpressionUUID->"b1da9366-a853-4b67-92b0-f543945cdf67"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Out", "[", "866", "]"}], "//", "OutputForm"}]], "Input",
 CellChangeTimes->{{3.856352657973503*^9, 3.8563526710679483`*^9}, {
  3.856352706033793*^9, 3.856352707341776*^9}},
 CellLabel->
  "In[871]:=",ExpressionUUID->"8c4b501d-b210-44ee-abab-d032b5db0beb"],

Cell[OutputFormData["\<\
(Sinh[u]*Sqrt[Cosh[u]^2*(Cos[2*v] + Cosh[2*v]) - 
    Sin[2*v]*Sinh[u]^2*Sinh[2*v]])/Sqrt[2]\
\>", "\<\
                     2
(Sinh[u] Sqrt[Cosh[u]  (Cos[2 v] + Cosh[2 v]) - 
 
                      2
      Sin[2 v] Sinh[u]  Sinh[2 v]]) / Sqrt[2]\
\>"], "Output",
 CellChangeTimes->{3.8563526714110823`*^9, 3.856352707661471*^9},
 CellLabel->
  "Out[871]//OutputForm=",ExpressionUUID->"d573a9ef-c4ba-4adc-8237-\
590df77ce9a3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{
   RowBox[{"f", "[", 
    RowBox[{"u", ",", "v"}], "]"}], ",", "v"}], "]"}]], "Input",
 CellChangeTimes->{{3.857110120986288*^9, 3.857110137507434*^9}},
 CellLabel->
  "In[256]:=",ExpressionUUID->"df4a2eb8-1c46-4bdf-b15e-b99e536da80e"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     RowBox[{"Cosh", "[", "u", "]"}]}], " ", 
    RowBox[{"Sin", "[", "v", "]"}]}], ",", 
   RowBox[{
    RowBox[{"Cos", "[", "v", "]"}], " ", 
    RowBox[{"Cosh", "[", "u", "]"}]}], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{{3.8571101240521917`*^9, 3.857110138161008*^9}},
 CellLabel->
  "Out[256]=",ExpressionUUID->"d3b2b5a0-7621-468a-862a-c5ce8499532e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.856352663040607*^9, 
  3.856352663909132*^9}},ExpressionUUID->"119099d7-c5de-4065-bbb1-\
91a9adc811f1"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", 
   SqrtBox["2"]], 
  RowBox[{
   RowBox[{"Sinh", "[", "u", "]"}], " ", 
   RowBox[{"\[Sqrt]", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       SuperscriptBox[
        RowBox[{"Cosh", "[", "u", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"2", " ", "v"}], "]"}], "+", 
         RowBox[{"Cosh", "[", 
          RowBox[{"2", " ", "v"}], "]"}]}], ")"}]}], "-", 
      RowBox[{
       RowBox[{"Sin", "[", 
        RowBox[{"2", " ", "v"}], "]"}], " ", 
       SuperscriptBox[
        RowBox[{"Sinh", "[", "u", "]"}], "2"], " ", 
       RowBox[{"Sinh", "[", 
        RowBox[{"2", " ", "v"}], "]"}]}]}], ")"}]}]}]}]], "Output",
 CellChangeTimes->{3.856352661049667*^9},
 CellLabel->
  "Out[869]=",ExpressionUUID->"caa78133-1dab-4ed1-930a-5fb1f3d98cab"]
}, Open  ]],

Cell["Question 4", "Text",
 CellChangeTimes->{{3.856354513268071*^9, 
  3.8563545163456507`*^9}},ExpressionUUID->"dcb22dba-dd90-4952-b388-\
c01022b7e1ae"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"v", "*", 
     RowBox[{"Cos", "[", "u", "]"}]}], ",", 
    RowBox[{"v", "*", 
     RowBox[{"Sin", "[", "u", "]"}]}], ",", "v"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{"fu", " ", "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"u", ",", "v"}], "]"}], "\[InvisibleComma]", "u"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"fv", " ", "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"u", ",", "v"}], "]"}], "\[InvisibleComma]", "v"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Dot", "[", 
   RowBox[{"fv", ",", "fv"}], "]"}], "//", "Simplify"}]}], "Input",
 CellChangeTimes->{{3.856354519025158*^9, 3.856354579137228*^9}, {
  3.856354619304796*^9, 3.856354666111731*^9}, {3.856354739992845*^9, 
  3.8563547417812433`*^9}, {3.8563549008173237`*^9, 3.856354974735594*^9}, {
  3.856355035573187*^9, 3.856355043987925*^9}},
 CellLabel->
  "In[954]:=",ExpressionUUID->"59bc581f-fbff-4bed-9ea4-ee55e013ce3e"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", "v"}], " ", 
    RowBox[{"Sin", "[", "u", "]"}]}], ",", 
   RowBox[{"v", " ", 
    RowBox[{"Cos", "[", "u", "]"}]}], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{{3.8563546626300097`*^9, 3.856354666476264*^9}, 
   3.8563547421814127`*^9, {3.8563549056791286`*^9, 3.8563550029298563`*^9}, {
   3.856355036324341*^9, 3.8563550443211117`*^9}, 3.856355634204438*^9},
 CellLabel->
  "Out[955]=",ExpressionUUID->"0e250b2d-1c84-4ee6-85c0-80d467a2442a"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Cos", "[", "u", "]"}], ",", 
   RowBox[{"Sin", "[", "u", "]"}], ",", "1"}], "}"}]], "Output",
 CellChangeTimes->{{3.8563546626300097`*^9, 3.856354666476264*^9}, 
   3.8563547421814127`*^9, {3.8563549056791286`*^9, 3.8563550029298563`*^9}, {
   3.856355036324341*^9, 3.8563550443211117`*^9}, 3.856355634207698*^9},
 CellLabel->
  "Out[956]=",ExpressionUUID->"018115c7-c09e-4645-8df9-3e4603892971"],

Cell[BoxData["2"], "Output",
 CellChangeTimes->{{3.8563546626300097`*^9, 3.856354666476264*^9}, 
   3.8563547421814127`*^9, {3.8563549056791286`*^9, 3.8563550029298563`*^9}, {
   3.856355036324341*^9, 3.8563550443211117`*^9}, 3.856355634210474*^9},
 CellLabel->
  "Out[957]=",ExpressionUUID->"0cdb88df-d17a-40a9-b876-434edc7b2be5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"g", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"v", "*", 
     RowBox[{"Sqrt", "[", "2", "]"}], 
     RowBox[{"Cos", "[", 
      RowBox[{"u", "/", 
       RowBox[{"Sqrt", "[", "2", "]"}]}], "]"}]}], ",", 
    RowBox[{"v", "*", 
     RowBox[{"Sqrt", "[", "2", "]"}], 
     RowBox[{"Sin", "[", 
      RowBox[{"u", "/", 
       RowBox[{"Sqrt", "[", "2", "]"}]}], "]"}]}], ",", "0"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{"gu", "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"g", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", "u"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"gv", " ", "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"g", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", "v"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Dot", "[", 
   RowBox[{"gv", ",", "gv"}], "]"}], "//", "Simplify"}]}], "Input",
 CellChangeTimes->{{3.856355068399938*^9, 3.8563551323666573`*^9}, {
  3.856355242456192*^9, 3.8563552526160097`*^9}, {3.856355462228025*^9, 
  3.856355522016541*^9}, {3.856355625751648*^9, 3.856355632055633*^9}},
 CellLabel->
  "In[958]:=",ExpressionUUID->"55d74664-14d9-4f28-a946-0ef0a7a9f67c"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", "v"}], " ", 
    RowBox[{"Sin", "[", 
     FractionBox["u", 
      SqrtBox["2"]], "]"}]}], ",", 
   RowBox[{"v", " ", 
    RowBox[{"Cos", "[", 
     FractionBox["u", 
      SqrtBox["2"]], "]"}]}], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{
  3.8563552530224113`*^9, 3.856355469318973*^9, {3.85635550360476*^9, 
   3.856355522315547*^9}, {3.856355627792432*^9, 3.8563556359227133`*^9}},
 CellLabel->
  "Out[959]=",ExpressionUUID->"1ea9830b-3fed-4e84-ab59-fcbf8c362afb"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    SqrtBox["2"], " ", 
    RowBox[{"Cos", "[", 
     FractionBox["u", 
      SqrtBox["2"]], "]"}]}], ",", 
   RowBox[{
    SqrtBox["2"], " ", 
    RowBox[{"Sin", "[", 
     FractionBox["u", 
      SqrtBox["2"]], "]"}]}], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{
  3.8563552530224113`*^9, 3.856355469318973*^9, {3.85635550360476*^9, 
   3.856355522315547*^9}, {3.856355627792432*^9, 3.856355635926115*^9}},
 CellLabel->
  "Out[960]=",ExpressionUUID->"c66bffdf-1d5d-4051-8b93-1eeb38ba89d6"],

Cell[BoxData["2"], "Output",
 CellChangeTimes->{
  3.8563552530224113`*^9, 3.856355469318973*^9, {3.85635550360476*^9, 
   3.856355522315547*^9}, {3.856355627792432*^9, 3.856355635928928*^9}},
 CellLabel->
  "Out[961]=",ExpressionUUID->"6ee27204-70f5-4cd8-bf96-6aac01b20edd"]
}, Open  ]],

Cell["Question 5", "Text",
 CellChangeTimes->{{3.856365304582588*^9, 
  3.856365307045498*^9}},ExpressionUUID->"c6056940-178a-4871-86d6-\
98adee7c574a"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"u_", ",", "v_"}], "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"v", "-", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"v", "^", "3"}], ")"}], "/", "3"}], "+", 
     RowBox[{"v", "*", 
      RowBox[{"u", "^", "2"}]}]}], ",", 
    RowBox[{"u", "-", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"u", "^", "3"}], ")"}], "/", "3"}], "+", 
     RowBox[{"u", "*", 
      RowBox[{"v", "^", "2"}]}]}], ",", 
    RowBox[{
     RowBox[{"u", "^", "2"}], "-", 
     RowBox[{"v", "^", "2"}]}]}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{"fu", "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", "u"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"fv", "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"u", ",", "v"}], "]"}], ",", "v"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"fE", " ", "=", 
  RowBox[{
   RowBox[{"Dot", "[", 
    RowBox[{"fu", ",", "fu"}], "]"}], "//", 
   "Simplify"}]}], "\[IndentingNewLine]", 
 RowBox[{"F", "=", 
  RowBox[{
   RowBox[{"Dot", "[", 
    RowBox[{"fu", ",", "fv"}], "]"}], "//", 
   "Simplify"}]}], "\[IndentingNewLine]", 
 RowBox[{"G", "=", 
  RowBox[{
   RowBox[{"Dot", "[", 
    RowBox[{"fv", ",", "fv"}], "]"}], "//", "Simplify"}]}]}], "Input",
 CellChangeTimes->{{3.856365311194796*^9, 3.856365401726612*^9}, {
  3.85636552013764*^9, 3.856365560733316*^9}, {3.856365660330295*^9, 
  3.8563656715776577`*^9}, {3.8563657280152283`*^9, 3.856365735682379*^9}},
 CellLabel->
  "In[1011]:=",ExpressionUUID->"ebc29825-763a-4fee-8b06-61788e19e981"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"2", " ", "u", " ", "v"}], ",", 
   RowBox[{"1", "-", 
    SuperscriptBox["u", "2"], "+", 
    SuperscriptBox["v", "2"]}], ",", 
   RowBox[{"2", " ", "u"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.856365366844347*^9, 3.85636540229515*^9}, {
  3.85636552514089*^9, 3.856365561461986*^9}, {3.8563656683873587`*^9, 
  3.856365671927577*^9}, {3.856365732831313*^9, 3.856365736091579*^9}},
 CellLabel->
  "Out[1012]=",ExpressionUUID->"7d09e271-30a7-4370-82de-18a43b5089ad"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"1", "+", 
    SuperscriptBox["u", "2"], "-", 
    SuperscriptBox["v", "2"]}], ",", 
   RowBox[{"2", " ", "u", " ", "v"}], ",", 
   RowBox[{
    RowBox[{"-", "2"}], " ", "v"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.856365366844347*^9, 3.85636540229515*^9}, {
  3.85636552514089*^9, 3.856365561461986*^9}, {3.8563656683873587`*^9, 
  3.856365671927577*^9}, {3.856365732831313*^9, 3.856365736094545*^9}},
 CellLabel->
  "Out[1013]=",ExpressionUUID->"85393e7b-4ca7-4b42-8866-2383eb295565"],

Cell[BoxData[
 SuperscriptBox[
  RowBox[{"(", 
   RowBox[{"1", "+", 
    SuperscriptBox["u", "2"], "+", 
    SuperscriptBox["v", "2"]}], ")"}], "2"]], "Output",
 CellChangeTimes->{{3.856365366844347*^9, 3.85636540229515*^9}, {
  3.85636552514089*^9, 3.856365561461986*^9}, {3.8563656683873587`*^9, 
  3.856365671927577*^9}, {3.856365732831313*^9, 3.856365736097598*^9}},
 CellLabel->
  "Out[1014]=",ExpressionUUID->"103b77a6-d4be-465f-a6a8-f4157332600b"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.856365366844347*^9, 3.85636540229515*^9}, {
  3.85636552514089*^9, 3.856365561461986*^9}, {3.8563656683873587`*^9, 
  3.856365671927577*^9}, {3.856365732831313*^9, 3.856365736100539*^9}},
 CellLabel->
  "Out[1015]=",ExpressionUUID->"91a871b3-1325-4e77-afd8-d25783474524"],

Cell[BoxData[
 SuperscriptBox[
  RowBox[{"(", 
   RowBox[{"1", "+", 
    SuperscriptBox["u", "2"], "+", 
    SuperscriptBox["v", "2"]}], ")"}], "2"]], "Output",
 CellChangeTimes->{{3.856365366844347*^9, 3.85636540229515*^9}, {
  3.85636552514089*^9, 3.856365561461986*^9}, {3.8563656683873587`*^9, 
  3.856365671927577*^9}, {3.856365732831313*^9, 3.856365736102714*^9}},
 CellLabel->
  "Out[1016]=",ExpressionUUID->"16e94c8a-47f0-410c-80e4-3122bfb79da1"]
}, Open  ]]
},
WindowSize->{Full, Full},
WindowMargins->{{207, Automatic}, {4, Automatic}},
FrontEndVersion->"13.0 for Mac OS X ARM (64-bit) (December 2, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"750b7076-d1c6-4963-aaa9-3668e854893b"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 154, 3, 35, "Text",ExpressionUUID->"978eee93-6265-4c46-a062-aad0e0b9eedd"],
Cell[CellGroupData[{
Cell[737, 27, 3143, 100, 262, "Input",ExpressionUUID->"6ed8119d-b300-4862-b648-5b9d32f05853"],
Cell[3883, 129, 479, 8, 34, "Output",ExpressionUUID->"95755856-8509-47ee-bb30-689f616549bc"],
Cell[4365, 139, 558, 11, 34, "Output",ExpressionUUID->"fd72e2df-d748-4e1a-b6d8-690fb2d450fd"],
Cell[4926, 152, 597, 13, 34, "Output",ExpressionUUID->"aa65ba51-1e2e-42f1-a463-b920de06f1c5"],
Cell[5526, 167, 550, 11, 34, "Output",ExpressionUUID->"788198a9-9adc-4bd4-b45c-c8e5e9f44c6d"],
Cell[6079, 180, 556, 12, 34, "Output",ExpressionUUID->"95093fef-176c-458e-a8e9-cae44987cc5e"],
Cell[6638, 194, 549, 11, 34, "Output",ExpressionUUID->"c0473503-d303-47cf-8726-c98fd9283089"],
Cell[7190, 207, 553, 11, 34, "Output",ExpressionUUID->"cd2e70f4-d3ed-44c7-8e6c-511ba238dbe0"],
Cell[7746, 220, 684, 16, 34, "Output",ExpressionUUID->"e42c62e3-a1a2-4f37-ac7e-cd820f1f0464"],
Cell[8433, 238, 595, 14, 34, "Output",ExpressionUUID->"1d774262-7672-407d-8f9a-0c10b50a281a"],
Cell[9031, 254, 514, 10, 35, "Output",ExpressionUUID->"f5382ce7-45d7-4fea-9282-16c761c5fde6"]
}, Open  ]],
Cell[9560, 267, 156, 3, 35, "Text",ExpressionUUID->"7af14335-6a5b-4961-88c2-86cdd18f47cf"],
Cell[CellGroupData[{
Cell[9741, 274, 4772, 123, 262, "Input",ExpressionUUID->"157dc97b-fa30-48b0-9460-5a7f292d710a"],
Cell[14516, 399, 1669, 29, 34, "Output",ExpressionUUID->"4c00913b-82cf-4aa8-9a39-e149ccefa345"],
Cell[16188, 430, 1610, 27, 97, "Output",ExpressionUUID->"b1da9366-a853-4b67-92b0-f543945cdf67"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17835, 462, 291, 6, 30, "Input",ExpressionUUID->"8c4b501d-b210-44ee-abab-d032b5db0beb"],
Cell[18129, 470, 452, 13, 94, "Output",ExpressionUUID->"d573a9ef-c4ba-4adc-8237-590df77ce9a3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18618, 488, 280, 7, 30, "Input",ExpressionUUID->"df4a2eb8-1c46-4bdf-b15e-b99e536da80e"],
Cell[18901, 497, 433, 12, 34, "Output",ExpressionUUID->"d3b2b5a0-7621-468a-862a-c5ce8499532e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19371, 514, 152, 3, 30, "Input",ExpressionUUID->"119099d7-c5de-4065-bbb1-91a9adc811f1"],
Cell[19526, 519, 854, 27, 80, "Output",ExpressionUUID->"caa78133-1dab-4ed1-930a-5fb1f3d98cab"]
}, Open  ]],
Cell[20395, 549, 154, 3, 35, "Text",ExpressionUUID->"dcb22dba-dd90-4952-b388-c01022b7e1ae"],
Cell[CellGroupData[{
Cell[20574, 556, 1111, 31, 94, "Input",ExpressionUUID->"59bc581f-fbff-4bed-9ea4-ee55e013ce3e"],
Cell[21688, 589, 518, 12, 34, "Output",ExpressionUUID->"0e250b2d-1c84-4ee6-85c0-80d467a2442a"],
Cell[22209, 603, 448, 9, 34, "Output",ExpressionUUID->"018115c7-c09e-4645-8df9-3e4603892971"],
Cell[22660, 614, 331, 5, 34, "Output",ExpressionUUID->"0cdb88df-d17a-40a9-b876-434edc7b2be5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23028, 624, 1229, 36, 115, "Input",ExpressionUUID->"55d74664-14d9-4f28-a946-0ef0a7a9f67c"],
Cell[24260, 662, 544, 16, 52, "Output",ExpressionUUID->"1ea9830b-3fed-4e84-ab59-fcbf8c362afb"],
Cell[24807, 680, 550, 17, 52, "Output",ExpressionUUID->"c66bffdf-1d5d-4051-8b93-1eeb38ba89d6"],
Cell[25360, 699, 275, 5, 34, "Output",ExpressionUUID->"6ee27204-70f5-4cd8-bf96-6aac01b20edd"]
}, Open  ]],
Cell[25650, 707, 152, 3, 35, "Text",ExpressionUUID->"c6056940-178a-4871-86d6-98adee7c574a"],
Cell[CellGroupData[{
Cell[25827, 714, 1632, 51, 136, "Input",ExpressionUUID->"ebc29825-763a-4fee-8b06-61788e19e981"],
Cell[27462, 767, 521, 12, 37, "Output",ExpressionUUID->"7d09e271-30a7-4370-82de-18a43b5089ad"],
Cell[27986, 781, 541, 13, 37, "Output",ExpressionUUID->"85393e7b-4ca7-4b42-8866-2383eb295565"],
Cell[28530, 796, 454, 10, 38, "Output",ExpressionUUID->"103b77a6-d4be-465f-a6a8-f4157332600b"],
Cell[28987, 808, 322, 5, 34, "Output",ExpressionUUID->"91a871b3-1325-4e77-afd8-d25783474524"],
Cell[29312, 815, 454, 10, 38, "Output",ExpressionUUID->"16e94c8a-47f0-410c-80e4-3122bfb79da1"]
}, Open  ]]
}
]
*)

