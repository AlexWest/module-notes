% ⬇ adds subdirectory with provided m files to the path 
addpath([pwd,'/two_ODE_codes_student-1']) 
% It can be deleted if there in the current working directory.
student_name='Alex West'; student_number='1912648';

% Parameters
a = 0.5;
Ymin = -0.9;
Ymax = 2;

% ODE
f = @(y) (y.^2-a)./(y+1);

% Finds the equilbiria
syms x; Ystar = double(solve(f(x)==0))';


title_params={'a'};
set_title_details;
single_ODE_phase_plot_fn(f, Ymin, Ymax, Ystar, title_details)