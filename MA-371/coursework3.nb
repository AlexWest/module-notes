(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21189,        550]
NotebookOptionsPosition[     19784,        520]
NotebookOutlinePosition[     20180,        536]
CellTagsIndexPosition[     20137,        533]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"R", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "4"}], ",", 
       RowBox[{"-", "6.5552"}], ",", "7.70179", ",", "203.56"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "3", ",", 
       RowBox[{"-", "8.02133"}], ",", 
       RowBox[{"-", "557.674"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", 
       RowBox[{"-", "2"}], ",", 
       RowBox[{"-", "135.475"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "1"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Q", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"\[Minus]", "0.14472"}], ",", " ", "0.772176", ",", 
       RowBox[{"\[Minus]", "0.567116"}], ",", 
       RowBox[{"\[Minus]", "0.247346"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"\[Minus]", "0.648163"}], ",", 
       RowBox[{"\[Minus]", "0.261519"}], ",", 
       RowBox[{"\[Minus]", "0.437451"}], ",", "0.5658"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"\[Minus]", "0.623897"}], ",", 
       RowBox[{"\[Minus]", "0.251758"}], " ", ",", "0.133782", ",", 
       RowBox[{"\[Minus]", "0.727648"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"\[Minus]", "0.411939"}], ",", " ", "0.521507", ",", " ", 
       "0.684923", " ", ",", "0.298693"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"c", "[", 
   RowBox[{"k_", ",", "j_"}], "]"}], ":=", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{"k", "==", "j"}], ",", "1", ",", 
    RowBox[{
     RowBox[{"1", "/", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"R", "[", 
         RowBox[{"[", 
          RowBox[{"k", ",", "k"}], "]"}], "]"}], "-", 
        RowBox[{"R", "[", 
         RowBox[{"[", 
          RowBox[{"j", ",", "j"}], "]"}], "]"}]}], ")"}]}], "*", 
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"R", "[", 
         RowBox[{"[", 
          RowBox[{"j", ",", "i"}], "]"}], "]"}], 
        RowBox[{"c", "[", 
         RowBox[{"k", ",", "i"}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", 
         RowBox[{"j", "+", "1"}], ",", "k"}], "}"}]}], "]"}]}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"cVals", "[", "k_", "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"c", "[", 
     RowBox[{"k", ",", "1"}], "]"}], ",", 
    RowBox[{"c", "[", 
     RowBox[{"k", ",", "2"}], "]"}], ",", 
    RowBox[{"c", "[", 
     RowBox[{"k", ",", "3"}], "]"}], ",", 
    RowBox[{"c", "[", 
     RowBox[{"k", ",", "4"}], "]"}]}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"eigenVec", "[", "k_", "]"}], ":=", 
  RowBox[{"Q", ".", 
   RowBox[{"cVals", "[", "k", "]"}]}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"eigenVec", "[", "1", "]"}], "/", 
  RowBox[{"Norm", "[", 
   RowBox[{
    RowBox[{"eigenVec", "[", "1", "]"}], ",", "2"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"eigenVec", "[", "2", "]"}], "/", 
  RowBox[{"Norm", "[", 
   RowBox[{
    RowBox[{"eigenVec", "[", "2", "]"}], ",", "2"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"eigenVec", "[", "3", "]"}], "/", 
  RowBox[{"Norm", "[", 
   RowBox[{
    RowBox[{"eigenVec", "[", "3", "]"}], ",", "2"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"eigenVec", "[", "4", "]"}], "/", 
  RowBox[{"Norm", "[", 
   RowBox[{
    RowBox[{"eigenVec", "[", "4", "]"}], ",", "2"}], 
   "]"}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->CompressedData["
1:eJxTTMoPSmViYGCQAmIQvWd1x447e984LnnrfhhEf1p0+TiI1rO6B6Z3Xt7w
GEQv8LvxFkTL8AQK3AXSPoxWYiB66ff/M0H0hCbj2SB6XY3vD9Z9bxxjFA//
AdEVAUxbOYB03KJPO0F03oY1+0G0yIprR0F0V6PIORAtayP7DERrFX3h4ATS
JQHxvCC668YUYRC9S9xZBET3bf4nD6KLDkgogOhUqSX+INpBVSwERDe/eRcO
ouO5dsaC6LYMAUEBIK0nqCUEoh9yWoSB6F1N3WkgetlSlXwQ/aEntQBEs3PE
lIDoPOFvZSD69ar4PhBdxZ0Kppe9/cEiCKR9Ff+C6c57PnIguleSURFEtzQa
qILoPNYXGiCa6Ym1Hoh+F50Mps/N87cB0T3Xr9uBaLXOa+5gdcGLgkD0/RkJ
00D0rMia2SAaAOom0LA=
  "],
 CellLabel->
  "In[300]:=",ExpressionUUID->"a0a5a59d-9fb9-4e93-8d1d-d3d0ed10be75"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "0.14471997400113135`"}], ",", 
   RowBox[{"-", "0.6481628835578725`"}], ",", 
   RowBox[{"-", "0.6238968879172461`"}], ",", 
   RowBox[{"-", "0.41193892599538456`"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.8577462293586407`*^9, 3.8577462491447067`*^9}, 
   3.8577462915542727`*^9, {3.857746413026185*^9, 3.857746423475168*^9}, {
   3.8577465362981167`*^9, 3.85774658180842*^9}, 3.8577471644666224`*^9},
 CellLabel->
  "Out[305]=",ExpressionUUID->"5503abd1-1a4e-4412-898d-52585fde3d9d"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.6625455146999869`", ",", "0.25215549565887635`", ",", 
   "0.2426935341007661`", ",", "0.6622317535920759`"}], "}"}]], "Output",
 CellChangeTimes->{{3.8577462293586407`*^9, 3.8577462491447067`*^9}, 
   3.8577462915542727`*^9, {3.857746413026185*^9, 3.857746423475168*^9}, {
   3.8577465362981167`*^9, 3.85774658180842*^9}, 3.857747164469962*^9},
 CellLabel->
  "Out[306]=",ExpressionUUID->"485aab9f-0791-428d-96a6-2fbaf3e5f034"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.37141680174595376`", ",", "0.023392305052998406`", ",", 
   "0.25793420535197326`", ",", "0.8916121943729575`"}], "}"}]], "Output",
 CellChangeTimes->{{3.8577462293586407`*^9, 3.8577462491447067`*^9}, 
   3.8577462915542727`*^9, {3.857746413026185*^9, 3.857746423475168*^9}, {
   3.8577465362981167`*^9, 3.85774658180842*^9}, 3.857747164472381*^9},
 CellLabel->
  "Out[307]=",ExpressionUUID->"0247f6cd-eeee-450d-8384-bf20de334a98"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.64904627444432`", ",", "0.5070142279914764`", ",", 
   "0.34969073320651717`", ",", "0.44653319848992656`"}], "}"}]], "Output",
 CellChangeTimes->{{3.8577462293586407`*^9, 3.8577462491447067`*^9}, 
   3.8577462915542727`*^9, {3.857746413026185*^9, 3.857746423475168*^9}, {
   3.8577465362981167`*^9, 3.85774658180842*^9}, 3.8577471644752693`*^9},
 CellLabel->
  "Out[308]=",ExpressionUUID->"c24e3a63-de1a-44a1-8f68-ddba6e84ca19"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"u", "[", 
   RowBox[{"x_", ",", "t_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"a", "*", 
    RowBox[{"x", "^", "2"}]}], "+", 
   RowBox[{"b", "*", "x"}], "+", 
   RowBox[{"d", "*", "t"}]}]}], "\[IndentingNewLine]", 
 RowBox[{"a", "=", 
  RowBox[{
   RowBox[{"-", "1"}], "/", "\[Pi]"}]}], "\[IndentingNewLine]", 
 RowBox[{"Reduce", "[", 
  RowBox[{
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "t"}], "]"}], ",", "t"}], "]"}], "==", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x", ",", "t"}], "]"}], ",", "x", ",", "x"}], "]"}]}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.857750052433132*^9, 3.857750100860989*^9}, {
  3.8577507273037357`*^9, 3.857750731303145*^9}, {3.857751140804289*^9, 
  3.857751147028923*^9}, {3.857755354291658*^9, 3.857755356904462*^9}, {
  3.857760065044322*^9, 3.857760081280569*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"fdcc776b-8b0e-44d8-b9b4-0db13945208c"],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox["1", "\[Pi]"]}]], "Output",
 CellChangeTimes->{3.8577507320715647`*^9, 3.857751147911744*^9, 
  3.857755358928391*^9, 3.85776008215873*^9},
 CellLabel->"Out[2]=",ExpressionUUID->"35117f39-a721-4051-bf70-4f615639d2c5"],

Cell[BoxData[
 RowBox[{"d", "\[Equal]", 
  RowBox[{"-", 
   FractionBox["2", "\[Pi]"]}]}]], "Output",
 CellChangeTimes->{3.8577507320715647`*^9, 3.857751147911744*^9, 
  3.857755358928391*^9, 3.85776008217698*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"91463c48-9e3f-434b-a720-8e97ef04a085"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"e", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"b", "*", "x"}], "+", 
         RowBox[{"a", "*", 
          SuperscriptBox["x", "2"]}]}], ")"}]}], "\[Equal]", 
      RowBox[{"2", "*", "a", " ", 
       RowBox[{"(", 
        RowBox[{"e", "*", "t"}], ")"}]}]}], ",", 
     RowBox[{"a", "!=", "0"}], ",", 
     RowBox[{"e", "!=", "0"}], ",", 
     RowBox[{"t", ">", "0"}], ",", 
     RowBox[{"x", "\[NotEqual]", "0"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"a", ",", "b", ",", "e"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.857750110915943*^9, 3.8577503201217422`*^9}, {
  3.857750764884097*^9, 3.85775086315313*^9}},
 CellLabel->
  "In[361]:=",ExpressionUUID->"9175d885-800d-4992-83d6-c68c42a00a9d"],

Cell[BoxData[
 TemplateBox[{
  "Solve", "svars", 
   "\"Equations may not give solutions for all \\\"solve\\\" variables.\"", 2,
    361, 26, 26272544145028570944, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{{3.857750821087626*^9, 3.857750863754258*^9}},
 CellLabel->
  "During evaluation of \
In[361]:=",ExpressionUUID->"8a76130f-230c-4444-a946-f5be7f722fe3"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"a", "\[Rule]", 
    TemplateBox[{
      RowBox[{"-", 
        FractionBox[
         RowBox[{"b", " ", "e", " ", "x"}], 
         RowBox[{
           RowBox[{
             RowBox[{"-", "2"}], " ", "e", " ", "t"}], "+", 
           RowBox[{"e", " ", 
             SuperscriptBox["x", "2"]}]}]]}], 
      InterpretationBox[
       DynamicModuleBox[{Typeset`open = False}, 
        TemplateBox[{"Expression", 
          StyleBox[
           TagBox[
            TooltipBox["\"condition\"", 
             TagBox[
              RowBox[{
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], ">", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Im", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], ">", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}], "||", 
                RowBox[{"(", 
                  RowBox[{
                    RowBox[{
                    RowBox[{"Re", "[", "b", "]"}], "<", "0"}], "&&", 
                    RowBox[{
                    RowBox[{"Re", "[", "e", "]"}], "<", "0"}], "&&", 
                    RowBox[{"t", ">", "0"}]}], ")"}]}], Short[#, 7]& ]], 
            Annotation[#, 
             Short[
              Or[
               And[
               Im[$CellContext`b] > 0, Im[$CellContext`e] > 0, $CellContext`t > 
                0], 
               And[
               Im[$CellContext`b] > 0, Im[$CellContext`e] < 0, $CellContext`t > 
                0], 
               And[
               Im[$CellContext`b] < 0, Im[$CellContext`e] > 0, $CellContext`t > 
                0], 
               And[
               Im[$CellContext`b] < 0, Im[$CellContext`e] < 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`e] > 0, Im[$CellContext`b] > 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`e] > 0, Im[$CellContext`b] < 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`e] < 0, Im[$CellContext`b] > 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`e] < 0, Im[$CellContext`b] < 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] > 0, Im[$CellContext`e] > 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] > 0, Im[$CellContext`e] < 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] > 0, Re[$CellContext`e] > 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] > 0, Re[$CellContext`e] < 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] < 0, Im[$CellContext`e] > 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] < 0, Im[$CellContext`e] < 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] < 0, Re[$CellContext`e] > 0, $CellContext`t > 
                0], 
               And[
               Re[$CellContext`b] < 0, Re[$CellContext`e] < 0, $CellContext`t > 
                0]], 7], "Tooltip"]& ], "IconizedCustomName", StripOnInput -> 
           False], 
          GridBox[{{
             RowBox[{
               TagBox["\"Head: \"", "IconizedLabel"], "\[InvisibleSpace]", 
               TagBox["Or", "IconizedItem"]}]}, {
             RowBox[{
               TagBox["\"Byte count: \"", "IconizedLabel"], 
               "\[InvisibleSpace]", 
               TagBox["6184", "IconizedItem"]}]}}, 
           GridBoxAlignment -> {"Columns" -> {{Left}}}, DefaultBaseStyle -> 
           "Column", 
           GridBoxItemSize -> {
            "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}], 
          Dynamic[Typeset`open]}, "IconizedObject"]], 
       Or[
        And[
        Im[$CellContext`b] > 0, Im[$CellContext`e] > 0, $CellContext`t > 0], 
        And[
        Im[$CellContext`b] > 0, Im[$CellContext`e] < 0, $CellContext`t > 0], 
        And[
        Im[$CellContext`b] < 0, Im[$CellContext`e] > 0, $CellContext`t > 0], 
        And[
        Im[$CellContext`b] < 0, Im[$CellContext`e] < 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`e] > 0, Im[$CellContext`b] > 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`e] > 0, Im[$CellContext`b] < 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`e] < 0, Im[$CellContext`b] > 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`e] < 0, Im[$CellContext`b] < 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] > 0, Im[$CellContext`e] > 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] > 0, Im[$CellContext`e] < 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] > 0, Re[$CellContext`e] > 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] > 0, Re[$CellContext`e] < 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] < 0, Im[$CellContext`e] > 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] < 0, Im[$CellContext`e] < 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] < 0, Re[$CellContext`e] > 0, $CellContext`t > 0], 
        And[
        Re[$CellContext`b] < 0, Re[$CellContext`e] < 0, $CellContext`t > 0]], 
       SelectWithContents -> True, Selectable -> False]},
     "ConditionalExpression"]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.857750110964085*^9, 3.85775016491717*^9}, {
   3.8577502241028357`*^9, 3.8577503214312468`*^9}, 3.8577507661260843`*^9, {
   3.857750821092928*^9, 3.857750863760125*^9}},
 CellLabel->
  "Out[361]=",ExpressionUUID->"53e63a49-9642-4e41-baf6-e39e9329840f"]
}, Open  ]]
},
WindowSize->{808, 747},
WindowMargins->{{Automatic, 152}, {8, Automatic}},
FrontEndVersion->"13.0 for Mac OS X ARM (64-bit) (December 2, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"afda91ff-c0ba-4560-bb28-d1d50ffaf4e4"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 4151, 123, 304, "Input",ExpressionUUID->"a0a5a59d-9fb9-4e93-8d1d-d3d0ed10be75"],
Cell[4734, 147, 545, 11, 34, "Output",ExpressionUUID->"5503abd1-1a4e-4412-898d-52585fde3d9d"],
Cell[5282, 160, 473, 9, 34, "Output",ExpressionUUID->"485aab9f-0791-428d-96a6-2fbaf3e5f034"],
Cell[5758, 171, 476, 9, 34, "Output",ExpressionUUID->"0247f6cd-eeee-450d-8384-bf20de334a98"],
Cell[6237, 182, 474, 9, 34, "Output",ExpressionUUID->"c24e3a63-de1a-44a1-8f68-ddba6e84ca19"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6748, 196, 992, 27, 73, "Input",ExpressionUUID->"fdcc776b-8b0e-44d8-b9b4-0db13945208c"],
Cell[7743, 225, 260, 5, 50, "Output",ExpressionUUID->"35117f39-a721-4051-bf70-4f615639d2c5"],
Cell[8006, 232, 291, 6, 73, "Output",ExpressionUUID->"91463c48-9e3f-434b-a720-8e97ef04a085"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8334, 243, 837, 24, 33, InheritFromParent,ExpressionUUID->"9175d885-800d-4992-83d6-c68c42a00a9d"],
Cell[9174, 269, 386, 9, 28, "Message",ExpressionUUID->"8a76130f-230c-4444-a946-f5be7f722fe3"],
Cell[9563, 280, 10205, 237, 63, "Output",ExpressionUUID->"53e63a49-9642-4e41-baf6-e39e9329840f"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

