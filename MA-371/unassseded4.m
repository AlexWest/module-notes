% ⬇ adds subdirectory with provided m files to the path 
addpath([pwd,'/two_ODE_codes_student-1']) 
% It can be deleted if there in the current working directory.
student_name='Alex West'; student_number='1912648';

%---------------------------------------------------------------
plot_type='same_axes'; %options are yyplot, subplots or same_axes
%---------------------------------------------------------------

%parameters
%================================
betaX= -0.7;
betaY=1.3;
KX=0.7;
KY=1.3;
deltaXhat=-1.1;
deltaYhat=-1.1;
%critical time at which new effect as added
tc=20;
%time-dependent interaction parameters
deltaX=@(t) 1 + deltaXhat*(t>=tc);
deltaY=@(t) -2 + deltaYhat*(t>=tc);

%================================
%initial condition
U0 = [1;    %U1(0)
      0.5]; %U2(0)
%================================
t_end=40; 
N=1000;
tspan=linspace(0,t_end,N+1)';

%===================================
%system of ODEs
%--------------
U1var='R';
U2var='W';
dUdt=@(t,U) [deltaX(t)*U(1) + betaX*U(1).*U(2);      %dU1/dt
             deltaY(t)*U(2) + betaY*U(1).*U(2)];     %dU2/dt
%===================================

%==========================================
%solve the ODE system and plot time courses
%==========================================
title_params={'betaX','betaY','deltaXhat','deltaYhat','KX','KY','tc'};
set_title_details;
%----------------
[t,U] = two_ODEs_timecourse_fn(dUdt,tspan,U0,U1var,U2var,plot_type,title_details);
%======================================================================




% Question 2
rx =1;
ry =1;
Kx = 1000;
Ky = 600;
deltaX = 2/1000;
deltaY = 1/600;
%================================

%X and Y grids for direction field
%=================================
U1var='X';
U2var='Y';
U1vec=linspace(0,1050,81);
U2vec=linspace(0,650,81);
%=================================

%===================================
%system of ODEs
%--------------
dUdt=@(U) [rx*U(1)*(1-(U(1))/Kx) - deltaX*U(1)*U(2);      %dU1/dt
           ry*U(2)*(1-(U(2)/Ky)) - deltaY*U(1)*U(2)];     %dU2/dt
%===================================


%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_params={'betaR','betaW','aR','aW'};
set_title_details;
%--------------------------------------
clf
two_ODEs_dir_field_fn(dUdt,U1vec,U2vec,U1var,U2var,title_details)
%======================================================================
shg
hold on
plot([0 600],[600 0],'r', 'linewidth' ,2)
plot([0 1000],[500 0],'b', 'linewidth' ,2)
hold off
