%================================================
% discrete_timecourse_script.m
%------------------------------------------------
%display time course(s) for a discrete map
%corresponding to one or more initial conditions
%================================================
clear all;  clc;
student_name='Alex';  student_number='1912648';
%================================================

%-------------------------------------------------
%parameters
%-------------------------------------------------
r=4;
%function
%-------------------------------------------------
f=@(X) r*X.*(1-X);
%-------------------------------------------------
%vector of initial values
Xinit=[0.3];
%number of points
nmax=30;
%-------------------------------------------------

%===================================================
%specify parameters you'd like to appear in figure title
title_params={'r'}; 
set_title_details;
%generate and plot timecourse for each initial condition
[nvec,X]=discrete_timecourse_fn(f,Xinit,nmax,title_details,'x-');
%===================================================
