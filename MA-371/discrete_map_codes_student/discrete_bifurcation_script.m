%================================================
% discrete_bifurcation_script_LJB.m
%------------------------------------------------
%creates a bifurcation diagram for a specified function with 
%respect to a specified parameter
%================================================
clear all;  clc;
student_name='Alex';  student_number='1912648';
%================================================

%-------------------------------------------------
%fixed parameters
%-------------------------------------------------
%
%
%
%-------------------------------------------------
%bifurcation parameter
%-------------------------------------------------
bif_param='r';
bif_param_min=0;
bif_param_max=4;
num_bif_param_vals=8001;
%function
%-------------------------------------------------
f_str='f=@(X) r*X.*(1-X)';
%-------------------------------------------------

%simulation parameters
%================================
%large n to compute up to
nmax=500;
%want to plot the final numtoplot points of the timecourse
numtoplot=50;
%================================

bif_param_range=linspace(bif_param_min,bif_param_max,num_bif_param_vals);
points=zeros(numtoplot,num_bif_param_vals);

for j=1:num_bif_param_vals
    eval([bif_param,'  = bif_param_range(j);']);
    eval([f_str,';']);
    Xinit=rand(1);
    [nvec,X]=discrete_timecourse_only_fn(f,Xinit,nmax);
    points(:,j)=X(nmax-numtoplot+1:nmax);
end;
 
%======================================================================
%plots
%======================================================================
%specify parameters you'd like to appear in figure title
title_params={}; %probably just want any fixed parameters in here
set_title_details;
%--------------------------------------
title_str={['  ',mfilename,'.m   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
        ['bifurcation diagram       ',title_details{2}],...
        [title_details{1},'     f = ',func2str(f),'   bifurcation parameter = ',bif_param]};
%--------------------------------------
%set the linestyle
plot_str='b.';
%--------------------------------------
 
%now the plots
%-------------
plot(bif_param_range,points,plot_str,'linewidth',2,'markersize',2)
set(gca,'fontsize',12)
title(title_str,'interpreter','none','edgecolor','k')
xlabel(bif_param),ylabel('X*')
%--------------------------------------
shg
%======================================================================
