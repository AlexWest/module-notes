%================================================
% discrete_cobweb_script.m
%------------------------------------------------
%for a given discrete map and initial condition, 
%plot a cobweb diagram
%================================================
clear all;  clc;
student_name='Alex';  student_number='1912648';
%================================================
 
%-------------------------------------------------
%parameters
%-------------------------------------------------
R0=2;
a=0.01;
b=4;
%function
%-------------------------------------------------
f=@(X) R0*X./((1+a*X).^b);
%-------------------------------------------------
%initial value
Xinit=1;
%number of points
%-------------------------------------------------
nmax=60;
%-------------------------------------------------

%===================================================
%specify parameters you'd like to appear in figure title
title_params={'R0','a','b'}; 
set_title_details;
%generate and plot cobweb
Xvec=discrete_map_cobweb_fn(f,Xinit,nmax,title_details);
%===================================================
