%================================================
% discrete_timecourse_fn.m
%------------------------------------------------
%inputs: f - function
%        Xinit_vec - vector of initial values
%        nmax - final time
%
%outputs: vector nvec (time) and matrix X (population)
%         
%================================================
function [nvec,XX]=discrete_timecourse_fn(f,Xinit_vec,nmax,title_details,plot_str)

[ST,I] = dbstack;
calling_filename=ST(2).file;

num_ICs=length(Xinit_vec);

%create vector of n values
nvec=(1:nmax)';

for j=1:num_ICs
    X=zeros(nmax,1);
    X(1)=Xinit_vec(j);
    %iteration
    for n=1:nmax-1
        X(n+1)=f(X(n));
    end;
    XX(:,j)=X;
end;
 
%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    ['time course       ',title_details{2}],...
    [title_details{1},'     f = ',func2str(f)]};
%--------------------------------------
%now the plots
%-------------
plot(nvec,XX,plot_str,'linewidth',2)
set(gca,'fontsize',12)
xlabel('n'), ylabel('X_{n}')
title(title_str,'interpreter','none','edgecolor','k')
shg 
%======================================================================

