%================================================
% discrete_timecourse_only_fn.m
%------------------------------------------------
%inputs: f - function
%        Xinit_vec - vector of initial values
%        nmax - final time
%
%outputs: vector nvec (time) and matrix X (population)
%         
%================================================
function [nvec,XX]=discrete_timecourse_only_fn(f,Xinit_vec,nmax)

num_ICs=length(Xinit_vec);

%create vector of n values
nvec=(1:nmax)';

for j=1:num_ICs
    X=zeros(nmax,1);
    X(1)=Xinit_vec(j);
    %iteration
    for n=1:nmax-1
        X(n+1)=f(X(n));
    end;
    XX(:,j)=X;
end;
