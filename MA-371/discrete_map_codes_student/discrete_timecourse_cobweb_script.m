%================================================
% discrete_timecourse_cobweb_script.m
%------------------------------------------------
%for a given discrete map and initial condition, 
%plot a timecourse and corresponding cobweb diagram
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================
 
%-------------------------------------------------
%parameters
%-------------------------------------------------
R0=20;
a=0.01;
b=4;
%function
%-------------------------------------------------
f=@(X) R0*X./((1+a*X).^b);
%-------------------------------------------------
%initial value
Xinit=10;
%number of points
%-------------------------------------------------
nmax=60;
%-------------------------------------------------

%===================================================
%specify parameters you'd like to appear in figure title
title_params={'R0','a','b'}; 
set_title_details;
%generate and plot timecourse
subplot(1,2,1)
[nvec,X]=discrete_timecourse_fn(f,Xinit,nmax,title_details,'x-');
%generate and plot corresponding cobweb
subplot(1,2,2)
Xvec=discrete_map_cobweb_fn(f,Xinit,nmax,title_details);
%===================================================
