function X = discrete_map_cobweb_fn(f,X1,nmax,title_details)
%================================================
% discrete_map_cobweb_fn.m
%------------------------------------------------
% function X = discrete_map_cobweb_fn(f,X1,nmax,title_str_params)
%
%constructs a cobweb diagram for function f for the 1D map X_{n+1}=f(X_{n})
%f = function  
%X1 = initial condition X_{1}
%nmax = number of x-values in output
%
%------------------------------------------------
% 
%Created : 26.07.16  ----  14:29
%Author  : LJB
% 
%Notes   :
% 
%================================================


[ST,I] = dbstack;
calling_filename=ST(2).file;

X=zeros(nmax,1);
% xplot=zeros(nmax+1,1);
% yplot=xplot;
X(1)=X1;
for n=1:nmax-1
    X(n+1)=f(X(n));
end;

%plots
xplot(1)=X1;
yplot(1)=0;
for n=1:nmax-1
    xplot(2*n)=X(n);
    yplot(2*n)=X(n+1);
    xplot(2*n+1)=X(n+1);
    yplot(2*n+1)=X(n+1);
end; 

xcurves=linspace(min(0,min(xplot)),max(xplot)*1.1,5001)';
for j=1:5001
    ycurves(j)=f(xcurves(j));
end;


% disp(' ')
% disp(mfilename)
% disp('------------------------------')
% disp(['f = ',func2str(f)])
% disp(' ')
% disp([[1:nmax]' X])
% disp('------------------------------')
% disp(' ')

%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    ['cobweb       ',title_details{2}],...
    [title_details{1},'     f = ',func2str(f)]};
%--------------------------------------
 
%now the plots
%-------------
plot(xplot,yplot,'b-',xcurves,xcurves,'r--',xcurves,ycurves,'k--','linewidth',2)
hold on
Yarrow(X1,[0 X(2)],0.1,'k-',1,'k')
set(gca,'fontsize',12)
xlabel('X_{n}'), ylabel('X_{n+1}')
title(title_str,'interpreter','none','edgecolor','k')
hold off
%--------------------------------------
shg
%======================================================================


function Yarrow(x,y,alph,plot_str,linewidth,arrow_color)
%================================================
 
y1=y(1);y2=y(2);
L=y2-y1;

ay=y2-alph*L;
axp=x+alph*abs(L)/sqrt(3);
axm=x-alph*abs(L)/sqrt(3);

plot([x x],y,plot_str,'linewidth',linewidth)
hold on
patch([x axp axm],[y2 ay ay],arrow_color)
shg
