% % Problem 1
% r=3.83;
% f=@(X) r*X.*(1-X);
% Xinit=[0.7];
% title_params={'r'}; 
% set_title_details;
% discrete_timecourse_fn(f,Xinit, 100, title_details, 'x-')

% % Problem 2
% R0 = 1.7;
% a = 1.1;
% Xmax = 12;
% X0 = linspace(0.05, 1.2, 10);
% f=@(X) (R0*X)/(1+a*X);
% title_params={'a', 'R0'}; 
% set_title_details;
% discrete_timecourse_fn(f, Xinit, Xmax, title_details, 'x-')

% Question 3
% a = 5;
% b = 4;
% Xinit = 0.1;
% Xmax = 20;
% f = @(X) a*exp(-b*X)*X;
% title_params={'a','b'};
% set_title_details;
% 
% subplot(1,2,1)
% [nvec,X]=discrete_timecourse_fn(f,Xinit,nmax,title_details,'x-');
% %generate and plot corresponding cobweb
% subplot(1,2,2)
% Xvec=discrete_map_cobweb_fn(f,Xinit,nmax,title_details);

% Question 4
R0 = 2.3;
a = 1;
Xinit = 0.1;
Xmax = 10;
f = @(X) (R0*X)/(1+a*X);
title_params = {'R0','a'};
set_title_details;
discrete_timecourse_fn(f, Xinit, Xmax, title_details, 'x-')
hold on
f2 = @(n) ((R0-1).*Xinit)./(a.*Xinit+(R0-1-a.*Xinit)*R0.^(1-n));
plot(f2(1:Xmax))
hold off