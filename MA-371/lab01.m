% Question 1
((8.2^2)-(6^2))/sqrt(2) + 10*3*pi

9*nthroot(4,3)*(0.856/3)
(sin(3.5*pi)/cos(1.8*pi)) + 0.5*cot(2.1*pi)
6*sind(75)+0.25*cotd(63)
log10(4987) + log(6.5) - tan(4.1*pi)

% Question 2
a=3;b=5;c=-6;

root1 = (-b+sqrt(b^2-4*a*c))/2*a
root2 = (-b-sqrt(b^2-4*a*c))/2*a

% Question 3
v1 = linspace(67,524,10)'
v2  = v1'

% Question 4
a = 0.432; b=1.654;
[a, a^3, 1/b, a*b, nthroot(a,b)]'

% Question 5
x = linspace(-3, 3, 30);

y = -(x.^3) + (2.*x) - 8;
y = cos(3.*x) - 4.*(x.^2);

% Question 6
a = [9, 5, -1, 3]';b=[4,-6.5,8,7]';
a.^b
a./b
a.*b

% Question 7
M = [4, 7, 9, 0; 8, -42, 5, -pi; 4, 2, 0, 1; -6, -5, -4, -3];
M(:,2:4)
M(2:3,2:3)
M(2:4, 1:2)

% Question 8
M1 = zeros(2,4)
M2  = [zeros(3,2) eye(3); zeros(2, 5)]
M3 = repmat(1:5, 5, 1)

% Question 9
x = linspace(-10,10,41)';

[x (log(x.^2)+2) (1./((x+7).^2)) (3.*exp(-x.^2))]
