r = 3.2;
X = zeros(30,1);
X(1) = 0.3;
N = 30;
for n = 2:N
    X(n) = r*(1-X(n-1))*X(n-1);
end

plot(1:N, X)