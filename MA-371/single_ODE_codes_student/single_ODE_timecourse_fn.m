%================================================
% single_ODE_timecourse_fn.m
%------------------------------------------------
%solves the IVP 
%dP/dt=f(t,P),   P(0)=P0,
%and plots the solution for a range of initial conditions
%         
%================================================
function [t,PP]=single_ODE_timecourse_fn(f,Pinit_vec,t_end,line_style,title_details)

[ST,I] = dbstack;
calling_filename=ST(2).file;

N=1000;
tspan=linspace(0,t_end,N+1)';

num_ICs=length(Pinit_vec);

%solver options
options=odeset('AbsTol',1e-20,'RelTol',1e-6);

PP=zeros(N+1,num_ICs);
%loop over initial conditions
for j=1:num_ICs
    Pinit=Pinit_vec(j);
    %Solve using ode solver
    [t, P]  = ode45(f,tspan,Pinit,options);
    PP(1:length(P),j)=P;
end;
 
%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
           ['dP/dt = f(t,P)     with       f = ',func2str(f),'     ',title_details{2}],...
            title_details{1}};
%--------------------------------------
%now the plots
%-------------
plot(t,PP,line_style,'linewidth',2)
set(gca,'fontsize',12)
xlabel('t'), ylabel('P(t)')
title(title_str,'interpreter','none','edgecolor','k')
shg 
%======================================================================

