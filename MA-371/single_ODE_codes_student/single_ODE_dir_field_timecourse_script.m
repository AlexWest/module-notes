%================================================
% single_ODE_dir_field_timecourse_script.m
%------------------------------------------------
%for a given ODE     dX/dt = f(t,X)
%plot a direction field for a grid of 
%t and X0 values, together with time courses for a 
%vector of initial conditions
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================
 
%parameters
%================================
r=1;
K=40;
%----------------------------
Xinit=0.2;   %vector of initial conditions
%================================

%================================
%RHS of the ODE
f=@(t,X) r*(1-X/K).*X;
%================================

%ranges for t and X0
%------------------
tspan=linspace(0,10,31)';
X0span=linspace(0,60,31)';
%------------------

%==================================
%construct and plot direction field
%==================================
title_params={'r','K'};
set_title_details;
%----------------
single_ODE_dir_field_fn(f,tspan,X0span,title_details)
hold on
[t,PP]=single_ODE_timecourse_fn(f,Xinit,tspan(end),'b-',title_details);
hold off
%=======================================================================
