function single_ODE_phase_plot_fn(f,Pmin,Pmax,equivec,title_details)
%======================================================================
%construct a phase plot with arrows on the phase line
%for the autonomous ODE dP/dt=f(P)
%======================================================================

[ST,I] = dbstack;
calling_filename=ST(2).file;

P=linspace(Pmin,Pmax,101)';
xvec=[Pmin equivec Pmax];

%======================================================================
%plots
%======================================================================
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    ['phase plot      ',title_details{2}],...
    [title_details{1},'     f = ',func2str(f)]};
%--------------------------------------
%set the linestyle
plot_str='b-';
%--------------------------------------

%now the plots
%-------------
plot(P,f(P),plot_str,'linewidth',2)
%plot x,y axes
hold on
plot(xlim(gca),[0 0],'k-')
plot([0 0],ylim(gca),'k-')
set(gca,'fontsize',12)
xlabel('P'), ylabel('dP/dt = f(P)')
title(title_str,'interpreter','none','edgecolor','k')
hold on

numintervals=length(xvec)-1;
xscale=0.05;
yscale=0.05;
xrange=Pmax-Pmin;

for j=1:numintervals
    x1=xvec(j);
    x2=xvec(j+1);
    L=x2-x1;
    Lscale=xscale*xrange/L;
    xm=(x1+x2)/2;
    arrowdir(j)=sign(f(xm));
    if arrowdir(j)>0
        arrowX(xm-L/4,xm+L/4,0,Lscale,yscale,'k-',3,'k')
        hold on
    else
        arrowX(xm+L/4,xm-L/4,0,Lscale,yscale,'k-',3,'k')
        hold on
    end;
end;

%check stability
for j=1:numintervals-1
    if arrowdir(j)>0
        if arrowdir(j+1)<0
            S(j)=1; %stable
            plot(equivec(j),0,'o','markersize',14,'markerfacecolor','b')
        else
            S(j)=0.5; %"semi-stable"
            plot(equivec(j),0,'s','markersize',14)
        end;
    else
        if arrowdir(j+1)<0
            S(j)=0.5; %"semi-stable"
            plot(equivec(j),0,'s','markersize',14)
       else
            S(j)=0; %unstable
            plot(equivec(j),0,'o','markersize',14,'linewidth',2)
        end;
    end;
end;
hold off
%--------------------------------------
shg
%======================================================================





%*******************************************************
%subfunction for arrows
%*******************************************************
function arrowX(xL,xR,y,xscale,yscale,plot_str,linewidth,arrow_color)
%*******************************************************

L=xR-xL;

ax=xR-xscale*L;
ymax=ylim(gca);
yrange=ymax(2)-ymax(1);
ayp=y+yscale*yrange;
aym=y-yscale*yrange;

plot([xL xR],[y y],plot_str,'linewidth',linewidth)
hold on
patch([xR ax ax],[y ayp aym],arrow_color)
shg
hold off



