%================================================
% single_ODE_dir_field_script.m
%------------------------------------------------
%for a given ODE, plot a direction field for a grid of 
%t and X0 values
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================
 
%parameters
%================================
r=1;
K=40;
%================================

%================================
%RHS of the ODE
f=@(t,X) r*(1-X/K).*X;
%================================

%ranges for t and X0
%------------------
tspan=linspace(0,5,31)';
X0span=linspace(0,20,31)';
%------------------

%==================================
%construct and plot direction field
%==================================
title_params={'r','K'};
set_title_details;
%----------------
single_ODE_dir_field_fn(f,tspan,X0span,title_details)
%=======================================================================
