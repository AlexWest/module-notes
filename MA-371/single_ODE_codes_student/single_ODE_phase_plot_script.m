%================================================
% single_ODE_phase_plot_script.m
%------------------------------------------------
%constructs a phase plot for a given autonomous ODE
%dP/dt=f(P)
%and a given vector of equilibrium values P*
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================

%parameters
%================================
r=1;
K=10;
%================================

%================================
%RHS of the autonomous ODE
f=@(P) r*(1-P/K).*P - 16/10;
%================================

%======================
%range of P to consider
%======================
Pmin=-5;
Pmax=25;
%============================
%vector of equilibrium points
%============================
Pstar=[2 8];
%============================


%======================================================================
%generate phase plot
%======================================================================
title_params={'r','K'};
set_title_details;
single_ODE_phase_plot_fn(f,Pmin,Pmax,Pstar,title_details)
%======================================================================
