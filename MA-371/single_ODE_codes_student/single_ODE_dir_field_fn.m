function single_ODE_dir_field_fn(f,tspan,X0span,title_details)

[ST,I] = dbstack;
calling_filename=ST(2).file;

num_t=length(tspan); num_X0=length(X0span);
dt=(max(tspan)-min(tspan))/num_t;
dX0=(max(X0span)-min(X0span))/num_X0;
t=tspan; y=X0span;
dy=dX0;

[tgrid,X0grid]=meshgrid(t',y');

for i=1:num_X0
    for j=1:num_t
        if abs(f(tspan(j),y(i)))<dy/dt
            u(i,j)=dt;
            v(i,j)=f(t(j),y(i))*dt;
        else
            v(i,j)=sign(f(tspan(j),y(i)))*dy;
            u(i,j)=dy/abs(f(tspan(j),y(i)));
        end;
    end;
end;

L=min(dt,dy);


%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    ['direction field      ',title_details{2}],...
    [title_details{1},'     f = ',func2str(f)]};
%--------------------------------------
%set the linestyle
plot_str='-';
grey=[0.4 0.4 0.4];
%--------------------------------------

%now the plots
%-------------
plot(tgrid,X0grid,'.','color',grey),hold on
quiver(tgrid,X0grid,u,v,0,'color',grey,'linewidth',1,'MaxHeadSize',0.05*L)
set(gca,'fontsize',12)
xlabel('t'), ylabel('X')
title(title_str,'interpreter','none','edgecolor','k')
axis([tspan(1) tspan(end) X0span(1) X0span(end)])
%--------------------------------------
hold off
shg
%======================================================================
