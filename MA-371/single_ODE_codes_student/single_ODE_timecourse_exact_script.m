%================================================
% single_ODE_timecourse_exact_script_LJB.m
%------------------------------------------------
%solves the IVP 
%dP/dt=f(t,P),   P(0)=P0,
%and plots both numerical and analytical solutions
%for one initial condition
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================
 
%fixed parameters
%================================
r=0.2;
K=40;
%----------------------------
P0=5;   %single initial condition
%================================
t_end=30; 
%================================

%================================
%RHS of the ODE
f=@(t,P) r*(1-P/K).*P;
%================================

%exact solution
Pexact=@(t) K./(1+(K/P0-1)*exp(-r*t));

%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_params={'r','K'};
set_title_details;
[t,PP]=single_ODE_timecourse_fn(f,P0,t_end,'-',title_details);
%======================================================================
hold on
texact=linspace(0,t_end,51)';
plot(texact,Pexact(texact),'ro');
hold off
legend('numerical','analytical','location','best')
%======================================================================