function linear_2x2_dir_field_fn(A,XX,YY,U1var,U2var,title_details)

[ST,I] = dbstack;
calling_filename=ST(2).file;


%calculate eigenvalues and eigenvectors
[V,D]=eig(A);
lambda1=D(1,1); lambda2=D(2,2);
v1=V(:,1); v2=V(:,2);


%===================================
%linear system of ODEs
%---------------------
dUdt=@(X) A*X;
%===================================

[XXX,YYY]=meshgrid(XX,YY);
for i=1:length(XX)
    Xp=XX(i);
    for j=1:length(YY)
        Yp=YY(j);
        dy_and_dx=dUdt([Xp;Yp]);
        dxdt(i,j)=dy_and_dx(1)/norm(dy_and_dx);
        dydt(i,j)=dy_and_dx(2)/norm(dy_and_dx);
    end;
end;

if isreal(lambda1)
    str1format='%6s %+.2f  %+.2f %15s %+.2f %10s %+.2f %15s %+.2f %10s %+.2f';
    t_str_1=sprintf(str1format,'A = ',A(1,:),'    lambda1 = ',lambda1,'   v1 = ',v1(1),'    lambda2 = ',lambda2,'   v2 = ',v2(1));
    t_str_2=sprintf(str1format,'A = ',A(2,:),'    lambda1 = ',lambda1,'   v1 = ',v1(2),'    lambda2 = ',lambda2,'   v2 = ',v2(2));
else
    str1format='%6s %+.2f  %+.2f %15s %10s';
    t_str_1=sprintf(str1format,'A = ',A(1,:),'    lambda1 = ',num2str(lambda1));
    t_str_2=sprintf(str1format,'A = ',A(2,:),'    lambda1 = ',num2str(lambda1));
end;

%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    [title_details{2},'   ',title_details{1}],...
    t_str_1,...
    t_str_2};
%--------------------------------------
grey=[0.4 0.4 0.4];

%now the plots
%-------------
quiver(XXX',YYY',dxdt,dydt,0.7,'-','color','r','linewidth',1)
set(gca,'fontsize',12)
axis([min(XX) max(XX) min(YY) max(YY)])
xlabel(U1var), ylabel(U2var)
title(title_str,'interpreter','none','edgecolor','k')
%--------------------------------------
if isreal(lambda1)
    hold on
    if v1(1)~=0
        h1=plot(XX,v1(2)*XX/v1(1),'b-','linewidth',3);
    else
        h1=plot([0 0],[XX(1) XX(end)],'b-','linewidth',3);
    end;
    if v2(1)~=0
        h2=plot(XX,v2(2)*XX/v2(1),'k--','linewidth',3);
    else
        h2=plot([0 0],[XX(1) XX(end)],'k--','linewidth',3);
    end;
    hold off
    legend([h1,h2],'v1','v2','location','best')
end;
shg
%======================================================================
