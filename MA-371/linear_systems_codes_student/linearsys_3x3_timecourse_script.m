%================================================
% linearsys_3x3_timecourse_script.m
%------------------------------------------------
%solves a 3x3 coupled ODE system
%dX/dt=A*X, X(0)=X0
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================

%matrix
%================================
A=[-2 5 1;
   -4 -3 0;
   1 2 -1];
%================================
%initial condition
X0 = [1;    %x(0)
      0;    %y(0)
      0];   %z(0)
%================================

%time span
%----------------------------
t_end=6; 
N=1000;
tspan=linspace(0,t_end,N+1)';
%----------------------------


%solve numerically
%==========================================================
%solver options
options=odeset('AbsTol',1e-20,'RelTol',1e-6);
%Solve system of ODEs using ode solver
[t, X]  = ode45(@(t,X) A*X,tspan, X0, options);
%==========================================================
%retrieve individual variables
x=X(:,1);
y=X(:,2);
z=X(:,3);
%=============================

%==========================================
%plots
%==========================================
a11=A(1,1); a12=A(1,2); a13=A(1,3);  
a21=A(2,1); a22=A(2,2); a23=A(2,3);  
a31=A(3,1); a32=A(3,2); a33=A(3,3);  
x0=X0(1); y0=X0(2); 
title_params={'a11','a12','a13','a21','a22','a23','a31','a32','a33','x0','y0'};
set_title_details;
title_str={[mfilename,'.m   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    [title_details{2},'   ',title_details{1}]};
%----------------
plot(t,x,'k-',t,y,'b--',t,z,'r-.','linewidth',2)
set(gca,'fontsize',12)
xlabel('t'), ylabel('solutions')
title(title_str,'interpreter','none','edgecolor','k')
legend('x(t)','y(t)','z(t)','location','best')
hold off
%======================================================================
shg