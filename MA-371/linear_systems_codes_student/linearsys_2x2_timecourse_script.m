%================================================
% linearsys_2x2_timecourse_script.m
%------------------------------------------------
%solves a 2x2 coupled ODE system
%dX/dt=A*X, X(0)=X0
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================

%matrix
%================================
A=[3 -2;
   2 -2];
%================================
%initial condition
X0 = [1;    %x(0)
      0];   %y(0)
%================================

%time span
%----------------------------
t_end=1; 
N=1000;
tspan=linspace(0,t_end,N+1)';
%----------------------------


%solve numerically
%==========================================================
%solver options
options=odeset('AbsTol',1e-20,'RelTol',1e-6);
%Solve system of ODEs using ode solver
[t, X]  = ode45(@(t,X) A*X,tspan, X0, options);
%==========================================================
%retrieve individual variables
x=X(:,1);
y=X(:,2);
%=============================

%==========================================
%plots
%==========================================
a11=A(1,1); a12=A(1,2); a21=A(2,1); a22=A(2,2); x0=X0(1); y0=X0(2); 
title_params={'a11','a12','a21','a22','x0','y0'};
set_title_details;
title_str={[mfilename,'.m   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    [title_details{2},'   ',title_details{1}]};
%----------------
plot(t,x,'b--',t,y,'r-','linewidth',2)
set(gca,'fontsize',12)
xlabel('t'), ylabel('solutions')
title(title_str,'interpreter','none','edgecolor','k')
legend('x(t)','y(t)','location','best')
hold off
%======================================================================
shg