function [t,U1,U2] = two_ODEs_phaseplane_fn(dUdt,tspan,U0_matrix,U1var,U2var,title_details)

[ST,I] = dbstack;
calling_filename=ST(2).file;

%solver options
options=odeset('AbsTol',1e-20,'RelTol',1e-6);

num_ICs=size(U0_matrix,2);

%loop over initial conditions
for j=1:num_ICs
    U0=U0_matrix(:,j);
    %==========================================================
    %Solve using ode solver
    [t, U]  = ode45(dUdt,tspan, U0, options);
    %==========================================================
    %retrieve individual variables
    U1(:,j)=U(:,1);
    U2(:,j)=U(:,2);
    %=============================
end;

%======================================================================
%plots
%======================================================================
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    [title_details{2},'   ',title_details{1}],...
    ['  f = ',func2str(dUdt)]};
%--------------------------------------
%set the linestyle
plot_str='b-';
%--------------------------------------

%now the plots
%-------------
plot(U1(1,:),U2(1,:),'ko')
hold on
plot(U1(end,:),U2(end,:),'kx')
plot(U1,U2,'linewidth',2)
set(gca,'fontsize',12)
xlabel(U1var), ylabel(U2var)
title(title_str,'interpreter','none','edgecolor','k')
shg 
hold off
%======================================================================
