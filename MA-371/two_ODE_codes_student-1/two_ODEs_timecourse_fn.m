function [t,U] = two_ODEs_timecourse_fn(dUdt,tspan,U0,U1var,U2var,plot_type,title_details)

[ST,I] = dbstack;
calling_filename=ST(2).file;

%solver options
options=odeset('AbsTol',1e-20,'RelTol',1e-6);

%==========================================================
%Solve using ode solver
[t, U]  = ode45(dUdt,tspan, U0, options);
%==========================================================
%retrieve individual variables
eval([U1var,'=U(:,1);']);
eval([U2var,'=U(:,2);']);
%=============================

%======================================================================
%plots
%======================================================================
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    [title_details{2},'   ',title_details{1}],...
    ['  f = ',func2str(dUdt)]};
%--------------------------------------
%set the linestyle
plot_str='b-';
%--------------------------------------

%now the plots
%-------------
clf
plot_2_curves(t,U(:,1),U(:,2),U1var,U2var,title_str,plot_type)
