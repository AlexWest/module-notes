%================================================
% two_ODEs_dir_field_trajectories_script.m
%------------------------------------------------
%produces a direction field for 2-ODE autonomous system
%dUdt=f(t,U)
%and overlays phase plane trajectories for a number of initial conditions
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================
 
%parameters
%================================
betaR=1;
betaW=1;
aR=1;
aW=1;
%================================
%initial conditions
U1_0 = [1 2];    %U1(0)
U2_0 = [0.5 0.6]; %U2(0)
%================================
t_end=50; 
N=1000;
tspan=linspace(0,t_end,N+1)';

%X and Y grids for direction field
%=================================
U1var='R';
U2var='W';
U1vec=linspace(0,4,40);
U2vec=linspace(0,4,40);
%=================================

%===================================
%system of ODEs
%--------------
dUdt=@(U) [betaR*U(1) - aR*U(1).*U(2);      %dU1/dt
           -betaW*U(2) + aW*U(1).*U(2)];     %dU2/dt
%===================================


%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_params={'betaR','betaW','aR','aW'};
set_title_details;
clf
%--------------------------------------
two_ODEs_dir_field_fn(dUdt,U1vec,U2vec,U1var,U2var,title_details)
%======================================================================
hold on
[t,U] = two_ODEs_phaseplane_fn(@(t,U) dUdt(U),tspan,[U1_0;U2_0],U1var,U2var,title_details);
hold off
