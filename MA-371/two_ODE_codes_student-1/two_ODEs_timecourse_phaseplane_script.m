%================================================
% two_ODEs_timecourse_phaseplane_script.m
%------------------------------------------------
%solves a 2x2 coupled ODE system
%dUdt=f(t,U)
%plots a single timecourse for each variable, and a phase plane trajectory
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================

%---------------------------------------------------------------
plot_type='same_axes'; %options are yyplot or same_axes
%---------------------------------------------------------------

%parameters
%================================
betaR=1;
betaW=0.3;
aR=1;
aW=2;
%================================
%initial condition
U0 = [1;    %U1(0)
      0.5]; %U2(0)
%================================
t_end=50; 
N=1000;
tspan=linspace(0,t_end,N+1)';

%===================================
%system of ODEs
%--------------
U1var='R';
U2var='W';
dUdt=@(t,U) [betaR*U(1) - aR*U(1).*U(2);      %dU1/dt
           -betaW*U(2) + aW*U(1).*U(2)];     %dU2/dt
%===================================

%==========================================
%solve the ODE system and plot time courses
%==========================================
title_params={'betaR','betaW','aR','aW'};
set_title_details;
%----------------
subplot(2,1,1)
[t,U] = two_ODEs_timecourse_phaseplane_fn(dUdt,tspan,U0,U1var,U2var,plot_type,title_details);
%======================================================================
shg