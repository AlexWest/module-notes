function two_ODEs_dir_field_fn(dUdt,XX,YY,U1var,U2var,title_details)

[ST,I] = dbstack;
calling_filename=ST(2).file;

[XXX,YYY]=meshgrid(XX,YY);
for i=1:length(XX)
    Xp=XX(i);
    for j=1:length(YY)
        Yp=YY(j);
        dy_and_dx=dUdt([Xp,Yp]);
        dxdt(i,j)=dy_and_dx(1)/norm(dy_and_dx);
        dydt(i,j)=dy_and_dx(2)/norm(dy_and_dx);
    end;
end;

%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_str={['  ',calling_filename,'   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    [title_details{2},'   ',title_details{1}],...
    ['  dU/dt = ',func2str(dUdt)]};
%--------------------------------------
grey=[0.4 0.4 0.4];

%now the plots
%-------------
quiver(XXX',YYY',dxdt,dydt,0.7,'-','color',grey,'linewidth',1)
set(gca,'fontsize',12)
axis([min(XX) max(XX) min(YY) max(YY)])
xlabel(U1var), ylabel(U2var)
title(title_str,'interpreter','none','edgecolor','k')
%--------------------------------------
shg
%======================================================================
