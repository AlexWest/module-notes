function plot_2_curves(t,X,Y,Xstr,Ystr,title_str,plot_type)

Xoft=[Xstr,'(t)'];
Yoft=[Ystr,'(t)'];


switch plot_type
    case 'same_axes'
        %--------------------------------------
        plot(t,X,'b--',t,Y,'r-','linewidth',2)
        set(gca,'fontsize',12)
        xlabel('t'), ylabel('solutions')
        title(title_str,'interpreter','none','edgecolor','k')
        legend(Xoft,Yoft,'location','best')
        %--------------------------------------
    case 'yyplot'
        %colors and styles
        color1='b'; line_style1='--';
        color2='r'; line_style2='-';
        %now the yyplots
        %-------------
        [hAx,curve1,curve2]=plotyy(t,X,t,Y);
        
        set(hAx(1),'Ycolor',color1,'fontsize',12);
        set(curve1,'color',color1,'linestyle',line_style1,'linewidth',2);
        ylabel(hAx(1),Xoft,'color',color1,'fontsize',12);
        set(hAx(2),'Ycolor',color2,'fontsize',12);
        set(curve2,'color',color2,'linestyle',line_style2,'linewidth',2);
        ylabel(hAx(2),Yoft,'color',color2,'fontsize',12);
        xlabel('t')
        legend(Xoft,Yoft,'location','best')
        title(title_str,'interpreter','none','edgecolor','k')
        %--------------------------------------
    case 'subplots'
        %--------------------------------------
        subplot(2,1,1)
        plot(t,X,'b-','linewidth',2)
        set(gca,'fontsize',12)
        ylabel(Xoft)
        title(title_str,'interpreter','none','edgecolor','k')
        subplot(2,1,2)
        plot(t,Y,'b-','linewidth',2)
        set(gca,'fontsize',12)
        xlabel('t'), ylabel(Yoft)
end;
shg
