%================================================
% two_ODEs_timecourse_neweffect_script.m
%------------------------------------------------
%solves a 2x2 coupled ODE system
%dUdt=f(t,U)
%with a new effect introduced at t=tc
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================

%---------------------------------------------------------------
plot_type='same_axes'; %options are yyplot, subplots or same_axes
%---------------------------------------------------------------

%parameters
%================================
betaX=1;
betaY=2;
KX=5;
KY=3;
deltaXhat=0.5;
deltaYhat=1.3;
%critical time at which new effect as added
tc=7;
%time-dependent interaction parameters
deltaX=@(t) deltaXhat*(t>=tc);
deltaY=@(t) deltaYhat*(t>=tc);

%================================
%initial condition
U0 = [1;    %U1(0)
      1]; %U2(0)
%================================
t_end=10; 
N=1000;
tspan=linspace(0,t_end,N+1)';

%===================================
%system of ODEs
%--------------
U1var='X';
U2var='Y';
dUdt=@(t,U) [betaX*U(1).*(1-U(1)/KX) - deltaX(t)*U(1).*U(2);      %dU1/dt
             betaX*U(2).*(1-U(2)/KY) - deltaY(t)*U(1).*U(2)];     %dU2/dt
%===================================

%==========================================
%solve the ODE system and plot time courses
%==========================================
title_params={'betaX','betaY','deltaXhat','deltaYhat','KX','KY','tc'};
set_title_details;
%----------------
[t,U] = two_ODEs_timecourse_fn(dUdt,tspan,U0,U1var,U2var,plot_type,title_details);
%======================================================================
shg