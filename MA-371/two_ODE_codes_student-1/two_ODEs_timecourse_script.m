%================================================
% two_ODEs_timecourse_script.m
%------------------------------------------------
%solves a 2x2 coupled ODE system
%dUdt=f(t,U)
%================================================
clear all;  clc;
student_name='Lloyd';  student_number='007';
%================================================

%---------------------------------------------------------------
plot_type='same_axes'; %options are yyplot, subplots or same_axes
%---------------------------------------------------------------

%parameters
%================================
betaX=1;
betaY=2;
deltaX=0.5;
deltaY=1.3;
%================================
%initial condition
U0 = [1;    %U1(0)
      1]; %U2(0)
%================================
t_end=4; 
N=1000;
tspan=linspace(0,t_end,N+1)';

%===================================
%system of ODEs
%--------------
U1var='X';
U2var='Y';
dUdt=@(t,U) [betaX*U(1) - deltaX*U(1).*U(2);      %dU1/dt
             betaY*U(2) - deltaY*U(1).*U(2)];     %dU2/dt
%===================================

%==========================================
%solve the ODE system and plot time courses
%==========================================
title_params={'betaX','betaY','deltaX','deltaY'};
set_title_details;
%----------------
[t,U] = two_ODEs_timecourse_fn(dUdt,tspan,U0,U1var,U2var,plot_type,title_details);
%======================================================================
shg