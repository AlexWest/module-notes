clear all;  clc;
student_name='Alex';  student_number='1912648';
%-------------------------------------------------
%parameters
%-------------------------------------------------
r=0.1;
K=50;
h0 = 0.05;
%function
%-------------------------------------------------
f=@(t,P) r.*P.*(1-P/K) - h0.*P;
%-------------------------------------------------
%vector of initial values
Pinit=[0, 5, 10, 20, 30, 40];
%number of points
t_end=60;
%-------------------------------------------------

%===================================================
%specify parameters you'd like to appear in figure title
title_params={'r','K'}; 
set_title_details;
%generate and plot timecourse for each initial condition
[t,PP]=single_ODE_timecourse_fn(f,Pinit,t_end,'-',title_details);

% QUESTION 2
%parameters
%================================
r=2;
K=20;
h0 = 1.9;
%================================

%================================
%RHS of the ODE
f=@(t,X) r*(1-X/K).*X- h0;
%================================

%ranges for t and X0
%------------------
tspan=linspace(0,7,40)';
X0span=linspace(0,20,40)';
%------------------

%==================================
%construct and plot direction field
%==================================
title_params={'r','K','h0'};
set_title_details;
%----------------
Xinit = [2,3,5,7,9];
single_ODE_dir_field_fn(f,tspan,X0span,title_details)
hold on
[t,PP]=single_ODE_timecourse_fn(f,Xinit,tspan(end),'b-',title_details);
hold off
