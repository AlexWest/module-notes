% ⬇ adds subdirectory with provided m files to the path 
addpath([pwd,'/two_ODE_codes_student-1']) 
addpath([pwd,'/linear_systems_codes_student'])
% It can be deleted if there in the current working directory.
student_name='Alex West'; student_number='1912648';

%---------------------------------------------------------------
plot_type='same_axes';

% Question 2 a)

%================================

%X and Y grids for direction field
%=================================
U1var='x';
U2var='y';
U1vec=linspace(-5,5,20);
U2vec=linspace(-5,5,20);
%=================================

%===================================
%system of ODEs
%--------------
dUdt=@(U) [-3*U(1) + 4*U(2);      %dU1/dt
           -6*U(1) - 2*U(2)];     %dU2/dt
%===================================


%======================================================================
%plots
%======================================================================
%set the title
%--------------------------------------
title_params={};
set_title_details;
%--------------------------------------
clf
two_ODEs_dir_field_fn(dUdt,U1vec,U2vec,U1var,U2var,title_details)
%======================================================================
shg

% Question 2) b)
dUdt=@(U) [4*U(1) + 3*U(2);      %dU1/dt
           2*U(1) + 4*U(2)];
figure
clf
two_ODEs_dir_field_fn(dUdt,U1vec,U2vec,U1var,U2var,title_details)
%======================================================================
shg


% Question 3
%matrix
%================================
A=[-0.5 0 0;
   0.5 -0.25 0;
   0 0.25 -0.2];
%================================
%initial condition
X0 = [15;    %x(0)
      0;    %y(0)
      0];   %z(0)
%================================

%time span
%----------------------------
t_end=25; 
N=1000;
tspan=linspace(0,t_end,N+1)';
%----------------------------

%exact solutions
xexact=@(t) 14*exp(-t/2);
yexact=@(t) -30.*exp(-t./2)+30*exp(-t/4);
zexact=@(t) 25*exp(-0.2.*t).*(5.+exp(-3.*t/10) - 6.*exp(-t./20));



%solve numerically
%==========================================================
%solver options
options=odeset('AbsTol',1e-20,'RelTol',1e-6);
%Solve system of ODEs using ode solver
[t, X]  = ode45(@(t,X) A*X,tspan, X0, options);
%==========================================================
%retrieve individual variables
x=X(:,1);
y=X(:,2);
z=X(:,3);
%=============================

%==========================================
%plots
%==========================================
a11=A(1,1); a12=A(1,2); a13=A(1,3);  
a21=A(2,1); a22=A(2,2); a23=A(2,3);  
a31=A(3,1); a32=A(3,2); a33=A(3,3);  
x0=X0(1); y0=X0(2); z0=X0(3);
title_params={'a11','a12','a13','a21','a22','a23','a31','a32','a33','x0','y0','z0'};
set_title_details;
title_str={[mfilename,'.m   ',datestr(now,'dd.mm.yy  ----  HH:MM'),'  '],...
    [title_details{2},'   ',title_details{1}]};
%----------------
texact=linspace(0,t_end,51)';
plot(t,x,'k-',texact,xexact(texact),'ko',t,y,'b--',texact,yexact(texact),'bo',...
     t,z,'r:',texact,zexact(texact),'ro','linewidth',2)
set(gca,'fontsize',12)
xlabel('t'), ylabel('solutions')
title(title_str,'interpreter','none','edgecolor','k')
legend('x(t) numerics','x(t) exact','y(t) numerics','y(t) exact','z(t) numerics','z(t) exact','location','best')
hold off
%=====