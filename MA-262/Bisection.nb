(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21580,        541]
NotebookOptionsPosition[     19559,        498]
NotebookOutlinePosition[     19956,        514]
CellTagsIndexPosition[     19913,        511]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"bisection", "[", 
    RowBox[{"f_", ",", "interval_", ",", "n_"}], "]"}], ":=", " ", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "c", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"result", " ", "=", " ", 
       RowBox[{"{", "}"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{"n", ">", "0"}], ",", 
        RowBox[{
         RowBox[{"c", "=", 
          RowBox[{"Mean", "[", "interval", "]"}]}], ";", 
         "\[IndentingNewLine]", 
         RowBox[{"(*", 
          RowBox[{
           RowBox[{"Print", "[", 
            RowBox[{"\"\<c: \>\"", ",", 
             RowBox[{"N", "[", "c", "]"}], ",", "\"\<, f[c]: \>\"", ",", " ", 
             
             RowBox[{"N", "[", 
              RowBox[{"f", "[", "c", "]"}], "]"}]}], "]"}], ";"}], "*)"}], 
         "\[IndentingNewLine]", ";", "\[IndentingNewLine]", 
         RowBox[{"If", "[", " ", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"f", "[", 
              RowBox[{
              "interval", "\[LeftDoubleBracket]", "1", 
               "\[RightDoubleBracket]"}], "]"}], "*", 
             RowBox[{"f", "[", "c", "]"}]}], "\[LessEqual]", " ", "0"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"bisection", "[", 
             RowBox[{"f", ",", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{
                "interval", "\[LeftDoubleBracket]", "1", 
                 "\[RightDoubleBracket]"}], ",", "c"}], "}"}], ",", 
              RowBox[{"n", "-", "1"}]}], "]"}], ";", 
            RowBox[{"AppendTo", "[", 
             RowBox[{"result", ",", " ", 
              RowBox[{"{", 
               RowBox[{"n", ",", "interval", ",", " ", "c", ",", " ", 
                RowBox[{"f", "[", "c", "]"}]}], "}"}]}], "]"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"bisection", "[", 
             RowBox[{"f", ",", 
              RowBox[{"{", 
               RowBox[{"c", ",", 
                RowBox[{
                "interval", "\[LeftDoubleBracket]", "2", 
                 "\[RightDoubleBracket]"}]}], "}"}], ",", 
              RowBox[{"n", "-", "1"}]}], "]"}], ";", 
            RowBox[{"AppendTo", "[", 
             RowBox[{"result", ",", " ", 
              RowBox[{"{", 
               RowBox[{"n", ",", "interval", ",", " ", "c", ",", " ", 
                RowBox[{"f", "[", "c", "]"}]}], "}"}]}], "]"}]}]}], 
          "\[IndentingNewLine]", "]"}], ";"}]}], "\[IndentingNewLine]", "]"}],
       ";", "\[IndentingNewLine]", "result"}]}], "]"}]}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.822763333722707*^9, 3.82276334470432*^9}, {
   3.822763390374876*^9, 3.822763879668483*^9}, 3.822763962973467*^9, {
   3.82276408651604*^9, 3.822764121393735*^9}, {3.822764158333412*^9, 
   3.822764159343216*^9}, {3.822764197054343*^9, 3.8227642344487257`*^9}, {
   3.822764275309458*^9, 3.822764370931816*^9}, {3.8227645802259283`*^9, 
   3.822764631632718*^9}, {3.822765841644136*^9, 3.822765908842895*^9}, 
   3.82276596397649*^9, {3.8227660087640953`*^9, 3.82276601172388*^9}, {
   3.822766082978056*^9, 3.8227660892265368`*^9}, {3.8227662501950197`*^9, 
   3.822766327197906*^9}, {3.822766357971828*^9, 3.82276636421317*^9}, {
   3.82276645640923*^9, 3.8227664952321577`*^9}, {3.822766569511836*^9, 
   3.822766645881572*^9}, {3.82276670046177*^9, 3.82276677194309*^9}, {
   3.822766906071289*^9, 3.822766958153404*^9}, {3.822767116963709*^9, 
   3.822767136866158*^9}, {3.8227672084653053`*^9, 3.822767237169363*^9}, {
   3.8227672980898046`*^9, 3.822767331066238*^9}, {3.8227673621675158`*^9, 
   3.8227674767807703`*^9}, {3.822767775062055*^9, 3.822767775184106*^9}, {
   3.8227678781316833`*^9, 3.822767878405881*^9}, {3.8227680291285973`*^9, 
   3.822768033480752*^9}},
 CellLabel->
  "In[264]:=",ExpressionUUID->"b32e3656-bcdd-4328-b37c-dd6b36d307e4"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"g", "[", "x_", "]"}], ":=", 
   RowBox[{
    RowBox[{"\[ExponentialE]", "^", 
     RowBox[{"(", 
      RowBox[{"-", "x"}], ")"}]}], "-", 
    RowBox[{"x", "^", "2"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"TraditionalForm", "@", 
  RowBox[{"N", "[", 
   RowBox[{
    RowBox[{"Grid", "[", 
     RowBox[{
      RowBox[{"Prepend", "[", 
       RowBox[{
        RowBox[{"Reverse", "[", 
         RowBox[{"bisection", "[", 
          RowBox[{"g", ",", 
           RowBox[{"{", 
            RowBox[{"0", ",", "1"}], "}"}], ",", "6"}], "]"}], "]"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "\"\<\!\(\*Cell[\"i\",ExpressionUUID->\"93637058-2633-4f51-8483-\
3dee1e650a28\"]\)\>\"", ",", 
          "\"\<{\!\(\*SubscriptBox[\(a\), \(i\)]\), \!\(\*SubscriptBox[\(b\), \
\(i\)]\)}\>\"", ",", "\"\<\!\(\*SubscriptBox[\(c\), \(i\)]\)\>\"", ",", 
          "\"\<f(\!\(\*SubscriptBox[\(c\), \(i\)]\))\>\""}], "}"}]}], "]"}], 
      ",", 
      RowBox[{"Frame", "\[Rule]", " ", "All"}], ",", 
      RowBox[{"Background", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"None", ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Lighter", "[", 
            RowBox[{"Yellow", ",", ".9"}], "]"}], ",", 
           RowBox[{"{", 
            RowBox[{"White", ",", 
             RowBox[{"Lighter", "[", 
              RowBox[{
               RowBox[{"Blend", "[", 
                RowBox[{"{", 
                 RowBox[{"Blue", ",", "Green"}], "}"}], "]"}], ",", ".8"}], 
              "]"}]}], "}"}]}], "}"}]}], "}"}]}], ",", 
      RowBox[{"Spacings", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"Automatic", ",", ".8"}], "}"}]}]}], "]"}], ",", "3"}], 
   "]"}]}]}], "Input",
 CellChangeTimes->{{3.8227638816422873`*^9, 3.822763946777082*^9}, {
   3.822766252932684*^9, 3.822766253652554*^9}, {3.822766388890884*^9, 
   3.822766410684662*^9}, {3.822766971689704*^9, 3.82276702442386*^9}, {
   3.822767170813911*^9, 3.822767172768107*^9}, 3.822767463359642*^9, {
   3.822767510231127*^9, 3.82276751172064*^9}, {3.822767555468216*^9, 
   3.822767743960414*^9}, {3.8227677793484173`*^9, 3.822767865413446*^9}, {
   3.8227680054565687`*^9, 3.822768016859796*^9}, {3.822768134209633*^9, 
   3.8227681388086452`*^9}, 3.822768191413349*^9, {3.822768242339526*^9, 
   3.82276827613179*^9}, {3.822768333037198*^9, 3.822768346993392*^9}, {
   3.822768510610841*^9, 3.822768567456768*^9}, {3.822768598978627*^9, 
   3.822768652985086*^9}, 3.822812050411408*^9, {3.822812156198732*^9, 
   3.822812156620902*^9}, {3.822812263832086*^9, 3.8228123051178637`*^9}},
 CellLabel->
  "In[308]:=",ExpressionUUID->"b79e1fdb-2fb5-47e0-b232-dc2788af0018"],

Cell[BoxData[
 FormBox[
  TagBox[GridBox[{
     {"\<\"\\!\\(\\*Cell[\\\"i\\\"]\\)\"\>", "\<\"{\\!\\(\\*SubscriptBox[\\(a\
\\), \\(i\\)]\\), \\!\\(\\*SubscriptBox[\\(b\\), \\(i\\)]\\)}\"\>", \
"\<\"\\!\\(\\*SubscriptBox[\\(c\\), \\(i\\)]\\)\"\>", \
"\<\"f(\\!\\(\\*SubscriptBox[\\(c\\), \\(i\\)]\\))\"\>"},
     {"6.`3.", 
      RowBox[{"{", 
       RowBox[{"0", ",", "1.`3."}], "}"}], "0.5`3.", 
      "0.3565306597126334236`3."},
     {"5.`3.", 
      RowBox[{"{", 
       RowBox[{"0.5`3.", ",", "1.`3."}], "}"}], "0.75`3.", 
      RowBox[{"-", "0.0901334472589852928`3."}]},
     {"4.`3.", 
      RowBox[{"{", 
       RowBox[{"0.5`3.", ",", "0.75`3."}], "}"}], "0.625`3.", 
      "0.1446364285189902419`3."},
     {"3.`3.", 
      RowBox[{"{", 
       RowBox[{"0.625`3.", ",", "0.75`3."}], "}"}], "0.6875`3.", 
      "0.0301753279709409597`3."},
     {"2.`3.", 
      RowBox[{"{", 
       RowBox[{"0.6875`3.", ",", "0.75`3."}], "}"}], "0.71875`3.", 
      RowBox[{"-", "0.0292404857863808893`3."}]},
     {"1.`3.", 
      RowBox[{"{", 
       RowBox[{"0.6875`3.", ",", "0.71875`3."}], "}"}], "0.703125`3.", 
      "0.0006511313011985816`3."}
    },
    AutoDelete->False,
    GridBoxBackground->{"Columns" -> {{None}}, "Rows" -> {
        RGBColor[1., 1., 0.9], {
         GrayLevel[1.`3.], 
         RGBColor[0.8, 0.9, 0.9]}}},
    GridBoxFrame->{"Columns" -> {{True}}, "Rows" -> {{True}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
    GridBoxSpacings->{"Columns" -> {{Automatic}}, "Rows" -> {{0.8}}}],
   "Grid"], TraditionalForm]], "Output",
 CellChangeTimes->{{3.822767606961269*^9, 3.822767675586129*^9}, {
   3.822767714862075*^9, 3.8227677442617903`*^9}, {3.822767826801817*^9, 
   3.822767832915188*^9}, {3.8227678670060167`*^9, 3.822767880409617*^9}, {
   3.822768010412629*^9, 3.8227680405214148`*^9}, {3.822768187091786*^9, 
   3.822768191771243*^9}, 3.822768276434057*^9, {3.822768335967448*^9, 
   3.82276834820166*^9}, {3.8227685223952*^9, 3.822768537542967*^9}, 
   3.82276860128428*^9, {3.822768649919961*^9, 3.822768653976882*^9}, 
   3.8228120514505796`*^9, 3.822812157280542*^9, 3.82281226824336*^9, {
   3.822812298246213*^9, 3.822812305956317*^9}},
 CellLabel->
  "Out[309]//TraditionalForm=",ExpressionUUID->"82d81392-149a-48a2-a634-\
ab90022e8c0c"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Fixed Point Iteration", "Subsection",
 CellChangeTimes->{{3.822813749370123*^9, 
  3.8228137596650677`*^9}},ExpressionUUID->"697f7338-0f54-4eda-8f49-\
abf82c40a823"],

Cell[BoxData[
 RowBox[{
  RowBox[{"fpi", "[", 
   RowBox[{"f_", ",", "x_", ",", "n_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"i", ",", 
      RowBox[{"x0", " ", "=", " ", "x"}], ",", " ", 
      RowBox[{"result", " ", "=", " ", 
       RowBox[{"{", "}"}]}]}], "}"}], ",", " ", 
    RowBox[{
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"i", "=", "0"}], ",", 
       RowBox[{"i", "\[LessEqual]", "n"}], ",", 
       RowBox[{"i", "++"}], ",", " ", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"AppendTo", "[", 
         RowBox[{"result", ",", 
          RowBox[{"{", 
           RowBox[{"i", ",", " ", "x0", ",", " ", 
            RowBox[{"f", "[", "x0", "]"}]}], "}"}]}], "]"}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"x0", " ", "=", " ", 
         RowBox[{"f", "[", "x0", "]"}]}], ";"}]}], "\[IndentingNewLine]", 
      "]"}], " ", ";", " ", 
     RowBox[{"Return", "[", "result", "]"}]}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.82281376229283*^9, 3.822813773906817*^9}, {
  3.822813827005204*^9, 3.822813941035877*^9}, {3.8228139750092087`*^9, 
  3.82281407091604*^9}, {3.8228141149458733`*^9, 3.822814198126123*^9}, {
  3.8228142903998137`*^9, 3.8228143436514397`*^9}, {3.8228143831219053`*^9, 
  3.822814507039846*^9}},
 CellLabel->
  "In[367]:=",ExpressionUUID->"bb825e00-5cf6-44e2-bb1e-e9e441949e41"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"q", "[", "x_", "]"}], " ", ":=", " ", 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", "x"}], "/", "2"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"TraditionalForm", "@", 
  RowBox[{"N", "[", 
   RowBox[{
    RowBox[{"Grid", "[", 
     RowBox[{
      RowBox[{"Prepend", "[", 
       RowBox[{
        RowBox[{"fpi", "[", 
         RowBox[{"q", ",", " ", 
          RowBox[{"1", "/", "2"}], ",", " ", "6"}], "]"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "\"\<i\>\"", ",", " ", "\"\<\!\(\*SubscriptBox[\(x\), \(i\)]\)\>\"", 
          ",", "\"\<f(\!\(\*SubscriptBox[\(x\), \(i\)]\))\>\""}], "}"}]}], 
       "]"}], ",", " ", 
      RowBox[{"Frame", "\[Rule]", " ", "All"}], ",", " ", 
      RowBox[{"Background", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"None", ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Lighter", "[", 
            RowBox[{"Yellow", ",", ".9"}], "]"}], ",", 
           RowBox[{"{", 
            RowBox[{"White", ",", 
             RowBox[{"Lighter", "[", 
              RowBox[{
               RowBox[{"Blend", "[", 
                RowBox[{"{", 
                 RowBox[{"Blue", ",", "Green"}], "}"}], "]"}], ",", ".8"}], 
              "]"}]}], "}"}]}], "}"}]}], "}"}]}], ",", 
      RowBox[{"Spacings", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"Automatic", ",", ".8"}], "}"}]}]}], "]"}], " ", ",", "5"}], 
   "]"}]}]}], "Input",
 CellChangeTimes->{{3.822814075491506*^9, 3.8228140757372503`*^9}, {
  3.822814371563122*^9, 3.822814371821384*^9}, {3.822814545986657*^9, 
  3.822814634161516*^9}, {3.8228152052222023`*^9, 3.822815296733695*^9}},
 CellLabel->
  "In[386]:=",ExpressionUUID->"1fb905eb-b580-4782-bcbb-1c194a21e419"],

Cell[BoxData[
 FormBox[
  TagBox[GridBox[{
     {"\<\"i\"\>", "\<\"\\!\\(\\*SubscriptBox[\\(x\\), \\(i\\)]\\)\"\>", \
"\<\"f(\\!\\(\\*SubscriptBox[\\(x\\), \\(i\\)]\\))\"\>"},
     {"0", "0.5`5.", "0.7788007830714048682`5."},
     {"1.`5.", "0.7788007830714048682`5.", "0.677462965266488806`5."},
     {"2.`5.", "0.677462965266488806`5.", "0.7126737886881337394`5."},
     {"3.`5.", "0.7126737886881337394`5.", "0.7002366747179082012`5."},
     {"4.`5.", "0.7002366747179082012`5.", "0.7046047037252261426`5."},
     {"5.`5.", "0.7046047037252261426`5.", "0.7030675160640390717`5."},
     {"6.`5.", "0.7030675160640390717`5.", "0.7036080971364034447`5."}
    },
    AutoDelete->False,
    GridBoxBackground->{"Columns" -> {{None}}, "Rows" -> {
        RGBColor[1., 1., 0.9], {
         GrayLevel[1.`5.], 
         RGBColor[0.8, 0.9, 0.9]}}},
    GridBoxFrame->{"Columns" -> {{True}}, "Rows" -> {{True}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
    GridBoxSpacings->{"Columns" -> {{Automatic}}, "Rows" -> {{0.8}}}],
   "Grid"], TraditionalForm]], "Output",
 CellChangeTimes->{{3.822814486082934*^9, 3.822814512962422*^9}, 
   3.82281454805258*^9, {3.822814604254923*^9, 3.822814617592284*^9}, {
   3.822815269436796*^9, 3.822815297607114*^9}},
 CellLabel->
  "Out[387]//TraditionalForm=",ExpressionUUID->"67df0f68-4506-408e-8a90-\
507f631a7c63"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Regula Falsi", "Subsection",
 CellChangeTimes->{{3.82307365521607*^9, 
  3.823073672677107*^9}},ExpressionUUID->"e4762f73-ecce-4b7c-a5ca-\
da10f31022ef"],

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "[", "x_", "]"}], ":=", 
  RowBox[{
   RowBox[{"Tan", "[", 
    RowBox[{"2", 
     RowBox[{"x", "^", "2"}]}], "]"}], "-", "2"}]}]], "Input",
 CellChangeTimes->{{3.82307367451565*^9, 3.8230736856185637`*^9}},
 CellLabel->"In[21]:=",ExpressionUUID->"e4734db6-bbdf-4433-9621-00c5480caa73"],

Cell[BoxData[
 RowBox[{
  RowBox[{"rf", "[", 
   RowBox[{"f_", ",", "x_", ",", " ", "y_", ",", "n_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"c", ",", "a", ",", "b"}], "}"}], ",", 
    RowBox[{
     RowBox[{"a", "=", "x"}], ";", 
     RowBox[{"b", "=", "y"}], ";", 
     RowBox[{"Do", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{"c", " ", "=", " ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"b", "*", 
             RowBox[{"f", "[", "a", "]"}]}], "-", 
            RowBox[{"a", "*", 
             RowBox[{"f", "[", "b", "]"}]}]}], ")"}], "/", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"f", "[", "a", "]"}], "-", 
            RowBox[{"f", "[", "b", "]"}]}], ")"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"Print", "[", 
         RowBox[{"i", ",", 
          RowBox[{"{", 
           RowBox[{"a", ",", "b"}], "}"}], ",", "c", ",", "\"\<,\>\"", ",", 
          RowBox[{"RealSign", "[", 
           RowBox[{
            RowBox[{"f", " ", "[", "a", "]"}], 
            RowBox[{"f", "[", "c", "]"}]}], "]"}]}], "]"}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{"f", "[", "a", "]"}], 
            RowBox[{"f", "[", "c", "]"}]}], "<", "0"}], ",", 
          RowBox[{"b", "=", "c"}], ",", 
          RowBox[{"a", "=", "c"}]}], "]"}], ";"}], "\[IndentingNewLine]", ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "n"}], "}"}]}], "]"}]}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.823073691638069*^9, 3.823073875272294*^9}, {
  3.823073919545649*^9, 3.823073921586193*^9}, {3.823073972118155*^9, 
  3.823073975323502*^9}, {3.823074024292912*^9, 3.823074051860111*^9}, {
  3.8230742031036177`*^9, 3.823074245590447*^9}, {3.823074305930394*^9, 
  3.823074306490584*^9}, {3.823074341122821*^9, 3.823074373762618*^9}},
 CellLabel->"In[40]:=",ExpressionUUID->"4a8d9b96-7ff5-49e3-8a11-7edc28c9c6b3"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"rf", "[", 
  RowBox[{"f", ",", "0", ",", "0.8", ",", "3"}], "]"}]], "Input",
 CellChangeTimes->{{3.8230739236651077`*^9, 3.823073942577505*^9}},
 CellLabel->"In[41]:=",ExpressionUUID->"e73016d6-f952-41b4-aa9a-0734bbdd09c8"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"1", "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0.8`"}], "}"}], "\[InvisibleSpace]", 
   "0.478848372377294`", "\[InvisibleSpace]", "\<\",\"\>", 
   "\[InvisibleSpace]", "1"}],
  SequenceForm[1, {0, 0.8}, 0.478848372377294, ",", 1],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.823074250430501*^9, {3.823074310035516*^9, 3.823074321956896*^9}, {
   3.823074356291444*^9, 3.8230743783452387`*^9}},
 CellLabel->
  "During evaluation of \
In[41]:=",ExpressionUUID->"30d72861-0058-4016-9f1d-2b48835ea3ff"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"2", "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{"0.478848372377294`", ",", "0.8`"}], "}"}], "\[InvisibleSpace]", 
   "0.6487257582992781`", "\[InvisibleSpace]", "\<\",\"\>", 
   "\[InvisibleSpace]", "1"}],
  SequenceForm[2, {0.478848372377294, 0.8}, 0.6487257582992781, ",", 1],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.823074250430501*^9, {3.823074310035516*^9, 3.823074321956896*^9}, {
   3.823074356291444*^9, 3.823074378347013*^9}},
 CellLabel->
  "During evaluation of \
In[41]:=",ExpressionUUID->"6789c1fb-0669-4bb3-9cda-92461de73c02"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"3", "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{"0.6487257582992781`", ",", "0.8`"}], "}"}], "\[InvisibleSpace]", 
   "0.7086771721713135`", "\[InvisibleSpace]", "\<\",\"\>", 
   "\[InvisibleSpace]", "1"}],
  SequenceForm[3, {0.6487257582992781, 0.8}, 0.7086771721713135, ",", 1],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.823074250430501*^9, {3.823074310035516*^9, 3.823074321956896*^9}, {
   3.823074356291444*^9, 3.823074378348473*^9}},
 CellLabel->
  "During evaluation of \
In[41]:=",ExpressionUUID->"7dfa4916-5747-42db-8400-fc7023168f41"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"f", "[", "0.745", "]"}]], "Input",
 CellChangeTimes->{{3.823074492540316*^9, 3.823074560852324*^9}},
 CellLabel->"In[54]:=",ExpressionUUID->"1f95a086-4287-4061-8e3b-f8c792d77bbc"],

Cell[BoxData["0.014591117870801806`"], "Output",
 CellChangeTimes->{{3.8230744977939663`*^9, 3.823074561323162*^9}},
 CellLabel->"Out[54]=",ExpressionUUID->"ddea6e1c-0e68-4cf7-baea-6f43d13451f9"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{808, 776},
WindowMargins->{{305, Automatic}, {Automatic, 0}},
FrontEndVersion->"12.2 for Mac OS X x86 (64-bit) (December 12, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"708f6eac-7fcd-4fd1-b0db-332f44e61773"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 4006, 87, 304, "Input",ExpressionUUID->"b32e3656-bcdd-4328-b37c-dd6b36d307e4"],
Cell[CellGroupData[{
Cell[4589, 111, 2702, 62, 140, "Input",ExpressionUUID->"b79e1fdb-2fb5-47e0-b232-dc2788af0018"],
Cell[7294, 175, 2308, 52, 189, "Output",ExpressionUUID->"82d81392-149a-48a2-a634-ab90022e8c0c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9639, 232, 171, 3, 54, "Subsection",ExpressionUUID->"697f7338-0f54-4eda-8f49-abf82c40a823"],
Cell[9813, 237, 1419, 35, 115, "Input",ExpressionUUID->"bb825e00-5cf6-44e2-bb1e-e9e441949e41"],
Cell[CellGroupData[{
Cell[11257, 276, 1764, 46, 115, "Input",ExpressionUUID->"1fb905eb-b580-4782-bcbb-1c194a21e419"],
Cell[13024, 324, 1381, 27, 209, "Output",ExpressionUUID->"67df0f68-4506-408e-8a90-507f631a7c63"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[14454, 357, 159, 3, 54, "Subsection",ExpressionUUID->"e4762f73-ecce-4b7c-a5ca-da10f31022ef"],
Cell[14616, 362, 325, 8, 30, "Input",ExpressionUUID->"e4734db6-bbdf-4433-9621-00c5480caa73"],
Cell[14944, 372, 2047, 51, 115, "Input",ExpressionUUID->"4a8d9b96-7ff5-49e3-8a11-7edc28c9c6b3"],
Cell[CellGroupData[{
Cell[17016, 427, 247, 4, 30, "Input",ExpressionUUID->"e73016d6-f952-41b4-aa9a-0734bbdd09c8"],
Cell[CellGroupData[{
Cell[17288, 435, 573, 14, 24, "Print",ExpressionUUID->"30d72861-0058-4016-9f1d-2b48835ea3ff"],
Cell[17864, 451, 606, 14, 24, "Print",ExpressionUUID->"6789c1fb-0669-4bb3-9cda-92461de73c02"],
Cell[18473, 467, 608, 14, 24, "Print",ExpressionUUID->"7dfa4916-5747-42db-8400-fc7023168f41"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[19130, 487, 203, 3, 30, "Input",ExpressionUUID->"1f95a086-4287-4061-8e3b-f8c792d77bbc"],
Cell[19336, 492, 195, 2, 34, "Output",ExpressionUUID->"ddea6e1c-0e68-4cf7-baea-6f43d13451f9"]
}, Open  ]]
}, Open  ]]
}
]
*)

