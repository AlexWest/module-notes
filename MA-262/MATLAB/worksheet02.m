% Question 1
figure(1)
y = @(x) exp(-x.^2).*sin(pi.*x.^3);
x = linspace(-2,2,250);
% plot(x,y(x))
title("figure 1")
% Question 2
figure(2)
syms x;
y1(x) = 3*cos(x);
y2(x) = -2*cos(2*x);
y3(x) =  -0.8*sin(x^2);
hold on
fplot(y1(x),[-2*pi 2*pi], "b")
fplot(y2(x),[-2*pi 2*pi], "r")
fplot(y3(x),[-2*pi 2*pi], "m")
hold off
xlabel("x-axis")
ylabel("y-axis")
grid
title("Figure 2")
% Question 3
f(x) = 1 +5*x -6*x^3 - exp(2*x);
if f(-2)*f(2) < 0
    disp("Theres a root")
else
    disp("Ain't no root")
end
figure(3)
fplot(f(x),[-2 2])
grid
title("Figure 3")
% Question 4
n = [1:10];
[n; factorial(n)./(n.^n)]'
% Question 5
clear
syms x;
f(x) = (x^2)*asin(x);
g(x) = ((x^2)*(2*(x^2) - 3)) / ((1 - (x^2))^(3/2));
simplify(diff(f) -x*diff(f, 2) - g)