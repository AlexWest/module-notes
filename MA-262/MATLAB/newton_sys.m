function newton_sys(F,JF,xx0,tol,maxit)
    %solve a nonlinear system of equations using Newton's method
    x0=xx0
    iter=1;
    while(iter<=maxit)
        y=-1*feval(JF,x0)\feval(F,x0);
        xn=x0+y;
        err=max(abs(xn-x0));
        if (err<=tol)
            x=xn;
            fprintf(' Newton method converges after %3.0f iterations to\n', iter)
            x
            return;
        else
            x0=xn;
        end
        iter=iter+1;
    end
    disp('Newton method does not converge')
    x=xn;
end