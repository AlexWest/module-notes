% Question 1
syms x;a = 1;b=2;
f(x) = x^5 - 3*x^3 +2.5*x - 0.6;
fprintf("f(1)*f(2) = %f < 1 so a root exists." ,f(a)*f(b));
fprintf("\n Theres a root at x = %.5f.", bisect(f, 1,2,10^-5));

% Question 2
g(x) = x+ exp(x);
fprintf("\n A better root estimate would be x = %.5f (5dp).", bisect(g,-1,0,10^-5));
% Question 3
q(x) = -sin(x) - 1; % Simplest possirble rearangement of function given.
fprintf("Using the reagrangment g(x) = -1 -sin(x) and 1000 iterations we get an estimate of: %f.",...
         fpi(q, -0.5, 1000));
% Qustion 4
l(x) = atan(x)^(1/3);
fprintf("\n x^3 = arctan(x) has a root at aprox x = %f.", fpi(l, 1, 1000));

function xc=bisect(f,a,b,tol)
    fa=f(a); % Has no stopping criteria but oh well
    fb=f(b);
    while (b-a)/2>tol
        c = (a+b)/2;
        fc=f(c);
        if fc ==0
            break
        end
        if sign(fc)*sign(fa)<0
            b=c;
            fb=fc;
        else
            a=c;
            fa=fc;
        end
    end
    xc = (a+b)/2;
end

function xc=fpi(f, x0, k)
    x(1) = x0;
    for i=1:k
        x(i+1) = f(x(i));
    end
    xc=x(k+1);
end