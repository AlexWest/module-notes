% Question 1
a = [2, 3, -1, 5]; b = [4, -2, 6, 1];
result = 0;
for i = 1:length(a)
    result = result + a(i)*b(i);
end
fprintf("My result: %d. Built in function result %d.", result, dot(a, b))
% Question 2
result = 1;
for n = 1:10
    result = result*n^2;
end
fprintf("\nQ2 result: %u", result)
% Question 3
result = 0;
for k = 1:10
    result = result + k^2;
end
fprintf("\nQ3 result %u", result)
% Question 4
result = 0;
old_result = -1;
k =  0;
while result - old_result>0
    old_result = result;
    result = result + (2^k)/factorial(k);
    k = k +1;
end
fprintf("Q4 result: %.20f (20 dp)", result)