% Quesiton 1
disp('Question 1')
x = [0 pi/10 pi/5 3*pi/10 2*pi/5 pi/2];
fx = [0 0.0985 0.3846 0.7760 1 0.6243];     
% π/20
disp('For π/20 we get:')
spl1(x, fx, pi/20)
% π/4
disp("For π/4:")
spl1(x, fx, pi/4)
disp('For 9π/20:')
spl1(x, fx, 9*pi/20)

sx = pli(x,fx);
s0 = @(x) sx{1}(x);
s1 = @(x) sx{2}(x);
s2 = @(x) sx{3}(x);
s3 = @(x) sx{4}(x);
s4 = @(x) sx{5}(x);

hold on
title("Plot 1")
grid
% Sn Plots
for i=1:(length(x)-1)
    plotrange = linspace(x(i),x(i+1), 100);
    plot(plotrange, sx{i}(plotrange))
end
% True Value Plot
plot(linspace(0,pi/2,1000),sin(linspace(0,pi/2,1000).^2))
hold off

% Question 2
disp(" ")
disp("Question 2")
x2 = [0 .1 .2 .3 .4 .5 .6];
fx2 = [1 1.25 1.58 1.99 2.51 3.16 3.93];
disp("We can see the midpoints from every other point in this table")
spl2(x2,fx2, 1);


function sx = pli(x,y)
    % Returns a cell array of the function handles for the piece wise functions
    % of the linear spline through (xi,yi)
    sx = {};
    for i=1:(length(x)-1)
        m=(y(i+1)-y(i))/(x(i+1)-x(i));
        sx{i} = @(q) polyval([m (y(i) - m*x(i))], q);
    end
end