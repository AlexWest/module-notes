clear()
% Question 1
disp("Question 1")
syms x;
f(x) = x^3 - 7*x^2 + 11*x-5;
% Not sure if its the use of a symbolic function but this is embarissingly
% slow compared to python/Mathematica
disp("The soloution to f(x) = 0 to 5dp is shown in the followong table")
newton(f, diff(f), 1.5,10^-5,20)


% Question 2
fprintf("\n Question 2 \n")
g  = @(x) exp(-x^2) - cos(2*x) - 1;
g_diff = @(x) -2*x*exp(-x^2) + 2*sin(2*x);
disp("Using x0 = 0.5 we can solve f(x) = 0 producing the following table")
newton(g, g_diff, 0.5, 10^-7, 100)
% Although faster than using a symbolic function still noticably slow.
% Maybe unable to use CPU floating point instructions? 
% TODO; Try on x86; Check it doesn't try and use GPU.
disp("if we use x0 = 0 we get:")
newton(g, g_diff, 0, 10^-7, 100)
disp("As the derivative is 0 at at x = 0")

% Question 3
fprintf("\nQuestion 3\n")
q = @(x) x^3 - 4*x^2 - 3*x + 18;
q_diff = @(x) 3*x^2 - 8*x - x;
newton2(q, q_diff, 3.2, 2,10^-5 ,1000);

% Question 4
fprintf("\nQuestion 4\n")
newtonsys('f4', 'df4', [1.5 1.3]', 10^-4, 30); 
fprintf("\nThere are 4 soloutions as it's mirrored in both the x and y axis because of the squares. \n");
fprintf("This is easily checked with a simply plot. \n")
function f = f4(X)
    x=X(1); y =X(2);
    f = [x.^2+y.^2-4; x.^2-y.^2-1];
end

function df=df4(X)
    x=X(1); y=X(2);
    df = [2.*x, 2.*y; 2.*x, -2.*y];
    
end

% Another M file just seems unescarry
function newtonsys(F,JF,xx0,tol,maxit)
    %solve a nonlinear system of equations using Newton's method
    x0=xx0
    iter=1;
    while(iter<=maxit)
        y=-1*feval(JF,x0)\feval(F,x0);
        xn=x0+y;
        err=max(abs(xn-x0));
        if (err<=tol)
            x=xn;
            fprintf(' Newton method converges after %3.0f iterations to\n', iter)
            x
            return;
        else
            x0=xn;
        end
        iter=iter+1;
    end
    disp('Newton method does not converge')
    x=xn;
end
