function df=df4(X)
    x=X(1); y=X(2);
    df = [4.*x.^3, 4.*y.^3; 3.*x^2, 1];
end