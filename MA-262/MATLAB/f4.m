function f = f4(X)
    x=X(1); y =X(2);
    f = [x.^4+y.^4-1; x.^3+y-1.5];
end