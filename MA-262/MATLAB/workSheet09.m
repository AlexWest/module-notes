% Question 1
disp("Question 1")
f = @(x) x^4 * log(x + sqrt(x^2 + 1));
romberg2(f, 0, 2, 6)
fprintf("From the above table of romberg table we can see that the integral of f is aproximately 8.1533641200. \n")

% Question 2
fprintf("\n Qestion 2 \n")
f2 = @(x, t) (250*(x^2)) / ( (cosh(500*(x-t)))^2 );

disp("Using t=0.5")
romberg2(@(x) f2(x, 0.5), -pi, pi, 8)
disp("Using t=1")
romberg2(@(x) f2(x, 1), -pi, pi, 8)

fprintf("Using the above two tables we can get the following values.\n")
fprintf("N. Rows     t=0.5       t=1 \n")
fprintf("  4           0          0  \n")
fprintf("  6           0      0.0000032515  \n")
fprintf("  8     0.0006228376 0.0000003433  \n")

% Question 3
fprintf("\n Qestion 3 \n")
% Exact soloution function in euler m file 🦦.
f3 = @(t, y) -y/(1+t);
for i=0:2
    fprintf("N=%i", 8*2^i)
    euler(f3, 0, 1.6, 1, 8*2^i) 
end
fprintf("\n So we can clearly see that N=32 gives the best aproximation.")