% Question 1
cosh(0.3)
log10(2237) + log(4) - tan(7.2*pi)
atan(0.5)
4*(12^(1/3))
0.5*(acot(2.1 * pi))
6*sind(75)+ (1/4)*cosd(63)
% Question 2
w1 = 83*ones(1,10)
w2 = linspace(32, 421, 12)'
x = 0.432; y=1.654; w3 = [x x^3 inv(y) x*y x^inv(y)]
% Question 3
vec1 = [8, 6, 90, -0.14, 56, 76, 7, -2, 0, 0.82176, 10 , -54]
v1 = vec1(2:5)
v2 = [vec1(1) vec1(4:7) vec1(10)]
v3 = [vec1(11) vec1(5) vec1(8) vec1(1:3)]
% Question 4
u = [9 -7.5 4.1]; v = [-8.5 0.3 -7];
u*v'
sum(u.*v)
dot(u, v)
% Question 5
A = [10 15; 8 6]; B = [-1 5; 0.5 6.3];
A+B
B-A
B*A 
B.*A
% Question 6
M1 = zeros(2,4)
M2 = [zeros(3) eye(3); zeros(3,6)]
M3 = repmat([1:5], 4,1)