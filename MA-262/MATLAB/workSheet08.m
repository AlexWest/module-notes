% Question 1
disp("Question 1")
f = @(x) x*exp(-x^3);
fprintf("Using the trapezium rule with 20 equally spaced interval we find ∫f ≈ %f. \n", trapezium(f,0,1.2,20))

% Question 2
disp("Question 2")
f2 = @(x) sin(exp(x));
fprintf("Using simpsons rule with 1.2/0.01 intervals we get ∫f ≈ %f. \n",simpson(f2, 0, 1.2, (1.2/0.01)))

function I=trapezium(f,a,b,N)
    x=linspace(a,b,N+1)';
    h=(b-a)/N;
    I=f(x(1));
    for j=2:N
        I=I+2*f(x(j));
    end
    I=I+f(x(N+1));
    I=I*(h/2);
end

function I=simpson(f,a,b,M) %M=2N
    x=linspace(a,b,M+1)';
    h=(b-a)/M;
    %Simpson weights
    w=2*ones(M+1,1);
    w(1)=1;
    for j=2:2:M
        w(j)=4;
    end
    w(end)=1;
    %Simpson calculations
    %summing in a loop
    I=0;
    for j=1:M+1
        I=I+(h/3)*w(j)*f(x(j));
    end
end
