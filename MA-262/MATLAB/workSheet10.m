% Im afraid i haven't had a chance to finish this. But I would apreciate some feedback on whats here.
disp("Question 1")
disp("(a)")
yn=2;
y0=1;t0=1;
f = @(t, y) -4*y*t^3;
rk4(f, yn, t0, y0, 10);
abash(f, t0, yn, y0, 100, 4);
% They both see to yeild very diffrent results. And more importantly they both seem to be very wrong y(2)≈0.000000305902
% based on soloutions found using mathemtica. Masively increasing N for my rk4 yeilds better result. abash shows no real improvement for N=1000.
% 
disp("(b)")
yn=1;
y0=2;t0=0;
f = @(t, y) -t*y;
rk4(f, yn, t0, y0, 10);
abash(f, t0, yn, y0, 10, 4);

disp("(c)")
yn=1.4;
y0=2;t0=0;
f = @(t, y) -cos(t)*y;
rk4(f, yn, t0, y0, 14);
abash(f, t0, yn, y0, 14, 4);

disp("Question 2")
f = @(t, y) exp(-t) - y;
N = (1/0.1);

abash(f, 0, 1, 0, N ,2)
abash(f, 0, 1, 0, N ,3)


function yN=rk4(f, tn, t0, y0, N)
    ti = t0;
    yi = y0;
    h = abs(tn-t0)/N;
    fprintf("y(%1$.4f)≈y%2$i=%3$.10f\n",ti,0,yi);
    for i=1:N
        k1 = f(ti, yi);
        k2 = f(ti + h/2, yi + (k1*h)/2);
        k3 = f(ti + h/2,yi + (h*k2)/2);
        k4 = f(ti + h, yi + h*k3);
        yi = yi + (h/6)*(k1 + 2*k2 + 2*k3 + k4);
        ti = ti +h;
        fprintf("y(%1$.4f)≈y%2$i=%3$.10f\n",ti,i,yi);
    end
    yN=yi;
end