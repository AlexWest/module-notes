% Question 1
fprintf("\nQuestion 1\n")
x=[-2 0 -1 1 2]';
y=[4 1 -1 1 9]';
disp("We get a polynomial with the following coeificients: ")
lagrange(x,y,0.7)

% Question 2
fprintf("\nQuestion 2\n")
x = [-3 -2 -1 0 1];
y = [-41 -17 -5 1 3];
newtondd(x, y)
p=@(x) -41 + 24*(x+3) - 6*(x+3)*(x+2) + (x+3)*(x+2)*(x+1) -0.1667*(x+2)*(x+2)*(x+1)*(x-0);
fprintf("Using the above we can construct the polynomial f(x) ≈ -41 + 24*(x+3) - 6*(x+3)*(x+2) + (x+3)*(x+2)*(x+1) -0.1667*(x+2)*(x+2)*(x+1)*(x-0)")
fprintf("\nSo f(-0.5) ≈ %f. \n", p(-0.5))

% Question 3
fprintf("\nQuestion 3\n")
x = linspace(0, 1, 6)';
newtondd(x, exp(x))
fprintf("From the table above we can see that f(x) ≈ \n  1 + 1.1070(x-0) + 0.6217(x-0)(x-0.2) + 0.2261(x-0)(x-0.2)(x-0.4)+0.0626(x-0)(x-0.2)(x-0.4)(x-0.6)(x-0.8)+0.0139(x-0)(x-0.2)(x-0.4)(x-0.6)(x-0.8)(x-1)\n")
p = @(x) 1 + 1.1070*(x-0) + 0.6217*(x-0)*(x-0.2) + 0.2261*(x-0)*(x-0.2)*(x-0.4)+0.0626*(x-0)*(x-0.2)*(x-0.4)*(x-0.6)*(x-0.8)+0.0139*(x-0)*(x-0.2)*(x-0.4)*(x-0.6)*(x-0.8)*(x-1);
% I wasn't quite sure what you meant by midpoint here so I assumed the midpoint between the interpolated points.
for i = 1:(length(x)-1)
    xm=( x(i) + x(i+1) )/2;
    fprintf("For the midpoint x=%f we have exp(x) = %f and are aprximation is: %f. This gives us a relative error of %f. \n", xm, exp(xm), p(xm), abs(exp(xm)-p(xm))/exp(xm))
end

% Question 4
fprintf("\nQuestion 4 \n")
T = [605 685 725 765 825 855 875];
C = [0.622 0.655 0.668 0.679 0.730 0.907 1.336];
p = polyfit(T, C, 7);

fprintf("C(645) ≈ %f \n", polyval(p, 645))
fprintf("C(795) ≈ %f \n", polyval(p, 795))
fprintf("C(845) ≈ %f \n", polyval(p, 845))




function lagrange(x,y,a)
    % coefficients of the lagrange interpolating polynomial
    n=length(x);
    p=0;
    for k=1:n
        b(k)=1;
        d(k)=1;
        for j=1:n
            if j~=k
                b(k)=b(k)*(x(k)-x(j));
                d(k)=d(k)*(a-x(j));
            end
        end
        c(k)=y(k)/b(k);
        p=p+c(k)*d(k);
    end
    c
    fprintf('\n  f(%.3f) ≈%10.6f \n',a, p)
end