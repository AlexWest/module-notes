% Part i
fprintf("\nPart (i)\n")
disp("Using the newton_sys function we find that with:")
newton_sys('f4', 'df4', [0.85 0.7]', 10^-6, 100); 

% Part ii
fprintf("\nPart (ii)\n")
x = [0 0.8 1 1.4 2];
fx = [0 -0.174709 0 0.438612 0.932039];
disp("If we calculate the divided diffrence table with netwondd")
newtondd(x, fx)
disp("Using the above newton divided diffrence table above we find the following aproximation of f(x):")
disp("f(x) ≈ 0 + -0.2184*(x-0)+1.0919*(x-0)*(x-0.8) + -0.5145*(x-0)*(x-0.8)*(x-1) + -0.0118*(x-0)*(x-0.8)*(x-1)*(x-1.4)")
f = @(x)  -0.2184*(x-0)+1.0919*(x-0)*(x-0.8) + -0.5145*(x-0)*(x-0.8)*(x-1) + -0.0118*(x-0)*(x-0.8)*(x-1)*(x-1.4);
fprintf("So we get f(1.2) ≈ %f",f(1.2))
% (Function from part ii)
% function f = f4(X)
%     x=X(1); y =X(2);
%     f = [x.^4+y.^4-1; x.^3+y-1.5];
% end

% (Jacobian Function from part ii)
% function df=df4(X)
%     x=X(1); y=X(2);
%     df = [4.*x.^3, 4.*y.^3; 3.*x^2, 1];
% end