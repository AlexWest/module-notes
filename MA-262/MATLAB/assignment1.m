% Part (i)
n = 12;
result = zeros(n,3);
total = 0;
for i=1:n
    a = (1/(i^2+i));
    total = total + a;
    result(i,:) = [i a total];
end
result
% Part (ii)
a = 1/2;
b = 0;
n = 1;
while a-b>0
    n = n+1;
    b = a;
    a = a + 1/(n^2+n);
end
fprintf("S = %f and it took %i itterations \n",a,n)