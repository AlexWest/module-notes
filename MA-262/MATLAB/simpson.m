function I=simpson(f,a,b,M) %M=2N
    x=linspace(a,b,M+1)';
    h=(b-a)/M;
    %Simpson weights
    w=2*ones(M+1,1);
    w(1)=1;
    for j=2:2:M
        w(j)=4;
    end
    w(end)=1;
    %Simpson calculations
    %summing in a loop
    I=0;
    for j=1:M+1
        I=I+(h/3)*w(j)*f(x(j));
    end
end
