f = @(x) (x^2 - x + 0.25)*(x^2 - x - 0.75)/(sqrt(1+x));
disp("Part (i)")

M = @(h) (0.6+1.6)/h;
fprintf("Using composite simposns rule with h=0.1  we get I≈%10f.\n",simpson(f,-0.6,1.6,M(0.1)));
fprintf("Using composite simpsons rule with h=0.05 we get I≈%10f.\n",simpson(f,-0.6,1.6,M(0.05)));


disp("Part (ii)")
romberg2(f,-0.6,1.6,7)
fprintf("From this Romeberg table we can see that with n=7 Romberg Integration give us I≈ -0.2117202912")

function I=simpson(f,a,b,M) %M=2N
    x=linspace(a,b,M+1)';
    h=(b-a)/M;
    %Simpson weights
    w=2*ones(M+1,1);
    w(1)=1;
    for j=2:2:M
        w(j)=4;
    end
    w(end)=1;
    %Simpson calculations
    %summing in a loop
    I=0;
    for j=1:M+1
        I=I+(h/3)*w(j)*f(x(j));
    end
end
